@extends('layouts.default')
@section('content')<br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Customer Schedules</h3>
            </div>
        </div> 
    </div>
    <div class="row">
        <div class="col-lg-12 margin-tb" style="margin-top: -30px;">
            <div class="input-field col s5" style="margin-top: 25px;">
            </div>
            <div class="input-field col s5 pull-right">
                <i class="material-icons prefix">search</i>
          {!! Form::text('search_text', null, array('class' => 'search_text','id'=>'search_text')) !!}
          <label for="search_text">Search</label>
            </div>  
        </div>
    </div>

    <ul class="collapsible" data-collapsible="accordion">
    <li>
      <div class="collapsible-header waves-effect waves-red"><i class="material-icons">add_box</i>Add New Schedule</div>
      <div class="collapsible-body"><span>
   
 @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
 {!! Form::open(array('route' => 'maintenances.store','method'=>'POST')) !!}
          @include('maintenances.form_maintenance')
{!! Form::close() !!}
</span></div>
   </li>
  </ul> 


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    
    <div class="table-responsive">
    <table class="table table-bordered hoverTable">
    <thead>
         <tr class="#bdbdbd grey darken-2" style="color: white;">
            <th>Customer Name</th>
            <th>Vehicle Type</th>
            <th>Plate No</th>
            <th>Description</th>
            <th>From Date</th>
            <th>Until Date</th>
            <th>From Time</th>
            <th>To Time</th>
            <th>Amount</th>
            <th>Status</th>
            <th>Note</th>
        </tr>
    </thead>
       
<tbody id="schedtable">
     @foreach ($maintenances as $maintenance)
    <tr>
   <!--      <td>{{ ++$i }}</td> -->
        <!-- <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->id}}</a></td> -->
        <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->name}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->type}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->plate}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->description}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->sched_from}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->sched_to}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->from_time}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->to_time}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->amount}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->status}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$maintenance->id}}" style="color: black;">{{ $maintenance->note}}</a></td>

    </tr>
    @endforeach
</tbody>
   
    </table>
    </div>
   


 
   @foreach ($maintenances as $maintenance)
  <!-- Modal Structure for Show -->
  <div id="show{{$maintenance->id}}" class="modal">
    <div class="modal-content">
     
        <div class="modal-header">
            <h4 class="modal-title">Schedule Details</h4>
            <div class="pull-right">
                <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Edit" href="#edit{{$maintenance->id}}"><i class="material-icons">edit</i></a>

                 {!! Form::open(['method' => 'DELETE','route' => ['maintenances.destroy', $maintenance->id],'style'=>'display:inline']) !!}
                    {!! Form::button('<i class="material-icons">delete</i>', ['class' => 'waves-effect waves-light red btn modal-trigger tooltipped','data-tooltip' => 'Delete']) !!}
                {!! Form::close() !!}

            </div>
        </div>
        
<br>

    <div class="modal-body">
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Maintenance ID:</strong>
                {{ $maintenance->id}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Customer ID:</strong>
                {{ $maintenance->customer_id}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Vehicle ID:</strong>
                {{ $maintenance->vehicle_id}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description:</strong>
                {{ $maintenance->description}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Sched From:</strong>
                {{ $maintenance->sched_from}}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Sched To:</strong>
                {{ $maintenance->sched_to}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>From Time:</strong>
                {{ $maintenance->from_time}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>To Time:</strong>
                {{ $maintenance->to_time}}
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Amount:</strong>
                {{ $maintenance->amount}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status:</strong>
                {{ $maintenance->status}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Note:</strong>
                {{ $maintenance->note}}
            </div>
        </div>
    </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default modal-action modal-close" >Close</button>
    </div>
    </div>
    
  </div>



  <!-- Modal Structure for EDit -->
  <div id="edit{{$maintenance->id}}" class="modal">
    <div class="modal-content">
      
        <div class="modal-header">
            <h4 class="modal-title">Update Schedule</h4>
            <div class="pull-right">
                <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::model($maintenance, ['method' => 'PATCH','route' => ['maintenances.update', $maintenance->id]]) !!}
            @include('maintenances.form_maintenance')
        {!! Form::close() !!}

    </div>
    </div>
              
             
   @endforeach

   <script> 

         $(document).ready(function() {
        $('select').material_select();
        });

         $('#customer_name').on('change',function(){
         var name = $(this).children('option:selected').data('code');
         $('#customer_id').val(name);
    });


         $(document).ready(function() {

          Materialize.updateTextFields();

          $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year,
            format: 'yyyy-mm-dd',
            today: 'Today',
            clear: 'Clear',
            close: 'Ok',
            closeOnSelect: false // Close upon selecting a date,
          });

          $('.timepicker').pickatime({
            default: 'now', // Set default time: 'now', '1:30AM', '16:30'
            fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
            twelvehour: false, // Use AM/PM or 24-hour format
            donetext: 'OK', // text for done-button
            cleartext: 'Clear', // text for clear-button
            canceltext: 'Cancel', // Text for cancel-button
            autoclose: false, // automatic close timepicker
            ampmclickable: true, // make AM PM clickable
            aftershow: function(){} //Function for after opening timepicker5 
          });               

     });

    $(document).ready(function(){
    $("#search_text").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#schedtable tr").filter(function() {
    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
   </script>

  
@endsection