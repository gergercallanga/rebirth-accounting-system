@extends('layouts.default')
@section('content')
<br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Vehicles Information</h3>
            </div>
        </div>
    </div>
    <div class="row"> 
        <div class="col-lg-12 margin-tb" style="margin-top: -30px;">
            <div class="col s5" style="margin-top: 25px;">
                    <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Insert New Record" href="#addcar"> <i class="material-icons">add</i></a>
            </div>
            <div class="input-field col s5 pull-right">
              <i class="material-icons prefix">search</i>
          {!! Form::text('search_text', null, array('class' => 'search_text','id'=>'search_text')) !!}
          <label for="search_text">Search</label>
            </div> 
        </div> 
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered hoverTable">
        <tr class="#bdbdbd grey lighten-1">

            <th>Type</th>
            <th>Model</th>
            <th>Plate</th>
            <th>Color</th>
            <th>Owner</th>
</tr>
<tbody id="cartable">
     @foreach ($vehicles as $vehicle)
    <tr>
       
        <td><a class= "modal-trigger" href="#show{{$vehicle->id}}" style="color: black;">{{ $vehicle->type}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$vehicle->id}}" style="color: black;">{{ $vehicle->model}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$vehicle->id}}" style="color: black;">{{ $vehicle->plate}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$vehicle->id}}" style="color: black;">{{ $vehicle->color}}</a></td>
        <td><a class= "modal-trigger" href="#show{{$vehicle->id}}" style="color: black;">{{ $vehicle->customer->name}}</a></td>
     
    </tr>
    @endforeach
</tbody>
   
    </table>
    </div>

@foreach($vehicles as $vehicle)
  <!-- Modal Structure for SHOW -->
 <div id="show{{$vehicle->id}}" class="modal">
    <div class="modal-content">
     <div class="modal-header">
        <h4 class="modal-title">Full Details</h4>
             <div class="pull-right">
                 <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Edit" href="#edit{{$vehicle->id}}"><i class="material-icons">edit</i></a>
            {!! Form::open(['method' => 'DELETE','route' => ['vehicles.destroy', $vehicle->id],'style'=>'display:inline']) !!}

            {{Form::button('<i class="material-icons">delete</i>', array('type' => 'submit','class' => 'waves-effect waves-light red btn modal-trigger tooltipped','data-tooltip' => 'Delete'))}}
            {!! Form::close() !!}
                </div>
     </div>
<div class="modal-body">
    <div class="row">
       
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ID:</strong>
                {{ $vehicle->id}}
            </div>
        </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Customer ID:</strong>
                {{ $vehicle->customer_id}}
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Type:</strong>
                {{ $vehicle->type}}
            </div>
        </div>
   

        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Model:</strong>
                    {{ $vehicle->model}}
                </div>
            </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Plate:</strong>
                    {{ $vehicle->plate}}
                </div>
            </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Color:</strong>
                    {{ $vehicle->color}}
                </div>
            </div>
            
        </div>
    </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-default modal-action modal-close" >Close</button>
      </div>
  </div>
  </div>
   



 <!-- Modal Structure for EDIT -->
  <div id="edit{{$vehicle->id}}" class="modal">
    <div class="modal-content">
       
        <div class="modal-header">
        <h4 class="modal-title">Edit Details</h4>
            <div class="pull-right">
                <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
   
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($vehicle, ['method' => 'PATCH','route' => ['vehicles.update', $vehicle->id]]) !!}
        @include('vehicles.form')
    {!! Form::close() !!}
    </div>
<!--     <div class="modal-footer">
       <button type="submit" class="btn btn-primary">Submit</button>
    </div> -->
  </div>

  <div id="addcar" class="modal">
  <br><br>
   <div class="modal-content">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h4>Add New Vehicle</h4>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('vehicles.index') }}"> Back</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(array('route' => 'vehicles.store','method'=>'POST')) !!}
         @include('vehicles.form')
    {!! Form::close() !!}
 </div>
 </div>
   @endforeach

<script>
$(document).ready(function(){
  $("#search_text").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#cartable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

    {!! $vehicles->render() !!}
@endsection
