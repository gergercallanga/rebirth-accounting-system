


<div class="container" style="width: 90%">


     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
        <div class="input-field">
            <strong>Customer ID:</strong>
                 {!! Form::number('customer_id', null, array('class' => 'form-control')) !!}
              <!--  {!! Form::select('name', App\Customer::pluck('name','id'),null,array('class'=>'form-control'))!!} -->

        </div>
        </div>
    </div>

     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Type/Brand:</strong>
            {!! Form::text('type', null, array('placeholder' => 'Type/Brand','class' => 'form-control')) !!}
        </div>
    </div>

     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Model:</strong>
            {!! Form::text('model', null, array('placeholder' => 'Model','class' => 'form-control')) !!}
        </div>
    </div>

     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Plate:</strong>
            {!! Form::text('plate', null, array('placeholder' => 'Plate','class' => 'form-control')) !!}
        </div>
    </div>

     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Color:</strong>
            {!! Form::text('color', null, array('placeholder' => 'Color','class' => 'form-control')) !!}
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>


</div>

 

<script>
      $(document).ready(function() {
    $('select').material_select();
  });
</script>
    