@extends('layouts.default')
 
@section('content')

 
 <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a>

   <!-- Modal Structure -->
 <div id="modal1" class="modal">
    <div class="modal-content">
      <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Vehicles</h2>
            </div>
        </div>
    </div>

    <div class="row">
       
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ID:</strong>
                {{ $vehicle->id}}
            </div>
        </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Customer ID:</strong>
                {{ $vehicle->customer_id}}
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Type:</strong>
                {{ $vehicle->type}}
            </div>
        </div>
   

        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Model:</strong>
                    {{ $vehicle->model}}
                </div>
            </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Plate:</strong>
                    {{ $vehicle->plate}}
                </div>
            </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Color:</strong>
                    {{ $vehicle->color}}
                </div>
            </div>
            
        </div>
    </div>
    <div class="modal-footer">
      <a href="{{ route('vehicles.index') }}" class="modal-action modal-close waves-effect waves-green btn-flat ">Back</a>
       <!--  <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('vehicles.index') }}"> Back</a>
            </div> -->
    </div>
  </div>

   

<!-- 'id','customer_id', 'type', 'model', 'plate','color' -->

@endsection