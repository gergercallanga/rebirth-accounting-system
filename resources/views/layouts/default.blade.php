<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>REASITS</title>

    <link href="{{asset('css/app.css')}}" rel="stylesheet"> 
    <link href="{{ asset ('css/materialize.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="{{asset('sweet_alert.min.js')}}"></script>
    
    <link rel="stylesheet" type="text/css" href="{{asset('DataTables/css/dataTables.foundation.css')}}"/>

    <script type="text/javascript" src="{{asset('DataTables/js/jquery.dataTables.js')}}"></script>
  

<!-- jquery-3.2.1.min -->

 <script>
      $(document).ready(function() {
    $('select').material_select();
  });
          
</script>

<script>
      $(document).ready(function(){
         $('.modal').modal();
      });

      $('#modal1').modal('open');
      $('#modal1').modal('close');

      $('.modal').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        startingTop: '4%', // Starting top style attribute
        endingTop: '10%', // Ending top style attribute
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
          alert("Ready");
          console.log(modal, trigger);
        },
        complete: function() { alert('Closed'); } // Callback for Modal close
      });
      
</script>

<script type="text/javascript">

  window.setTimeout(function() {
    $(".alert").fadeTo(200, 100).slideUp(200, function(){
        $(this).remove();
    });
}, 4000);

</script>

</head>


 <style type="text/css">
      .no-js #loader { display: none;  }
      .js #loader { display: block; position: absolute; left: 100px; top: 0; }
      .se-pre-con {
        position: absolute;
        background-position: top center;
        margin: 0;
        left: 38%;
        top: 25%;
        width: 25%;
        opacity: 0.5;
        float: center;
        -webkit-animation: spin 4s linear infinite;
        animation: spin 4s linear infinite;
      }

      @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
      }

      @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
      }

  nav{
    background-color:  #850202
  }
    .hoverTable{
        width:100%; 
        border-collapse:collapse; 
    }
    .hoverTable td{ 
        padding:7px; border:#4e95f4 1px solid;
    }
    /* Define the default color for all the table rows */
    .hoverTable tr{
        background: #f5f5f5;
    }
     .hoverTable tr:hover {
          background-color: #850202;
          color: white;
    }

    body {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
  }

    .side-nav>li>a:hover {
          background-color: #891818 !important;
          border-right: 5px solid #bfb5b5;
        }
    .hoverinv>li>a:hover {
          background-color: #891818 !important;
          border-right: 5px solid #bfb5b5;
        }
    .hoverspan>span:hover {
          background-color: #891818 !important;
          border-right: 5px solid #bfb5b5;
        }

  main {
    flex: 1 0 auto;
  }

   header, main, footer {
      padding-left: 300px;
    }

    @media only screen and (max-width : 992px) {
      header, main, footer {
        padding-left: 0;
      }
    }
      
</style>

<script>
      $(window).on('load',function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");
    $('#body').fadeIn("slow").css("display","block");
    $('.page-footer').fadeIn("slow").css("display","block");

    });
</script>

<body>


<!-- 
<script src="{{url('/js/app.js')}}"></script> -->



<ul id="slide-out" class="side-nav fixed" style="width:250px; background-color: #2a2a2a;">
  <li>
    <div class="nav-wrapper" style="border: 0; background-color: #2a2a2a;height: 64px;">

    <div class="site_title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="/home"><img src="{{asset('images\rb.png')}}" alt="Rebirth Admin" style=" margin-top: 10px; height: 45px;border-radius: 20px;"></a>&nbsp;&nbsp;<span style="color: white; font-size: 30px;"><b>REASITS</b></span></div>
    </div>
  
    <div class="user-view">
    <div class="background">
        <img src="{{asset('images\1.jpg')}}">
    </div>
    <a class="tooltipped" data-tooltip="View Profile"  data-position="right" href="/userprofiles"><img class="circle" src="{{ url('/images/'.Auth::user()->avatar) }}"></a>
    <a href="#!name"><span class="white-text name">{{Auth::user()->username}}</span></a>
    <a href="#!email"><span class="white-text email">{{Auth::user()->email}}</span></a>
    </div>
    </li>
  <li><a href="/customers" class="waves-effect waves-red" style="color: #F5F8FA;"><i class="material-icons" style="margin-right: 10px; margin-left:-15px; color: #F5F8FA;">people</i>Customers</a></li>
    <li><a href="/admin" class="waves-effect waves-red" style="color: #F5F8FA;"><i class="material-icons" style="margin-right: 10px; margin-left:-15px; color: #F5F8FA;">accessibility</i>Roles and Permissions</a></li>
    <li><a href="/vehicles" class="waves-effect waves-red" style="color: #F5F8FA;"><i class="material-icons" style="margin-right: 10px; margin-left:-15px; color: #F5F8FA;">airport_shuttle</i>Vehicles</a></li>
    <li><a href="/maintenances" class="waves-effect waves-red" style="color: #F5F8FA;"><i class="material-icons" style="margin-right: 10px; margin-left:-15px; color: #F5F8FA;">access_alarms</i>Maintenance Schedule</a></li>
    <li><a href="/services" class="waves-effect waves-red" style="color: #F5F8FA;"><i class="material-icons" style="margin-right: 10px; margin-left:-15px; color: #F5F8FA;">room_service</i>Services</a></li>
     <ul class="collapsible hoverinv" data-collapsible="accordion" style="margin-right: 10px; margin-left:-15px;">
      <li>
        <div class="collapsible-header hoverspan"><i class="material-icons" style="margin-right: 8px;margin-left: 14px; color:#F5F8FA; font-size: bold;">assignment</i><span style="color:white;">Inventory</span><i class="material-icons" style="color: white;">arrow_drop_down</i></div>
        <div class="collapsible-body" style="display: none; margin-left: 10px;">
          <ul class="hoverinv" style="background-color: #2A2A2A;">
          <li><a href="/inventories" style="font-size: 12px; color:#F5F8FA;"><i class="tiny material-icons" style="margin-right: 10px; margin-left:15px;color:#F5F8FA;">create_new_folder</i>Inventory Master List</a></li>
          <li><a href="/inventory_receiving" style="font-size: 12px;color:#F5F8FA;"><i class="material-icons" style="margin-right: 10px; margin-left:15px;color:#F5F8FA;">vertical_align_bottom</i>Inventory Receiving</a></li>
          <li><a href="/inventory_conversion" style="font-size: 12px;color:#F5F8FA;"><i class="tiny material-icons" style="margin-right: 10px; margin-left:15px;color:#F5F8FA;">sync</i>Inventory Conversion</a></li>
          <li><a href="/itemqty_adjustment" style="font-size: 12px;color:#F5F8FA;"><i class="tiny material-icons" style="margin-right: 10px; margin-left:15px;color:#F5F8FA;">layers</i>Quantity Adjustment</a></li>
          <li><a href="/itemcost_adjustment" style="font-size: 12px;color:#F5F8FA;"><i class="tiny material-icons" style="margin-right: 10px; margin-left:15px;color:#F5F8FA;">local_atm</i>Cost Adjustment</a></li>
          
          </ul>
    </div>
      </li>
    </ul>

    <li><a href="/calendar/show" class="waves-effect waves-red" style="color:#F5F8FA;"><i class="material-icons" style="margin-right: 10px;color:#F5F8FA; margin-left:-15px;">perm_contact_calendar</i>Calendar</a></li>

     <li><a href="/suppliers" class="waves-effect waves-red" style="color:#F5F8FA;"><i class="material-icons" style="margin-right: 10px; color:#F5F8FA; margin-left:-15px;">account_box</i>Suppliers</a></li>

     </ul>
<div class="navbar-fixed">
  <nav>
    <div class="nav-wrapper">
    <a><i class="material-icons button-collapse" data-activates="slide-out">menu</i></a>
      <a href="/home" class="brand-logo"><i class="material-icons">home</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="sass.html"><i class="material-icons">search</i></a></li>
        <li><a href="badges.html"><i class="material-icons">view_module</i></a></li>
        <li><a href="collapsible.html"><i class="material-icons">refresh</i></a></li>
        <li><a href="mobile.html"><i class="material-icons">more_vert</i></a></li>
        <li><a href="mobile.html"><i class="material-icons ">account_circle</i></a></li>
      </ul>
    </div>

  </nav>
</div>




<main class="#eceff1 blue-grey lighten-5" id="body" style="display: none;">
  <div class="container" style="width: 100%;">
    @include('partials.alert')
    @yield('content')
</div>

</main>


<footer class="page-footer" style="background-color:  #850202; display: none;">
      <div class="container">
        © 2018 Copyright 
        <a class="grey-text text-lighten-4 right" href="http://www.rebirth-car.com/" target="_blank">REBIRTH CAR COATING CENTER</a>
      </div>
</footer>

<div class="back">
  <img src="{{asset('images/rb.png')}}" class="se-pre-con">
</div>


</body>
<script>
    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 240
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        }
    );
</script>
</html>