<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/userManagement.css') }}">
</head>
<body>
<a class="btn"
   onclick="event.preventDefault();
   document.getElementById('logout-form').submit();">
    Logout
</a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<div>

    @yield('content')
</div>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

<script>
    $(document).ready(function(){
       @if(\Session::has('success'))
            var $toastContent = $('<ul>' +
                '                <li>{!! \Session::get('success') !!}</li>' +
                '        </ul>')
                    .add($('<button class="btn-flat toast-action" onclick="Materialize.Toast.removeAll()">Close</button>'));
            Materialize.toast($toastContent, 10000);
        @endif

     @if (count($errors) > 0)
            var $toastContent = $('<ul>' +
                '            @foreach ($errors->all() as $error)' +
                '                <li>{{ $error }}</li>' +
                '            @endforeach' +
                '        </ul>')
                    .add($('<button class="btn-flat toast-action" onclick="Materialize.Toast.removeAll()">Close</button>'));
            Materialize.toast($toastContent, 10000);
        @endif
    });
</script>
</body>
</html>
