<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/userManagement.css') }}">
</head>
<body>
<a class="btn"
   onclick="event.preventDefault();
   document.getElementById('logout-form').submit();">
    Logout
</a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<div>

    @yield('content')
</div>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

<script>
    $(document).ready(function() {
        $('ul.tabs').tabs();
        $('.modal').modal(
            {
                dismissible: false, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                inDuration: 300, // Transition in duration
                outDuration: 200, // Transition out duration
                startingTop: '4%', // Starting top style attribute
                endingTop: '10%', // Ending top style attribute
                ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    $('.modal').removeData('.modal')
                },
                complete: function() {
                    var div= document.createElement("div");
                    div.className += "overlay";
                    document.body.appendChild(div);
                    location.reload();
                  } // Callback for Modal close
            }
        );

        Materialize.updateTextFields();

        $('#user_add').click(function(){

            $("#user_modal").modal('open');
            $('#user_form_tab').tabs('select_tab',"new_user_tab");
        });

        $('#permission_add').click(function(){

            $("#permission_modal").modal('open');
        });

        $('#role_add').click(function(){

            $("#role_modal").modal('open');
            $('#role_form_tab').tabs('select_tab',"new_role_tab");
        });


        $('button[name="user_edit"]').click(function(){
            $.ajax({
                url: "admin/users/"+$(this).attr('id')+"/edit",
                type: "GET",
                btn_data: $(this),
                success: function(response)
                {
                    $("#user_form").attr("action", "admin/users/" + this.btn_data.attr('id'));
                    $("#user_form").attr("method", "POST");
                    $("#user_form  #fname").val(response.fname);
                    $("#user_form  #lname").val(response.lname);
                    $("#user_form  #mname").val(response.mname);
                    $("#user_form  #email").val(response.email);
                    $("#user_form  #contact_num").val(response.contact_num);
                    $("#user_form  #username").val(response.username);
                    var tbody_content = null;

                  @foreach(json_decode($role, true) as $roles)
                    tbody_content +=
                        "                            <tr>\n" +
                        "                                <td>{{ $roles['name'] }}</td>\n" +
                        "                                <td>\n" +
                        "                                    <div class=\"row\">\n" +
                        "                                        <button class=\"btn col s6 push-s3 pull-s3\">View Permissions</button>\n" +
                        "                                        <div class=\"switch col s12\">\n" +
                        "                                            <label>\n" +
                        "                                                Off\n" +
                        (_isContains(response.roles, "id" ,"{{$roles['id']}}")
                            ? "                                              <input type=\"checkbox\" name=\"role_chbox[]\" id=\"role_chbox[]\" value=\"{{$roles['id']}}\" checked>\n"
                            :"                                               <input type=\"checkbox\" name=\"role_chbox[]\" id=\"role_chbox[]\" value=\"{{$roles['id']}}\">\n")
                       +
                        "                                                <span class=\"lever\"></span>\n" +
                        "                                                On\n" +
                        "                                            </label>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </td>\n" +
                        "                            </tr>\n"
                  @endforeach

                    $('#user_form_role_tbody').html(tbody_content);
                    Materialize.updateTextFields();
                    $("#user_form").append('<input name="_method" type="hidden" value="PUT">');
                    $("#user_modal").modal('open');
                    $('#user_form_tab').tabs('select_tab',"new_user_tab");

                }, error: function(xhr, ajaxOptions, thrownError){
                    // alert(xhr.status);
                    alert(xhr.status+" "+thrownError);
                }
            });

        });
        $('button[name="role_edit"]').click(function(){
            $.ajax({
                url: "admin/roles/"+$(this).attr('id')+"/edit",
                type: "GET",
                success: function(response)
                {   response = response[0]; //to adjust the json message
                    $("#role_form").attr("action", "admin/roles/" + response.id);
                    $("#role_form").attr("method", "POST");
                    $("#role").val(response.name);
                    var tbody_content = null;

                    @foreach(json_decode($permission, true) as $permissions)
                        tbody_content +=
                            "                            <tr>\n" +
                        "                                <td>{{$permissions['name']}}</td>\n" +
                        "\n" +
                        "                                <td>\n" +
                        "                                    <div class=\"row\">\n" +
                        "                                        <button class=\"btn col s6 push-s3 pull-s3\">View Roles with this Permission</button>\n" +
                        "                                        <div class=\"switch col s12\">\n" +
                        "                                            <label>\n" +
                        "                                                Off\n"+
                        (_isContains(response.permissions, "id" ,"{{$permissions['id']}}")
                            ? "<input type=\"checkbox\" name=\"permission_chbox[]\" id=\"permission_chbox[]\"\n" +
                            "                   value=\"{{$permissions['id']}}\" checked='true'>\n" +
                            "                      <span class=\"lever\"></span>\n"
                            : "<input type=\"checkbox\" name=\"permission_chbox[]\" id=\"permission_chbox[]\"\n" +
                "                   value=\"{{$permissions['id']}}\">\n" +
                "                        <span class=\"lever\"></span>\n") +
                        "                                                On\n" +
                        "                                            </label>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </td>\n" +
                        "                            </tr>\n"
                    @endforeach

                    $('#role_form_tbody').html(tbody_content);

                    $("#role_form").append('<input name="_method" type="hidden" value="PUT">');
                    $("#role_modal").modal('open');

                }, error: function(xhr, ajaxOptions, thrownError){
                    // alert(xhr.status);
                    alert(xhr.status+" "+thrownError);
                }
            });

        })

        $('#user_modal_next').click(function () {
            $('#user_form_tab').tabs('select_tab',"sel_role_tab");
        });

        $('#user_modal_back').click(function () {
            $('#user_form_tab').tabs('select_tab',"new_user_tab");
        });

        $('button[name="permission_edit"]').click(function(){
            $.ajax({
                url: "admin/permissions/"+$(this).attr('id')+"/edit",
                type: "GET",
                success: function(response)
                {
                    alert(JSON.stringify(response))
                }, error: function(xhr, ajaxOptions, thrownError){
                    // alert(xhr.status);
                    alert(xhr.status+" "+thrownError);
                }
            });

        });
        $('button[name="permission_delete"]').click(function(){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "admin/permissions/"+$(this).attr('id'),
                type: "GET",
                success: function(response)
                {
                    $("#permission_name_lbl").text(response.name);
                    $("#permission_modal_delete").modal("open");
                }, error: function(xhr, ajaxOptions, thrownError){
                    // alert(xhr.status);
                    alert(xhr.status+" "+thrownError);
                }
            });

        });

        $('button[name="user_deactivate"]').click(function(){

            $.ajax({
                url: "admin/users/" + $(this).attr('id'),
                type: "GET",
                btn_data: $(this),
                success: function(response)
                {   alert(JSON.stringify(response));
                    $("#activate_msg").text(this.btn_data.text().toLowerCase());
                    $("#user_cred").text(response.fname + " " + response.lname);
                    $("#user_form_activation #user_id").val(this.btn_data.attr('id'));
                    $("#user_form_activation #activate_btn").text(this.btn_data.text());
                    $("#user_toggle_activation_modal").modal("open");
                }, error: function(xhr, ajaxOptions, thrownError){
                    // alert(xhr.status);
                    alert(xhr.status+" "+thrownError);
                }
            });

        });


        $('#user_form_submit').click(function () {
            $('#loaders_modal').removeClass('hide');
          @if(count($errors) < 0)
            var $toastContent = $("<span>Action Sucess!</span>").add($('<button class="btn-flat toast-action" onclick="Materialize.Toast.removeAll()">Close</button>'));;
            Materialize.toast($toastContent, 10000);
          @endif
        });
        $('#fname, #lname').on('input', function(){

            $('#username').val((($('#fname').val() +'.'+ $('#lname').val())).toLowerCase());
            Materialize.updateTextFields();
        });

        $('.tab_href').click(function(){

            var tab_select = $(this).attr('href');
            tab_select = tab_select.replace('#','');
            localStorage.setItem('tab_select',tab_select);

        });

        if(typeof  localStorage.getItem('tab_select') !== 'undefined') {
            $('ul.tabs').tabs('select_tab', localStorage.getItem('tab_select'));
        }
     @if (count($errors) > 0)

        var $toastContent = $('<ul>' +
            '            @foreach ($errors->all() as $error)' +
            '                <li>{{ $error }}</li>' +
            '            @endforeach' +
            '        </ul>')
                .add($('<button class="btn-flat toast-action" onclick="Materialize.Toast.removeAll()">Close</button>'));
        Materialize.toast($toastContent, 10000);

     @endif

      @if(\Session::has('success'))
         var $toastContent = $('<ul>' +
             '                <li>{!! \Session::get('success') !!}</li>' +
         '        </ul>')
             .add($('<button class="btn-flat toast-action" onclick="Materialize.Toast.removeAll()">Close</button>'));
            Materialize.toast($toastContent, 10000);
      @endif

    });
    function _isContains(json_data, key ,value) {
        var hasMatch =false;

        for (var index = 0; index < json_data.length; ++index) {

            var animal = json_data[index];

            if(animal[key] == value){
                hasMatch = true;
                break;
            }
        }
        return hasMatch;
    }
</script>

</body>
</html>
