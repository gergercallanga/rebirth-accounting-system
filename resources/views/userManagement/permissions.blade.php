<button class="btn" id="permission_add">Add New Permission</button>
<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>

            <th>Permissions</th>
            <th>Roles Acquired</th>
            @if(auth()->user()->can('edit permissions') || auth()->user()->can('delete permissions') )
                <th>Actions</th>
            @endif
        </tr>
        </thead>
        <tbody>

            @foreach(json_decode($permission, true) as $permissions)
            <tr>
                <td>{{$permissions['name']}}</td>
                <td>
                    <ul class="collection">
                        @foreach($permissions['roles'] as $roles_permissions)
                               <li class="collection-item"> {{$roles_permissions['name']}}</li>
                        @endforeach
                    </ul>
                </td>
                @if(auth()->user()->can('edit permissions') || auth()->user()->can('delete permissions') )
                    <td>
                        @can("edit permissions")
                            <button id="{{$permissions['id']}}" name="permission_edit" class="btn">Edit</button>
                        @endcan
                        @can("delete permissions")
                            <button id="{{$permissions['id']}}" name="permission_delete" class="btn">Delete</button>
                        @endcan
                    </td>
                @endif

            </tr>
             @endforeach
        </tbody>
    </table>
</div>


<div class="modal" id="permission_modal" role="dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <h4> Add New Permission</h4>


        <form id="permission_form" class="form-horizontal" method="POST" action="{{ url('/admin/permissions') }}">
                {{ csrf_field() }}
            {{--{{ $errors->has('fname') ? ' has-error' : '' }}--}}

            <div class="input-field col s4">
                <input id="name" type="text" class="validate" name="name" value="{{ old('name') }}" autofocus>
                <label for="fname">Permission</label>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                   Add Permission
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </form>

    </div>

</div>


<div class="modal" id="permission_modal_delete" role="dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <h4> Delete Permission</h4>


        <form id="permission_delete" class="form-horizontal" method="DELETE" action="{{ url('/admin/permissions') }}">
            {{ csrf_field() }}
            {{--{{ $errors->has('fname') ? ' has-error' : '' }}--}}
            <input type="hidden" name="permission_id" id="permission_id">
            <span>Are you sure you want to delete this permission?</span>
            Permission: <span id="permission_name_lbl"></span>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Delete Permission
                </button>

            </div>
        </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>

</div>