<form id="user_form" class="form-horizontal" method="POST" action="{{ url('/admin/users') }}">
    <div id="new_user_tab" class="col s12">
        {{ csrf_field() }}
            {{--{{ $errors->has('fname') ? ' has-error' : '' }}--}}

            <div class="input-field col s4">
                <input id="fname" type="text" class="validate" name="fname" value="{{ old('fname') }}" autofocus>
                <label for="fname">First Name</label>
                @if ($errors->has('name'))
                    <span class="help-block">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                @endif
            </div>

            <div class="input-field col s4">
                <input id="lname" type="text" class="validate" name="lname" value="{{ old('lname') }}" >
                <label for="lname">Last Name</label>
            </div>

            <div class="input-field col s4">
                <input id="mname" type="text" class="validate" name="mname" value="{{ old('mname') }}" >
                <label for="mname">Middle Name/Initial</label>
            </div>

            <div class="input-field col s6">
                {{--         <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" >
                <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                {{--  @if ($errors->has('email'))
                      <span class="help-block">
                                              <strong>{{ $errors->first('email') }}</strong>
                                          </span>
                  @endif--}}
            </div>

            <div class="input-field col s6">
                <input id="contact_num" type="number" class="validate" name="contact_num" value="{{ old('contactnum') }}" >
                <label for="contact_num">Contact Number</label>
                {{--@if ($errors->has('email'))
                    <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                @endif--}}
            </div>

            <div class="input-field col s12">
                <label for="username">Username</label>
                <input id="username" type="text" max="200" class="validate" name="username" >

                @if ($errors->has('username'))
                    <span class="help-block">
                                                                <strong>{{ $errors->first('username') }}</strong>
                                                            </span>
                @endif
            </div>

            <div class="modal-footer">

                <button id="user_modal_next" type="button" class="waves-effect btn red darken">
                   Next
                </button>
                <button id="user_modal_cancel" type="button" class="waves-effect btn red darken modal-action modal-close" data-dismiss="modal">Cancel</button>
            </div>

    </div>

    <div id="sel_role_tab" class="col s12">

        <div class="form-group">

            <div class="col-md-8">
                <div class="table-responsive">
                    <table class="table centered">
                        <thead>
                        <tr>
                            <th>Roles</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="user_form_role_tbody">
                        @foreach(json_decode($role, true) as $roles)
                            <tr>
                                <td>{{ $roles['name'] }}</td>
                                <td>
                                    <div class="row">
                                        <button class="btn col s6 push-s3 pull-s3">View Permissions</button>
                                        <div class="switch col s12">
                                            <label>
                                                Off
                                                <input type="checkbox" name="role_chbox[]" id="role_chbox[]" value="{{$roles['id']}}">
                                                <span class="lever"></span>
                                                On
                                            </label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div class="modal-footer">

            <button id="user_form_submit" type="submit" class="waves-effect btn red darken col s3 push-s4">
               Submit
            </button>
            <button id="user_modal_back" type="button" class="waves-effect btn red darken" data-dismiss="modal">Back</button>
            <button id="user_modal_cancel" type="button" class="waves-effect btn red darken modal-action modal-close" data-dismiss="modal">Cancel</button>
        </div>
    </div>

</form>



