
<button class="waves-effect btn red darken-2" id="user_add"> Add New User</button>
<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>

            <th>First Name</th>
            <th>Last Name</th>
            <th>Middle Name/Initial</th>
            <th>Email</th>
            <th>Contact Number</th>
            <th>Roles</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach(json_decode($user_roles, true) as $users)
            <tr>
                <td>{{ $users['fname']}}</td>
                <td>{{ $users['lname']}}</td>
                <td>{{ $users['mname']}}</td>
                <td>{{ $users['email']}}</td>
                <td>{{ $users['contact_num']}}</td>
                <td>
                    <ul class="collection">
                    @foreach($users['roles'] as $roles)
                        <li class="collection-item">
                            {{$roles['name']}}
                        </li>
                    @endforeach
                    </ul>
                </td>
                <td>@can('create users')
                        <button class="waves-effect btn waves-effect btn-warning" id="{{Crypt::encrypt($users['id'])}}"
                                name="user_edit">Edit
                        </button>
                    @endcan
                    @if($users['status']==1)
                    <button class="waves-effect btn waves-effect btn-danger" id="{{Crypt::encrypt($users['id'])}}"
                            name="user_deactivate">Deactivate</button>
                    @else
                     <button class="waves-effect btn waves-effect btn-danger" id="{{Crypt::encrypt($users['id'])}}"
                                name="user_deactivate">Activate</button>
                    @endif
                </td>
            </tr>


        @endforeach
        </tbody>
    </table>
</div>


<div class="modal" id="user_modal" role="dialog">

    <div class="row">

            <ul class="tabs tabs-fix-width" id="user_form_tab">
                <li class="tab col s5 offset-s1"><a class="active" href="#new_user_tab">Add New User</a></li>
                <li class="tab col s5"><a href="#sel_role_tab">Select Role</a></li>
            </ul>

        <div id="loaders_modal" class="progress hide">
            <div class="indeterminate"></div>
        </div>

        <div class="modal-content">
            @include("userManagement.forms.user_create_edit")
        </div>

    </div>
</div>

<div class="modal" id="user_toggle_activation_modal" role="dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <h4> Activate/Reactivate User</h4>


        <form id="user_form_activation" class="form-horizontal" method="POST" action="{{ url('/admin/users/toggle_activation') }}">
            {{ csrf_field() }}
            {{--{{ $errors->has('fname') ? ' has-error' : '' }}--}}
            <input type="hidden" name="user_id" id="user_id">

            <h5 class="col s12">Are you sure you want to <span id="activate_msg"></span> this permission?</h5>

            <h6 >User:<span id="user_cred"></span></h6>

            <input type="hidden" id="user_id" name="user_id">

            <div class="modal-footer">
                <button id="activate_btn" type="submit" class="btn btn-primary">
                    <!-- Custom Message Here-->
                </button>

            </div>
        </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>

</div>




