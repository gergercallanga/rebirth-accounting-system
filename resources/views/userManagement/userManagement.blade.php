@extends("userManagement.layouts.parent")
@section('title', '| User Management')
@section("content")
    <div class="row">
    <ul class="tabs tabs-fixed-width" style="overflow-x: hidden">

            <li class="tab"><a class="tab_href" href="#users_tab">Users</a></li>
            <li class="tab"><a class="tab_href" href="#roles_tab">Roles</a>
            <li class="tab"><a class="tab_href" href="#permission_tab">Permissions</a></li>

    </ul>
    <div id="users_tab" class="col s12">
        <h3>Users</h3>
        @include("userManagement.user")
    </div>
    <div id="roles_tab" class="col s12">
        <h3>Roles</h3>
        @include("userManagement.roles")
    </div>
    <div id="permission_tab" class="col s12">
        @include("userManagement.permissions")
    </div>
    </div>


@endsection