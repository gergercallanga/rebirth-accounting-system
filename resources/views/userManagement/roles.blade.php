<div class="container">
    <button class="waves-effect btn red darken-2" id="role_add"> Add New Role</button>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Role</th>
                <th>Associated Permissions</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach(json_decode($newroles, true) as $roles)
                <tr>
                    <td>{{ $roles['name']}}</td>
                    <td>
                        <ul  class="collection">
                            @foreach($roles['permissions'] as $role_permission)
                                <li class="collection-item">
                                  {{$role_permission['name']}}
                                <li>
                            @endforeach
                        </ul>
                    </td>
                    <td><button class="btn"  id="{{$roles['id']}}"
                                name="role_edit">Edit</button>
                        <button class="btn">Delete</button></td>
                </tr>


           @endforeach
            </tbody>
        </table>
    </div>
</div>


<div class="modal" id="role_modal" role="dialog">
    <div class="row">

        <ul class="tabs tabs-fix-width" id="role_form_tab">
            <li class="tab col s5 offset-s1"><a class="active" href="#new_role_tab">Add New Role</a></li>
        </ul>

        <div id="loaders_modal" class="progress hide">
            <div class="indeterminate"></div>
        </div>

        <div class="modal-content">
            @include("userManagement.forms.role_create_edit")
        </div>

    </div>
</div>