<div class="container" style="width:90%">
  
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class=" input-field form-group">
            {!! Form::text('name', null, array('class' => 'form-control')) !!}
            <label for="name">Full Name</label>
        </div>
    </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="input-field form-group">
            {!! Form::text('nickname', null, array('class' => 'form-control')) !!}
            <label for="nickname">Nickname</label>
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class=" input-field form-group">
            {!! Form::text('address', null, array('placeholder' => 'Street/Subdivision Barangay City Province/State Zip Code','class' => 'form-control')) !!}
            <label for="address">Address</label>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="input-field form-group">
            {!! Form::number('telephone', null, array('class' => 'form-control')) !!}
             <label for="telephone">Telephone Number</label>
        </div>
    </div>

     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="input-field form-group">
            {!! Form::number('mobile', null, array('class' => 'form-control')) !!}
             <label for="mobile">Mobile Number</label>
        </div>
    </div>

     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="input-field form-group">
            {!! Form::email('email', null, array('class' => 'form-control')) !!}
             <label for="email">Email Address</label>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
            
    </div>


</div>  

