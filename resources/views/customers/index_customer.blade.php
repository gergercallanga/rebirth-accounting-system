@extends('layouts.default')
@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h4>Customer Information</h4>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-lg-12 margin-tb" style="margin-top: -30px;">
            <div class="input-field col s5" style="margin-top: 25px;">
                <a class="waves-effect waves-light btn modal-trigger tooltipped modal-trigger" data-tooltip="Insert New Record" href="#addcus"><i class="material-icons">add</i></a>
            </div>
        <div class="input-field col s5 pull-right">
          <i class="material-icons prefix">search</i>
          {!! Form::text('search_text', null, array('class' => 'form-control','id'=>'search_text')) !!}
          <label for="search_text">Search</label>
        </div>  
    </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered hoverTable" id="example">
    <thead>
         <tr class="#bdbdbd grey darken-2" style="color: white;">
            <th>Name</th>
            <th>Nickname</th>
            <th>Address</th>
            <th>Telephone</th>
            <th>Mobile</th>
            <th>Email</th>
        </tr>
    </thead>
 
<tbody id="custtable">
     @foreach ($customers as $customer)
    <tr class="tooltipped" data-tooltip="Click to see records" >
  
        <td><a class= "modal-trigger" href="{{ route('customers.show', $customer->id) }}"></a>{{ $customer->name}}</td>
        <td>{{ $customer->nickname}}</td>
        <td>{{ $customer->address}}</td>
        <td>{{ $customer->telephone}}</td>
        <td>{{ $customer->mobile}}</td>
        <td>{{ $customer->email}}</td>

    </tr>
    @endforeach
</tbody>
   
    </table>
</div>
 @foreach ($customers as $customer)
 
  <!-- Modal -->
<div id="show{{$customer->id}}" class="modal" >
  <div class="modal-content">

    <!-- Modal content-->
    
      <div class="modal-header">
        <h4 class="modal-title">Customer Full Details</h4>
        <div class="pull-right">
        <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Edit" href="#edit{{$customer->id}}"><i class="material-icons">edit</i></a>

        {!! Form::open(['method' => 'DELETE','route' => ['customers.destroy', $customer->id],'style'=>'display:inline'])!!}
            {{ Form::button('<i class="material-icons">delete</i>', array('type' => 'submit','class' => 'waves-effect waves-light red btn modal-trigger tooltipped','data-tooltip' => 'Delete'))}}
        {!! Form::close() !!}

        </div>
      </div>

      <div class="modal-body">
        <div class="row">


        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong style=" font-weight: bold">Customer ID:</strong>
                {{ $customer->id}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong style=" font-weight: bold">Name:</strong>
                {{ $customer->name}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong style=" font-weight: bold">Nickname:</strong>
                {{ $customer->nickname}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong style=" font-weight: bold">Address:</strong>
                {{ $customer->address}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong style=" font-weight: bold">Telephone:</strong>
                {{ $customer->telephone}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong style=" font-weight: bold">Mobile:</strong>
                {{ $customer->mobile}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong style=" font-weight: bold">Email:</strong>
                {{ $customer->email}}
            </div>
            <hr>
        </div>
    
        <div>
            <h5 style=" color: #4CAF50; font-weight: bold;">Car Records</h5>
            <div class="pull-right">
             <a class="waves-effect waves-light btn modal-trigger tooltipped modal-trigger" data-tooltip="Insert New Record" href="/vehicles/#addcar"><i class="material-icons">add</i></a>
             </div>
             <br>
             <br>
             <div class="table-responsive">
             <table class="table table-bordered">
                <tr>
            
                    <th>ID</th>
                    <th>Type</th>
                    <th>Model</th>
                    <th>Plate</th>
                    <th>Color</th>
                </tr>
                @foreach($customer->vehicles as $vehicles)
                <tr>
                           <td>{{$vehicles->id}}</td>
                           <td> {{$vehicles->type}}</td>
                           <td> {{$vehicles->model}}</td>
                           <td> {{$vehicles->plate}}</td>
                           <td> {{$vehicles->color}}</td>
                </tr>
                @endforeach
                
            </table>
            </div>
        </div>

        
    </div>
      <div class="pull-right">
        <button type="button" class="btn btn-default modal-action modal-close" >Close</button>
        </div>
        <br>
      </div>
   

</div>
</div>


<!-- Modal Structure for Edit Customer -->
  <div id="edit{{$customer->id}}" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Details</h4>
            <div class="pull-right">
                    <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
            
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($customer, ['method' => 'PATCH','route' => ['customers.update', $customer->id]]) !!}
        @include('customers.form_customer')
    {!! Form::close() !!}
   
    </div>
 </div>



  @endforeach


<!-- Modal Structure for adding Customer -->
  <div id="addcus" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Insert New Record</h4>
            <div class="pull-right">
                    <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
            
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
     {!! Form::open(array('route' => 'customers.store','method'=>'POST')) !!}
         @include('customers.form_customer')
    {!! Form::close() !!}
   
    </div>
 </div>


    {!! $customers->render() !!}


<script>
    $(document).ready(function() {

        $('#example tr').click(function() {
            var href = $(this).find("a").attr("href");
        if(href) { 
                window.location=href;
            }
        });

    });


$(document).ready(function(){
  $("#search_text").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#custtable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});


</script>
@endsection