@extends('layouts.default')
@section('content')


<br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h4>Customer Records</h4>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('customers.index') }}"> Back</a>
            </div>
        </div>
    </div>

              <div class="card-group">

                <div class="col-lg-5">
                  <div class="card" style="height: 480px; border: 1px; border-color: #ededed;box-shadow: 3px 3px 5px #ededed;">
                    <div class="card-header d-flex align-items-center" style="color: #2a2a2a; background-color: #ededed; height: 40px;">

                      <h6 style="line-height: 40px;"><i class="material-icons" style="margin-top: 5px">perm_identity</i>&nbsp;&nbsp;Customer Info<a class="modal-trigger" href="#edit{{$customer->id}}"><i class="material-icons pull-right tooltipped"  data-tooltip="Update Info" style="margin-top: 5px; color: black;">create</i></a></h6>
                    </div>
                    <div class="card-body">
                         <div class="row">

                            <div class="block-section text-center">
                                <br>
                                    <img src="{{asset('images\man.png')}}" style="height: 60px;border-radius: 20px;" class="circle">
                                    <h5><strong>{{ $customer->name}}</strong></h5>
                            </div>

                            <center><table class="table table-striped table-vcenter responsive-table" style="width: 92%;">
                                        <tbody>
                                            <tr>
                                                <td class="text-right" style="width: 50%;"><strong>Customer ID</strong></td>
                                                <td>{{ $customer->id}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"> <strong>Nickname</strong></td>
                                                <td>{{ $customer->nickname}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>Address:</strong></td>
                                                <td>{{ $customer->address}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>Telephone no.</strong></td>
                                                <td> {{ $customer->telephone}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">  <strong>Mobile no.</strong></td>
                                                <td>  {{ $customer->mobile}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"> <strong>Email:</strong></td>
                                                <td>{{ $customer->email}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </center>
     
                                    
                                </div>
                            </div>

                           
                           
                        </div>

                    </div>
                  </div>

                <div class="col-lg-7">
                  <div class="card" style="height: 350px; border: 1; border-color: #ededed;box-shadow: 3px 3px 5px #ededed;">
                     <div class="card-header d-flex align-items-center" style="color: #2a2a2a; background-color: #ededed; height: 40px;">

                      <h6 style="line-height: 40px;"><i class="material-icons" style="margin-top: 5px">directions_car</i>&nbsp;&nbsp;Car Records</h6>
                    </div>
                    <div class="card-body">

                        <div>
                            <br>
                            <div class="pull-right" style="margin-right: 35px;">
                             <a class="waves-effect waves-light btn modal-trigger tooltipped modal-trigger" data-tooltip="Insert New Record" href="/vehicles/#addcar"><i class="material-icons">add</i></a>
                             </div>
                             <br>
                             <br>
                             <div class="table-responsive">
                             <center>
                                 <table class=" table striped highlight responsive-table" style="width:90%;">
                             <tbody>
                                  <tr class="#bdbdbd grey darken-2" style="color: white;">
                            
                                    <th>ID</th>
                                    <th>Type</th>
                                    <th>Model</th>
                                    <th>Plate</th>
                                    <th>Color</th>
                                </tr>
                                @foreach($customer->vehicles as $vehicles)
                                <tr>
                                           <td>{{$vehicles->id}}</td>
                                           <td> {{$vehicles->type}}</td>
                                           <td> {{$vehicles->model}}</td>
                                           <td> {{$vehicles->plate}}</td>
                                           <td> {{$vehicles->color}}</td>
                                </tr>
                                @endforeach
                                
                             </tbody>
                               
                            </table>
                             </center>
                            </div>
                        </div>


                    </div>
                  </div>
                </div>

              </div>

              <div class="card-group">
                <div class="col-lg-4">
                   <div class="card" style="height: 350px; border: 1px; border-color: #ededed;box-shadow: 3px 3px 5px #ededed;">
                    <div class="card-header d-flex align-items-center" style="color: white; background-color: #891717; height: 40px;">
                      <h4 style="line-height: 40px;">&nbsp;&nbsp;Card Title 3</h4>
                    </div>
                    <div class="card-body">
                    </div>
                  </div>
                </div>

                <div class="col-lg-4">
                  <div class="card" style="height: 300px; border: 1; border-color: #ededed;box-shadow: 3px 3px 5px #ededed;">
                    <div class="card-header d-flex align-items-center" style="color: white; background-color: #891717; height: 40px;">
                      <h4 style="line-height: 40px;">&nbsp;&nbsp;Card Title 4</h4>
                    </div>
                    <div class="card-body">
                    </div>
                  </div>
                </div>

                <div class="col-lg-4">
                  <div class="card" style="height: 300px; border: 1; border-color: #ededed;box-shadow: 3px 3px 5px #ededed;">
                    <div class="card-header d-flex align-items-center" style="color: white; background-color: #891717; height: 40px;">
                      <h4 style="line-height: 40px;">&nbsp;&nbsp;Card Title 5</h4>
                    </div>
                    <div class="card-body">
                    </div>
                  </div>
                </div>
              </div>
          
<!-- modal for edit -->
<div id="edit{{$customer->id}}" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Details</h4>
            <div class="pull-right">
                    <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
            
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($customer, ['method' => 'PATCH','route' => ['customers.update', $customer->id]]) !!}
        @include('customers.form_customer')
    {!! Form::close() !!}
   
    </div>
 </div>


@endsection