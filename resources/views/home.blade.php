<!DOCTYPE html>

<html>
<head>
    <title>Homepage</title>

 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{ asset ('css/materialize.min.css')}}" rel="stylesheet">
  <!--   <script src={{ asset ('js/materialize.min.js') }}></script> -->


  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>



</head>
<style>
  body{
    background-size: 100%
  }
  nav{
    background-color:  #850202
  }

  .collapsible-body:hover{
    display: block;
  }
</style>



<body background="{{asset('images\bg.jpg')}}">

<!-- <body> -->

     <ul id="slide-out" class="side-nav" style="width:250px;">
    <li><div class="user-view">
      <div class="background">
        <img src="{{asset('images\1.jpg')}}">
      </div>
      <a href="#!user"><img class="circle" src="{{ url('/images/'.Auth::user()->avatar) }}"></a>
      <a href="#!name"><span class="white-text name">{{Auth::user()->username}}</span></a>
      <a href="#!email"><span class="white-text email">{{Auth::user()->email}}</span></a>
    </div></li>
          <li><a href="/customers" class="waves-effect waves-red"><i class="material-icons" style="margin-right: 10px; margin-left:-15px;">people</i>Customers</a></li>
          <li><a href="/admin" class="waves-effect waves-red"><i class="material-icons" style="margin-right: 10px; margin-left:-15px;">accessibility</i>Roles and Permissions</a></li>
          <li><a href="/vehicles" class="waves-effect waves-red"><i class="material-icons" style="margin-right: 10px; margin-left:-15px;">airport_shuttle</i>Vehicles</a></li>
          <li><a href="/maintenances" class="waves-effect waves-red"><i class="material-icons" style="margin-right: 10px; margin-left:-15px;">access_alarms</i>Maintenance Schedule</a></li>
          <li><a href="/services" class="waves-effect waves-red"><i class="material-icons" style="margin-right: 10px; margin-left:-15px;">room_service</i>Services</a></li>
           <ul class="collapsible class="waves-effect waves-red"" data-collapsible="accordion" style="margin-right: 10px; margin-left:-15px;">
            <li>
        <div class="collapsible-header waves-effect waves-red"><i class="material-icons" style="margin-right: 8px;margin-left: 14px; color: #757575;">assignment</i>Inventory</a><i class="material-icons">arrow_drop_down</i></div>

        <div class="collapsible-body" style="display: none; margin-left: 10px;">
            <ul>
            <li><a href="/inventories" style="font-size: 12px;"><i class="tiny material-icons" style="margin-right: 10px; margin-left:15px;">create_new_folder</i>Inventory Master List</a></li>
            <li><a href="/inventory_receiving" style="font-size: 12px;"><i class="material-icons" style="margin-right: 10px; margin-left:15px;">vertical_align_bottom</i>Inventory Receiving</a></li>
            <li><a href="/inventory_conversion" style="font-size: 12px;"><i class="tiny material-icons" style="margin-right: 10px; margin-left:15px;">sync</i>Inventory Conversion</a></li>
            <li><a href="/itemqty_adjustment" style="font-size: 12px;"><i class="tiny material-icons" style="margin-right: 10px; margin-left:15px;">layers</i>Quantity Adjustment</a></li>
            <li><a href="/itemcost_adjustment" style="font-size: 12px;"><i class="tiny material-icons" style="margin-right: 10px; margin-left:15px;">local_atm</i>Cost Adjustment</a></li>
            </ul>
    </div>
      </li>
    </ul>


     <li><a href="/calendar/show" class="waves-effect waves-red"><i class="material-icons" style="margin-right: 10px; margin-left:-15px;">perm_contact_calendar</i>Calendar</a></li>

     <li><a href="/suppliers" class="waves-effect waves-red"><i class="material-icons" style="margin-right: 10px; margin-left:-15px;">account_box</i>Suppliers</a></li>

     <li><a href="/imageuploads" class="waves-effect waves-red"><i class="material-icons" style="margin-right: 10px; margin-left:-15px;">account_box</i>Image Uploads</a></li>
</ul>


  <nav>
    <div class="nav-wrapper">
    <a><i class="material-icons button-collapse" data-activates="slide-out">menu</i></a>
      <a href="/home" class="brand-logo"><i class="material-icons">home</i>Rebirth Car Coating</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="sass.html"><i class="material-icons">search</i></a></li>
        <li><a href="badges.html"><i class="material-icons">view_module</i></a></li>
        <li><a href="collapsible.html"><i class="material-icons">refresh</i></a></li>
        <li><a href="mobile.html"><i class="material-icons">more_vert</i></a></li>
        <li><a href="mobile.html"><i class="material-icons ">account_circle</i></a></li>
      </ul>
    </div>

  </nav>
<br>
 <!--  <div class="row">
    <div class="col s12 m3">
      <div class="card">
        <div class="card-image">
          <img src="{{asset('images\clients.png')}}">
          <span class="card-title">Customers</span> -->
       <!--    <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a>
        </div>
        <div class="card-content">
          <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col s12 m3">
      <div class="card">
        <div class="card-image">
          <img src="{{asset('images\1.jpg')}}">
          <span class="card-title">Customers</span>
      </div>
    </div>
  </div>
     <div class="col s12 m3">
      <div class="card">
        <div class="card-image">
          <img src="{{asset('images\1.jpg')}}">
          <span class="card-title">Customers</span>
      </div>
    </div>
  </div>
     <div class="col s12 m3">
      <div class="card">
        <div class="card-image">
          <img src="{{asset('images\1.jpg')}}">
          <span class="card-title">Customers</span>
      </div>
    </div>
  </div>
     <div class="col s12 m3">
      <div class="card">
        <div class="card-image">
          <img src="{{asset('images\1.jpg')}}">
          <span class="card-title">Customers</span>
      </div>
    </div>
  </div>
     <div class="col s12 m3">
      <div class="card">
        <div class="card-image">
          <img src="{{asset('images\1.jpg')}}">
          <span class="card-title">Customers</span>
      </div>
    </div>
  </div>
     <div class="col s12 m3">
      <div class="card">
        <div class="card-image">
          <img src="{{asset('images\1.jpg')}}">
          <span class="card-title">Customers</span>
      </div>
    </div>
  </div>
     <div class="col s12 m3">
      <div class="card">
        <div class="card-image">
          <img src="{{asset('images\1.jpg')}}">
          <span class="card-title">Customers</span>
      </div>
    </div>
  </div>
</div> 
 -->


</body>

<script>
    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 240
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        }
    );
     $(document).ready(function(){
    $('.collapsible').collapsible();
  });
  

</script>
<div class="container" style="width: 85%;">
    @yield('content')
</div>

<!-- <a class="btn" onclick="event.preventDefault();
   document.getElementById('logout-form').submit();">
    Logout
</a> -->
<!-- 
<form id="logout-form" action="http://localhost:8000/logout" method="POST" style="display: none;">
    <input type="hidden" name="_token" value="mVxbF8bOmfw7kr0AC8IyrcgH7HNJrdCkdK2h2YRI">
</form> -->
</html> 
