@extends('layouts.default')
@section('content')

<link href={{ asset('fullcalendar-3.8.2/fullcalendar.min.css') }} rel='stylesheet' />
<link href={{ asset('fullcalendar-3.8.2/materialFullCalendar.css') }} rel='stylesheet' />
<link href={{ asset('fullcalendar-3.8.2/fullcalendar.print.min.css') }} rel='stylesheet' media='print' />
<script src={{ asset('fullcalendar-3.8.2/lib/moment.min.js') }} ></script>

<script src={{ asset ('fullcalendar-3.8.2/fullcalendar.min.js') }}></script>
<style>
td{
  border-color: #000000
}
</style>



<br>
<script>
  $(document).ready(function() {
      $.ajax({
          url: "/calendar/viewMaintenance",
          type: "GET",
          success: function (response) {
             // alert(response);
              $('#calendar').fullCalendar({
                  header: {
                      left: 'prev,next today',
                      center: 'title',
                      right: 'month,agendaWeek,agendaDay'
                  },
                  navLinks: true, // can click day/week names to navigate views
                  selectable: true,
                  selectHelper: true,
                  /*select: function(start, end) {
                      var title = prompt('Event Title:');
                      var eventData;
                      if (title) {
                          eventData = {
                              title: title,
                              start: start,
                              end: end,  
                          };
                          $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                      }
                      $('#calendar').fullCalendar('unselect');
                  },*/
                  editable: true,
                  eventLimit: true, // allow "more" link when too many events
                  events: JSON.parse(response),
                  eventClick: function(calEvent, jsEvent, view) {
                     // window.open('/maintenances/' + calEvent.id, '_blank');
                       
                      $('#maintenance_modal'+calEvent.id).show();
                        // window.location.href = '#modal1'+calEvent.id;

                  


                  }
              });
          }
      });

  });

</script>


  <div id='calendar'></div>


@foreach($maintenance as $maintenancelist)  

  <!-- Modal Structure -->
  <div id="maintenance_modal{{$maintenancelist->id}}" class="modal modal-fixed-footer">
    <div class="modal-content">
       <div class="row">
         <div class="pull-left">
                  <h4>Schedule Information</h4>
         </div>
       </div>
<br>
       <div class="row">
    <form class="col s12">
   <div class="row" style="margin-top: -20px;">
    <div class="input-field col s4">
         <i class="material-icons prefix">build</i>
          <label for="">Maintenance ID:&nbsp;&nbsp;&nbsp; {{ $maintenancelist->id}}</label>
     </div>
  
    <div class="input-field col s4">
         <i class="material-icons prefix">face</i>
          <label for="">Customer ID:&nbsp;&nbsp;&nbsp; {{ $maintenancelist->customer_id}}</label>
     </div>

     <div class="input-field col s4">
         <i class="material-icons prefix">local_car_wash</i>
          <label for="">Vehicle ID:&nbsp;&nbsp;&nbsp;{{ $maintenancelist->vehicle_id}}</label>
     </div>
 </div>
 </div>

    <div class="row" style="margin-top: 25px;">

        <div class="input-field col s6">
              &nbsp&nbsp&nbsp&nbsp<i class="material-icons prefix">date_range</i>
              <label for="">&nbsp Schedule:&nbsp&nbsp&nbsp {{ $maintenancelist->sched_from}} - {{ $maintenancelist->sched_to}} </label>
        </div>

        <div class="input-field col s6">
             &nbsp&nbsp&nbsp<i class="material-icons prefix">access_time</i>
              <label for="">Time:&nbsp&nbsp&nbsp{{ $maintenancelist->from_time}} - {{ $maintenancelist->to_time}} </label>
        </div>


    </div> 

     <div class="row" style="margin-top: 25px;">

         <div class="input-field col s6">
            &nbsp&nbsp&nbsp&nbsp<i class="material-icons prefix">description</i>
              <label for="">&nbsp Description:&nbsp&nbsp&nbsp   {{ $maintenancelist->description}}</label>
        </div>
        
        <div class="input-field col s6">
             &nbsp&nbsp&nbsp<i class="material-icons prefix">local_offer</i>
              <label for="">Amount:&nbsp&nbsp&nbsp {{ $maintenancelist->amount}}</label>
        </div>

    </div> 

    <div class="row" style="margin-top: 25px;">

         <div class="input-field col s6">
            &nbsp&nbsp&nbsp&nbsp<i class="material-icons prefix">album</i>
              <label for="">&nbsp Status:&nbsp&nbsp&nbsp   {{ $maintenancelist->status}}</label>
        </div>
        
        <div class="input-field col s6">
             &nbsp&nbsp&nbsp<i class="material-icons prefix">event_note</i>
              <label for="">Note:&nbsp&nbsp&nbsp {{ $maintenancelist->note}}</label>
        </div>

    </div> 
    </div>
    <div class="modal-footer">
     <div class="pull-right">
        <!-- <a class="btn btn-primary" href="/calendar/show"> Back </a> -->
<a class="btn btn-primary" href="/calendar/show"> Back </a>


    </div>
    </div>
  </div>
@endforeach
@endsection