@extends('layouts.default')
@section('content')


	<div class="row">
	  <div class="col s12 m11">


	<div class="card"> 
        <div class="card-image">
         <img src="{{asset('images\bg.jpg')}}" style="height: 280px;">
          <span class="card-title">{{Auth::user()->fname}} {{Auth::user()->mname}} {{Auth::user()->lname}} &nbsp;&nbsp;<a href="#"><i class="material-icons tooltipped" data-tooltip="Edit Details" style="color:white;">create</i></a></span>
          <a class="modal-trigger z-depth-3 btn-large btn-floating halfway-fab tooltipped" href="#uploaddp" data-tooltip="Update picture" style="height: 120px; width: 120px"> <img class=" responsive-img dp modal-img halfway-fab " src="{{ url('/images/'.Auth::user()->avatar) }}" style="height: 120px; width: 120px"></a>
        </div>
        <br>
        <div class="card-content">
          <div class="row">


                            <center><table class="table table-striped table-vcenter" >
                                        <tbody>
                                            <tr>
                                                <td class="text-right" style="width: 50%;"><strong>Username:</strong></td>
                                                <td>{{Auth::user()->username}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"> <strong>Email:</strong></td>
                                                <td>{{Auth::user()->email}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>Contact no.:</strong></td>
                                                <td>{{Auth::user()->contact_num}}</td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                    </center>
     
                                    
                                </div>
        </div>
      </div>
      
    </div>

  </div>



  <div id="uploaddp" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Update Profile Picture</h5>
            <div class="pull-right">
                    <a class="modal-action modal-close waves-effect waves-light btn teal left">Cancel</a>
            </div>
        </div>
     <div class="modal-body">
    

   
   {!! Form::open(array('route' => 'image.upload.post','files'=>true)) !!}
   <br>
 	@if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<div class="row">
<div class="col s12 m4">
	<img id="preview" src="{{ url('/images/'.Auth::user()->avatar) }}" alt="your image" style=" max-width:180px; max-height: 180px;" />
</div>
<div class="col s12 m8">
    <div class="file-field input-field">

      <div class="btn">
        <span>BROWSE</span>
        {!! Form::file('image', array('class' => 'form-control','onchange' => 'readURL(this);','style' => 'padding:10px;')) !!}
		
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text">
      </div>
    </div>
</div>
</div>
   
  
  <br>
   <button type="submit" href="#!" class="modal-action waves-effect waves-light btn blue right">update</button>
   {!! Form::close() !!}   	
      	<br>
     </div>
    
    </div>
    
 </div>


<script>
	  function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>

@endsection