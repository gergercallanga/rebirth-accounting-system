@extends("layouts.default")
@section('title', '| Job Order')
@section("content")

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h4>Job Order Details</h4>
            </div>
        </div>
</div>

  <div class="row">
    <div class="col-lg-12 margin-tb" style="margin-top: -30px;">
            <div class="input-field col s5" style="margin-top: 25px;">
                <a class="waves-effect waves-light btn modal-trigger tooltipped modal-trigger" data-tooltip="Add New Job Order" href=#joborder_modal id="joborder_add"><i class="material-icons">add</i></a>
            </div>
       
    </div>
    </div>

  <br>
    <div class="table-responsive"  style="width: 100%" >
        <table id="joborder_table" class="display table">
            <thead>
             <tr class="#bdbdbd grey darken-2" style="color: white;">

                <th>ID</th>
                <th>Customer Name</th>
                <th>Vehicle</th>
                <th>Plate</th>
                <th>Stage</th>
                <th>Status</th>
                <th>Date of Creation</th>
                <th>Last Updated</th>
            </tr>
            </thead>
            <tbody>

            @foreach($joborder as $joborderlist)
                <tr>
                    <td>{{ $joborderlist['id']}}</td>
                    <td>
                        {{$joborderlist['customer']['name']}}            
                    </td>
                    <td>
                       {{$joborderlist['vehicles']['type']}} {{$joborderlist['vehicles']['model']}}                 
                    </td>
                    <td>{{$joborderlist['vehicles']['plate']}} </td>
                    <td>
                    @if($joborderlist['first_check_desc'] == null)
                        First Checklist
                    @elseif($joborderlist['second_check_desc'] == null)
                        Second Checklist
                    @elseif($joborderlist['third_check_desc'] == null)
                        Third Checklist
                    @else
                        Ongoing Maintenance
                    @endif
                    </td>
                    <td>
                        @switch($joborderlist['status'])
                            @case(1)
                                Pending
                            @case(2)
                                Ongoing Maintenance
                            @case(3)
                                Cancelled
                            @default
                        @endswitch
                    </td>
                    <td>
                        {{$joborderlist['created_at']}}
                    </td>
                    <td>
                        {{$joborderlist['updated_at']}}
                    </td>
                </tr>


            @endforeach
            </tbody>
        </table>
    </div>
    
   <!--  -->


    <div class="modal" id="joborder_modal" role="dialog">
         <div class="modal-header">
            <h5 class="modal-title">Add Job Order</h5>
                 <div class="row" style="margin-bottom: -30px;">
                    <div class="input-field col s6">
                    <i class="material-icons prefix">face</i>
                      <input placeholder="Customer" id="customer_name" type="text" class="validate" disabled>
                      <label for="customer">Customer</label>
                        <input type="hidden" id="customer_id">
                    </div>
                    <div class="input-field col s6">
                    <i class="material-icons prefix">directions_car</i>
                      <input placeholder="Vehicle" id="vehicle_name" type="text" class="validate" disabled>
                      <label for="vehicle">Vehicle</label>
                        <input type="hidden" id="vehicle_id">
                    </div>
                </div>
                 <input type="hidden" id="joborder_id">
        </div>
            <div class="col s12">
                <ul class="tabs tabs-fix-width z-depth-1 #ffebee red lighten-5"  style="height: 70px;" id="joborder_form_tab">
                    <li class="tab col s3"><a class="active" href="#cust_veh_tab">Customer and Vehicle</a></li>
                    <li class="tab col s3"><a href="#inq_dep_tab">Inquiry and Deposit</a></li>
                    <li class="tab col s3"><a href="#services_tab">Services</a></li>
                    <li class="tab col s3"><a href="#fir_check_tab">1st Check List</a></li>
                    <li class="tab col s3"><a href="#sec_check_tab">2nd Check List</a></li>
                    <li class="tab col s3"><a href="#fin_check_tab">Final Check List</a></li>
                    <li class="tab col s3"><a href="#ord_rec_tab">Order Receipt</a></li>
                    <li class="tab col s3"><a href="#maintenance_tab">Maintenance</a></li>
                </ul>
             </div>
       

           <div id="loaders_modal" class="progress hide">
                <div class="indeterminate"></div>
            </div>
            <input type="hidden" name="job_order_id" id="job_order_id">
            <div class="modal-content">
                <div id="cust_veh_tab" class="col s12">
                @include("joborder.forms.cust_veh_form")
                </div>

                <div id="ord_rec_tab">
                    @include("joborder.forms.order_receipt_form")
                </div>

                <div id="services_tab">
                    @include("joborder.forms.service_form")
                </div>

                <div id="fir_check_tab">
                    @include("joborder.forms.first_check_form")
                </div>

                <div id="fin_check_tab">
                    @include("joborder.forms.final_check_form")
                </div>

                 <div id="sec_check_tab">
                    @include("joborder.forms.sec_check_form")
                </div>

                <div id="inq_dep_tab">
                    @include("joborder.forms.cxinfo")
                </div>

                 <div id="maintenance_tab">
                    @include("joborder.forms.form_maintenance")
                </div>
                  
                
        <div class="modal-footer">
              <button class="modal-action modal-close waves-effect waves-green btn-flat ">Cancel</button>
              <button class="modal-action modal-close waves-effect waves-green btn-flat ">Close And Save</button>
        </div>
            </div>
           

       
        
    </div>





    <script>
                    $("#postbutton").click(function(){
                        
                        $.ajax({
                            /* the route pointing to the post function */
                            url: 'joborder/getJoborder',
                            type: 'POST',
                            /* send the csrf-token and the input to the controller */
                            data: {_token: "{{ csrf_token() }}", vehicle_id:$("#vehicle_id").val(),customer_id:$("#customer_id").val()},
                            dataType: 'JSON',
                            /* remind that 'data' is the response of the AjaxController */
                            success: function (data) { 
                               swal("Saved!", "Your input was successfully saved!", "success");
                               $("#joborder_id").val(data);
                               $("#postbutton").attr("disabled", true); 
                            },
                            error: function(e) {
                                alert(e.responseText);
                            }
                        }); 
                    });

                     $("#saveinquiry").click(function(){
                        
                        $.ajax({
                            /* the route pointing to the post function */
                            url: 'joborder/saveInquiry',
                            type: 'POST',
                            /* send the csrf-token and the input to the controller */
                            data: {_token: "{{ csrf_token() }}",joborder_id: $("#joborder_id").val(),inquiry_name: $("#inquiry_name").val(),ptype: $("#ptype").val(),amount: $("#joborder_id").val()},
                            dataType: 'JSON',
                            /* remind that 'data' is the response of the AjaxController */
                            success: function (data) { 
                               swal("Saved!", "Your input was successfully saved!", "success");
                                $("#saveinquiry").attr("disabled", true); 
                            },
                            error: function(e) {
                                swal("Saved!", "Your input was successfully saved!", "success");
                            }
                        }); 
                    });



    $( document ).ready(function() {
        $('#joborder_table').DataTable( {
        stateSave: true
        } );
    
        $('ul.tabs').tabs();
        $('.modal').modal(
            {
                dismissible: false, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                inDuration: 300, // Transition in duration
                outDuration: 200, // Transition out duration
                startingTop: '4%', // Starting top style attribute
                endingTop: '10%', // Ending top style attribute
                ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    $('.modal').removeData('.modal')
                },
                complete: function() {
                    var div= document.createElement("div");
                    div.className += "overlay";
                    document.body.appendChild(div);
                    location.reload();
                } // Callback   for Modal close
            }
        );

        Materialize.updateTextFields();

        
});



    </script>

@endsection