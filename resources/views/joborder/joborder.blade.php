@extends('layouts.default')
@section('content')
<br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Job Orders Information</h2>
            </div>
            <div class="pull-right">
                <a class="waves-effect waves-light green btn modal-trigger tooltipped" data-tooltip="Insert New Schedule" href="{{ route('suppliers.create') }}"> Add New Supplier </a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
           <!--  <th>No</th> -->
            <th>Supplier ID</th>
            <th>Supplier Name</th>
            <th>Supplier Address</th>
            <th width="280px">Operation</th>
        </tr>

    @foreach ($suppliers as $supplier)
    <tr>
   <!--      <td>{{ ++$i }}</td> -->
        <td>{{ $supplier->id}}</td>
        <td>{{ $supplier->supplier_name}}</td>
        <td>{{ $supplier->supplier_address}}</td>
       

        <td>
           <a class="waves-effect waves-light blue btn modal-trigger tooltipped" data-tooltip="View Details" href="{{ route('suppliers.show',$supplier->id) }}"><i class="material-icons">visibility</i></a>
            <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Edit" href="{{ route('suppliers.edit',$supplier->id) }}"><i class="material-icons">edit</i></a>

              {!! Form::open(['method' => 'DELETE','route' => ['suppliers.destroy', $supplier->id],'style'=>'display:inline']) !!}
            {{Form::button('<i class="material-icons">delete</i>', array('type' => 'submit','class' => 'waves-effect waves-light red btn modal-trigger tooltipped','data-tooltip' => 'Delete'))}}
            {!! Form::close() !!}
        </td>


    </tr>
    @endforeach
    </table>
    {!! $suppliers->render() !!}

<div class="modal" id="joborder_modal" role="dialog">

    <div class="row">

            <ul class="tabs tabs-fix-width" id="user_form_tab">
                <li class="tab col s5 offset-s1"><a class="active" href="#new_user_tab">Step 1</a></li>
                <li class="tab col s5"><a href="#sel_role_tab">Step 2</a></li>
            </ul>

        <div id="loaders_modal" class="progress hide">
            <div class="indeterminate"></div>
        </div>

        <div class="modal-content">
            @include("userManagement.forms.user_create_edit")
        </div>

    </div>
</div>

@endsection