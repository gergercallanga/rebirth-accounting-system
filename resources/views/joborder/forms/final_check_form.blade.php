


<form id="my-awesome-dropzone3" action="/joborder/upload" class="dropzone">
    <div class="dropzone-previews"></div>
    <div class="fallback"> <!-- this is the fallback if JS isn't working -->
        
    </div>

</form>




<div class="row">

        <div class="input-field col s6" style="overflow-x:auto ">
          <input placeholder="Description" id="fin_check_desc" type="text" class="validate" >
          <label for="fin_check_desc">Description</label>

        </div>
    <div class="col s6">
        <div class="row">
            <div class="col s6">
                    <ul class="collection">

                        @foreach($check_list_desc as $check_list2)
                            @if($check_list2['status'] == "Good")
                            <li class="collection-item">
                                <p>
                                        <input id="{{$check_list2['description']}}_3" name="checklistbox3" type="checkbox" class="filled-in" value="{{$check_list2['description']}}" />
                                        <label for="{{$check_list2['description']}}_3">{{$check_list2['description']}}</label>
                                </p>
                            </li>
                             @endif
                        @endforeach
                    </ul>
            </div>
            <div class="col s6">
                <ul class="collection" style="min-width:50px; max-height: 300px;overflow-y: scroll">
                    @foreach($check_list_desc as $check_list2)
                        @if($check_list2['status'] == "Bad")
                            <li class="collection-item">
                                <p>
                                    <input id="{{$check_list2['description']}}_3" name="checklistbox3" type="checkbox" class="filled-in" value="{{$check_list2['description']}}" />
                                    <label for="{{$check_list2['description']}}_3">{{$check_list2['description']}}</label>
                                </p>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    </div>
    <button type="submit" id="submit-all3" class="btn btn-primary btn-xs">Upload the file</button>

<script type="text/javascript">
    var myDropzoneTheSecond = new Dropzone( '#my-awesome-dropzone3',{ // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        addRemoveLinks: true,
        uploadMultiple: true,
        parallelUploads: 25,
        maxFiles: 25,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        // The setting up of the dropzone
        init: function() {
            var myDropzone2 = this;

            // Here's the change from enyo's tutorial...

            $("#submit-all3").click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                myDropzone2.options.addRemoveLinks = false;
                 myDropzone2.processQueue();

            });
            this.on('success', function( file, resp ){
                console.log( file );
                console.log( resp );
            });
            this.on('error', function( e ){
                console.log('erors and stuff');
                console.log( e );
            });


        },
        sending: function(file, xhr, formData) {
            formData.append("_token", "{{ csrf_token() }}");
            formData.append("checklist_id", 3);
            formData.append("joborder_id", $("#joborder_id").val());
             formData.append("check_description", $("#fin_check_desc").val());
        }
    }
    );
    $(document).ready(function() {
        var sec;



        $("input[name='checklistbox3']").on('change',function(){
            if($(this).is(":checked")){

                if($("#fin_check_desc").val() == ''){
                    sec = $(this).val();
                    $("#fin_check_desc").val($(this).val());
                }else{
                    $("#fin_check_desc").val( $("#fin_check_desc").val() + ", " + $(this).val());
                }
            }else{
               if($(this).val() == sec){
                   count = $("#fin_check_desc").val().indexOf(',');
                        if(count == -1){
                            $("#fin_check_desc").val($("#fin_check_desc").val().replace($(this).val(),""));
                            sec =  $("#fin_check_desc").val();

                        }else{
                            $("#fin_check_desc").val($("#fin_check_desc").val().replace($(this).val()+", ",""));
                            count = $("#fin_check_desc").val().indexOf(',');
                            sec =  (count == -1)? $("#fin_check_desc").val().substr(0, $("#fin_check_desc").val().indexOf(',')) : $("#fin_check_desc").val();

                        }

               }else{
                   $("#fin_check_desc").val($("#fin_check_desc").val().replace(", " + $(this).val(),""));
               }
            }

        });
    });

</script>