<form id="role_form" class="form-horizontal" method="POST" action="{{ url('/admin/roles') }}">

    {{ csrf_field() }}
    {{--{{ $errors->has('fname') ? ' has-error' : '' }}--}}

    <div class="input-field col s12">
        <input id="role" type="text" class="validate" name="name" value="{{ old('name') }}" autofocus>
        <label for="name">Role Name</label>
        @if ($errors->has('name'))
            <span class="help-block">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
        @endif
    </div>

    <div class="col s12">
        <h6> Specify Permission Acquired</h6>
    </div>

    <div class="form-group">

        <div class="col-md-8">
            <div class="table-responsive">
                <table class="table centered">
                    <thead>
                    <tr>
                        <th>Permission</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody id="role_form_tbody">

                        @foreach(json_decode($permission, true) as $permissions)
                            <tr>
                                <td>{{$permissions['name']}}</td>

                                <td>
                                    <div class="row">
                                        <button class="btn col s6 push-s3 pull-s3">View Roles with this Permission</button>
                                        <div class="switch col s12">
                                            <label>
                                                Off
                                                <input type="checkbox" name="permission_chbox[]" id="permission_chbox[]"
                                                       value="{{$permissions['id']}}">
                                                <span class="lever"></span>
                                                On
                                            </label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div class="modal-footer">

        <button id="role_form_submit" type="submit" class="waves-effect btn red darken col s3 push-s4">
            Submit
        </button>
        <button id="role_form_cancel" type="button" class="waves-effect btn red darken modal-close" data-dismiss="modal">Cancel
        </button>
    </div>


</form>



