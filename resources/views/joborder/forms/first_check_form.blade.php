<link href={{ asset('dropzone-master/dist/dropzone.css') }} rel="stylesheet" />
<script src={{ asset('dropzone-master/dist/dropzone.js') }}></script>


<form id="my-awesome-dropzone" action="/joborder/upload" class="dropzone">
    <div class="dropzone-previews"></div>
    <div class="fallback"> <!-- this is the fallback if JS isn't working -->
        <input name="file_images[]" type="file" multiple />
    </div>

</form>




<div class="row">

        <div class="input-field col s6" style="overflow-x:auto ">
          <input placeholder="Description" name="first_check_desc" id="first_check_desc" type="text" class="validate" >
          <label for="first_check_desc">Description</label>

        </div>
    <div class="col s6">
        <div class="row">
            <div class="col s6">
                    <ul class="collection">

                        @foreach($check_list_desc as $check_list)
                            @if($check_list['status'] == "Good")
                            <li class="collection-item">
                                <p>
                                        <input id="{{$check_list['description']}}" name="checklistbox" type="checkbox" class="filled-in" value="{{$check_list['description']}}" />
                                        <label for="{{$check_list['description']}}">{{$check_list['description']}}</label>
                                </p>
                            </li>
                             @endif
                        @endforeach
                    </ul>
            </div>
            <div class="col s6">
                <ul class="collection" style="min-width:50px; max-height: 300px;overflow-y: scroll">
                    @foreach($check_list_desc as $check_list)
                        @if($check_list['status'] == "Bad")
                            <li class="collection-item">
                                <p>
                                    <input id="{{$check_list['description']}}" name="checklistbox" type="checkbox" class="filled-in" value="{{$check_list['description']}}" />
                                    <label for="{{$check_list['description']}}">{{$check_list['description']}}</label>
                                </p>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    </div>
    <button type="submit" id="submit-all" class="btn btn-primary btn-xs">Upload the file</button>

<script type="text/javascript">
    var myDropzoneTheFirst = new Dropzone( '#my-awesome-dropzone', { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        addRemoveLinks: true,
        uploadMultiple: true,
        parallelUploads: 25,
        maxFiles: 25,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        // The setting up of the dropzone
        init: function() {
            var myDropzone = this;

            // Here's the change from enyo's tutorial...

            $("#submit-all").click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                myDropzone.options.addRemoveLinks = false;
                myDropzone.processQueue();

            });
            this.on('success', function( file, resp ){
                console.log( file );
                console.log( resp );
            });
            this.on('error', function( e ){
                console.log('erors and stuff');
                console.log( e );
            });


        },
        sending: function(file, xhr, formData) {
            formData.append("_token", "{{ csrf_token() }}");
            formData.append("checklist_id", 1);
            formData.append("joborder_id", $("#joborder_id").val());
             formData.append("check_description", $("#first_check_desc").val());
        }
    }
    );
    $(document).ready(function() {
        var first;



        $("input[name='checklistbox']").on('change',function(){
            if($(this).is(":checked")){

                if($("#first_check_desc").val() == ''){
                    first = $(this).val();
                    $("#first_check_desc").val($(this).val());
                }else{
                    $("#first_check_desc").val( $("#first_check_desc").val() + ", " + $(this).val());
                }
            }else{
               if($(this).val() == first){
                   count = $("#first_check_desc").val().indexOf(',');
                        if(count == -1){
                            $("#first_check_desc").val($("#first_check_desc").val().replace($(this).val(),""));
                            first =  $("#first_check_desc").val();

                        }else{
                            $("#first_check_desc").val($("#first_check_desc").val().replace($(this).val()+", ",""));
                            count = $("#first_check_desc").val().indexOf(',');
                            first =  (count == -1)? $("#first_check_desc").val().substr(0, $("#first_check_desc").val().indexOf(',')) : $("#first_check_desc").val();

                        }

               }else{
                   $("#first_check_desc").val($("#first_check_desc").val().replace(", " + $(this).val(),""));
               }
            }

        });
    });

</script>
