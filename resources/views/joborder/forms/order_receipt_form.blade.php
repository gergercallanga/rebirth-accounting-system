
    <div class="row" style="height: 300px;">
    <h6>Order Receipt</h6>
    <br>
       <div class="col-xs-12 col-sm-12 col-md-12">
        <div class=" input-field form-group">
            {!! Form::text('payment_type', null, array('class' => 'form-control','placeholder' => 'Cash/Credit/Check')) !!}
             <label for="deposit">Payment Type</label>
        </div>
    </div>

     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class=" input-field form-group">
            {!! Form::text('amount', null, array('class' => 'form-control','placeholder' => 'Amount')) !!}
             <label for="amount">Total Amount</label>
        </div>
    </div>
     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class=" input-field form-group">
            {!! Form::text('Deposit', null, array('class' => 'form-control','placeholder' => 'Deposit???')) !!}
             <label for="deposit">Deposit</label>
        </div>
    </div>

     <div class="pull-right">
            <button class="btn btn-primary">Print</button>
            
    </div>
  
</div>

