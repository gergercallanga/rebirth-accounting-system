    
    <h5>Service Form</h5>
    <br>

<input type="hidden" id="service_id">
    <div class="row">

        <div class="input-field col s6">
          <input placeholder="Service Description" id="service_name" type="text" class="validate" disabled>
          <label for="service">Service</label>
            
        </div>



        <div class="input-field col s6">
          <input placeholder="Total Amount" id="total_amount_rendered" name="total_amount_rendered" type="text" class="validate" value="0.00" disabled>
          <label for="">Total Amount</label>

        </div>



    </div>

   
       <div class="table-responsive">
            <table id="serv_item_table" class="display table" style="min-width: 500px">
                <thead>
                    <tr class="#grey lighten-1">
                        <th></th>
                        <th>Services</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Remarks</th>
                     
                    </tr>
                </thead>
            </table>
       </div>

    <div class="row">
            <div class=" col s6 offset s6 ">
                    <button type="button" class="btn" id="amtbutton"> Save And Next </button>
            </div>
    </div>

<script>

    function format_serv ( d ) {
        // d is the original data object for the row
        var collection = "";
    
        $.each(d, function (i, item){
            collection +=  
                '<tr>' +
                    '<td>' +
                   
                     '</td>' +
                      '<td> ' + 
                      `<button class="waves-effect waves-light btn-floating #76ff03 light-green accent-3 tooltipped veh-select" data-tooltip="Select Vehicle" id="${item.id}" name="veh-select" value="${item.type} ${item.plate}"><i class="material-icons">check</i></button>`  +
                    '</td>' +
                '</tr>';
        })

        return "";
    }

function addServiceWithComma(row){
      $('#service_name').val($('#service_name').val() + ", " +row.data().service_name);
       $('#service_id').val($('#service_id').val() + "|" +row.data().service_id);
}
function addServiceWithoutComma(row){
     $('#service_name').val($('#service_name').val() + row.data().service_name);
     $('#service_id').val($('#service_id').val() + row.data().service_id);
}

function removeServiceWithComma(row){
     $("#service_name").val($("#service_name").val().replace(", " + row.data().service_name,""));
      $("#service_id").val($("#service_id").val().replace("|" + row.data().service_id,""));
}
function removeServiceWithoutComma(row){
    $("#service_name").val($("#service_name").val().replace( row.data().service_name,""));
     $("#service_id").val($("#service_id").val().replace( row.data().service_id,""));
}
function removeServiceWithCommaFirst(row){
    $("#service_name").val($("#service_name").val().replace( row.data().service_name +", " ,""));
    $("#service_id").val($("#service_id").val().replace( row.data().service_id +"|" ,""));
}

function removeServiceWithoutCommaFirst(row){
    $("#service_name").val($("#service_name").val().replace( row.data().service_name,""));
    $("#service_id").val($("#service_id").val().replace( row.data().service_id,""));
}


function getServiceIndexWithComma(){
   return $("#service_name").val().substr(0, $("#service_name").val().indexOf(','));
}
function getServiceIDIndexWithComma(){
   return $("#service_id").val().substr(0, $("#service_name").val().indexOf('|'));
}

    $(document).ready(function() {
        var toggle_serv = 0;
        var last_check_serv;
        var serv_check_button = `<button class="waves-effect waves-light btn-floating tooltipped serv-select" data-tooltip="Select Service" name="serv-select"><i class="material-icons">check</i></button>`;
        var serv_clear_button = `<button class="waves-effect waves-light btn-floating red tooltipped serv-select" data-tooltip="Select Service" name="serv-select"><i class="material-icons">clear</i></button>`;
       
  

        var last_shown_child, last_shown_td;
        var table = $('#serv_item_table').DataTable( {
            "ajax": {
                url: "joborder/showServices",
                type: "GET"
            },
            "columns": [
                {
                    "className":      'service-control',
                    "orderable":      true,
                    "data":           null,
                    "defaultContent": serv_check_button
                },
                {"data": "service_name"},
                {"data": "price"},
                {"data": "status"},
                {"data": "remarks"}
                
               
            ],
            "order": [[1, 'asc']]
        } );

        // Add event listener for opening and closing details
         var first_serv;
        $('#serv_item_table tbody').on('click', 'td.service-control', function () {
            var tr = $(this).closest('tr');
            var td = $(this).closest('td');
            var row = table.row( tr );
           
           if (row.child.isShown()) {
                $('#total_amount_rendered').val( parseFloat(parseFloat($('#total_amount_rendered').val()) - row.data().price).toFixed(2) );
               
               if(row.data().service_name == first_serv){
                    removeServiceWithCommaFirst(row)
                     var first_serv_val = getServiceIndexWithComma();
                     if(first_serv_val == '' || first_serv_val == undefined){
                       removeServiceWithoutCommaFirst(row);
                        first_serv =  $("#service_name").val();
                     }else{
                       first_serv_val = getServiceIndexWithComma();
                           if(first_serv_val == '' || first_serv_val == undefined){
                                first_serv = $("#service_name").val();
                           }else{
                             first_serv = getServiceIndexWithComma();
                           }
                        
                     }
                
               }else{
                removeServiceWithComma(row);
               }

                row.child.hide();
                td.html(serv_check_button);
            }
            else {
                $('#total_amount_rendered').val( parseFloat(parseFloat($('#total_amount_rendered').val()) + row.data().price).toFixed(2) );
                if ($('#service_name').val() == ''){
                    first_serv=row.data().service_name;
                     addServiceWithoutComma(row);
                 }
                 else{
                    addServiceWithComma(row);
                 }

                
                 row.child( format_serv(row.data()) ).show();
                td.html(serv_clear_button);

            }

               
        } );
    });



</script>

<script>
    $("#amtbutton").click(function(){
                        
                        $.ajax({
                            /* the route pointing to the post function */
                            url: 'joborder/saveServices',
                            type: 'POST',
                            /* send the csrf-token and the input to the controller */
                            data: {_token: "{{ csrf_token() }}", joborder_id:$("#vehicle_id").val(),total_amount_rendered:$("#total_amount_rendered").val(), service_id: $("#service_id").val()},
                            dataType: 'JSON',
                            /* remind that 'data' is the response of the AjaxController */
                            success: function (data) { 

                               if (data.msg == "success"){
                               swal("Saved!", "Your input was successfully saved!", "success");
                               }
                            },
                            error: function(e) {
                                alert(e);
                            }
                        }); 
                    });

</script>