<div id="cust_veh_tab">
   
       <div class="table-responsive">
            <table id="cust_veh_table" class="display table" style="min-width: 500px">
                <thead>
                    <tr class="#grey lighten-1">
                        <th></th>
                        <th>Customer</th>
                    </tr>
                </thead>
            </table>
       </div>
</div>

<div class="row">
        <div class=" col s6 offset s6 ">
                <button type="button" class="btn" id="postbutton"> Save And Next </button>
        </div>
</div>
<script>

    function format ( d ) {
        // d is the original data object for the row
        var collection = "";
    
        $.each(d.vehicles, function (i, item){
            collection +=  
                '<tr>' +
                    '<td>' +
                     item.type + ' ' + item.plate +
                     '</td>' +
                      '<td> ' + 
                      `<button class="waves-effect waves-light btn-floating #76ff03 light-green accent-3 tooltipped veh-select" data-tooltip="Select Vehicle" id="${item.id}" name="veh-select" value="${item.type} ${item.plate}"><i class="material-icons">check</i></button>`  +
                    '</td>' +
                '</tr>';
        })

        return '<table cellpadding="5" cellspacing="0" border="0">'+
            '<thead>'+
                '<tr>Vehicles</tr>' +
            '</thead>' +
            '<tbody>' +
             collection +
            '</tbody>' + 
            '</table>';
    }

    $(document).ready(function() {
        var toggle_veh = 0;
        var last_check_veh;
        var cust_check_button = `<button class="waves-effect waves-light btn-floating tooltipped cust-select" data-tooltip="Select Customer" name="cust-select"><i class="material-icons">check</i></button>`;
        var cust_clear_button = `<button class="waves-effect waves-light btn-floating red tooltipped cust-select" data-tooltip="Select Customer" name="cust-select"><i class="material-icons">clear</i></button>`;
        function clearVehicles(){
            $("#vehicle_name").val('');
            $("#vehicle_id").val('');
        }
        function clearCustomers(){
            $("#customer_name").val('');
            $("#customer_id").val('');
        }
  

        var last_shown_child, last_shown_td;
        var table = $('#cust_veh_table').DataTable( {
            "ajax": {
                url: "joborder/showCustomerVehicles",
                type: "GET"
            },
            "columns": [
                {
                    "className":      'details-control',
                    "orderable":      true,
                    "data":           null,
                    "defaultContent": cust_check_button
                },
                { "data": "name" },
            ],
            "order": [[1, 'asc']]
        } );

        // Add event listener for opening and closing details
        $('#cust_veh_table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var td = $(this).closest('td');
            var row = table.row( tr );
            if(last_shown_child != undefined){
                last_shown_child.hide();
                clearCustomers();
                clearVehicles();
                last_shown_td.html(cust_check_button);
                last_shown_child = undefined;
            }else if ( row.child.isShown() ) {
                // This row is already open - close it
                clearCustomers();
                clearVehicles();
                row.child.hide();
                td.html(cust_check_button);
            }
            else {
                // Open this row
                $("#customer_name").val(row.data().name);
                $("#customer_id").val(row.data().id);
                last_shown_child = row.child;
                last_shown_td = td;
                row.child( format(row.data()) ).show();
                td.html(cust_clear_button);
            }

                 $('button[name=veh-select]').on('click', function(){
                    
                        $("#vehicle_name").val(this.value);
                        $("#vehicle_id").val(this.id);

                         var veh_check_button = '<i class="material-icons">check</i>';
                          var veh_clear_button = '<i class="material-icons">clear</i>';
                        if(toggle_veh == 0){
                                toggle_veh = 1;
                                clearVehicles();
                             $(this).html(veh_check_button);
                         }else{
                            if(last_check_veh != undefined){
                                    $('#' + last_check_veh).html(veh_check_button);
                                }
                            last_check_veh=this.id;
                                
                                
                            toggle_veh = 0;

                            $(this).html(veh_clear_button);
                         }
                       
             });
        } );
    });



</script>