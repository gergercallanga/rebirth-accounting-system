


<form id="my-awesome-dropzone2" action="/joborder/upload" class="dropzone">
    <div class="dropzone-previews"></div>
    <div class="fallback"> <!-- this is the fallback if JS isn't working -->
        
    </div>

</form>




<div class="row">

        <div class="input-field col s6" style="overflow-x:auto ">
          <input placeholder="Description" id="sec_check_desc" type="text" class="validate" >
          <label for="sec_check_desc">Description</label>

        </div>
    <div class="col s6">
        <div class="row">
            <div class="col s6">
                    <ul class="collection">

                        @foreach($check_list_desc as $check_list2)
                            @if($check_list2['status'] == "Good")
                            <li class="collection-item">
                                <p>
                                        <input id="{{$check_list2['description']}}_2" name="checklistbox2" type="checkbox" class="filled-in" value="{{$check_list2['description']}}" />
                                        <label for="{{$check_list2['description']}}_2">{{$check_list2['description']}}</label>
                                </p>
                            </li>
                             @endif
                        @endforeach
                    </ul>
            </div>
            <div class="col s6">
                <ul class="collection" style="min-width:50px; max-height: 300px;overflow-y: scroll">
                    @foreach($check_list_desc as $check_list2)
                        @if($check_list2['status'] == "Bad")
                            <li class="collection-item">
                                <p>
                                    <input id="{{$check_list2['description']}}_2" name="checklistbox2" type="checkbox" class="filled-in" value="{{$check_list2['description']}}" />
                                    <label for="{{$check_list2['description']}}_2">{{$check_list2['description']}}</label>
                                </p>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    </div>
    <button type="submit" id="submit-all2" class="btn btn-primary btn-xs">Upload the file</button>

<script type="text/javascript">
    var myDropzoneTheSecond = new Dropzone( '#my-awesome-dropzone2',{ // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        addRemoveLinks: true,
        uploadMultiple: true,
        parallelUploads: 25,
        maxFiles: 25,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        // The setting up of the dropzone
        init: function() {
            var myDropzone2 = this;

            // Here's the change from enyo's tutorial...

            $("#submit-all2").click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                myDropzone2.options.addRemoveLinks = false;
                 myDropzone2.processQueue();

            });
            this.on('success', function( file, resp ){
                console.log( file );
                console.log( resp );
            });
            this.on('error', function( e ){
                console.log('erors and stuff');
                console.log( e );
            });


        },
        sending: function(file, xhr, formData) {
            formData.append("_token", "{{ csrf_token() }}");
            formData.append("checklist_id", 2);
            formData.append("joborder_id", $("#joborder_id").val());
             formData.append("check_description", $("#sec_check_desc").val());
        }
    }
    );
    $(document).ready(function() {
        var sec;



        $("input[name='checklistbox2']").on('change',function(){
            if($(this).is(":checked")){

                if($("#sec_check_desc").val() == ''){
                    sec = $(this).val();
                    $("#sec_check_desc").val($(this).val());
                }else{
                    $("#sec_check_desc").val( $("#sec_check_desc").val() + ", " + $(this).val());
                }
            }else{
               if($(this).val() == sec){
                   count = $("#sec_check_desc").val().indexOf(',');
                        if(count == -1){
                            $("#sec_check_desc").val($("#sec_check_desc").val().replace($(this).val(),""));
                            sec =  $("#sec_check_desc").val();

                        }else{
                            $("#sec_check_desc").val($("#sec_check_desc").val().replace($(this).val()+", ",""));
                            count = $("#sec_check_desc").val().indexOf(',');
                            sec =  (count == -1)? $("#sec_check_desc").val().substr(0, $("#sec_check_desc").val().indexOf(',')) : $("#sec_check_desc").val();

                        }

               }else{
                   $("#sec_check_desc").val($("#sec_check_desc").val().replace(", " + $(this).val(),""));
               }
            }

        });
    });

</script>