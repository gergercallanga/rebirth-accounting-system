<br><br>


 <div class="row" style="margin-top: 15px;"> 
    <div class="input-field col s6">
           <i class="material-icons prefix">question_answer</i> 
              <select name="inquiry_name" id="inquiry_name">
             <option value="" disabled selected>Please Select</option>
          @foreach ($inquiries as $inquiry)
           <option value="{{ $inquiry->id }}"> {{ $inquiry->description }} </option>
          @endforeach
          </select>
             <label for="inquiry_name">Customer Inquiry</label>
    </div>

    <div class="input-field col s6">
           <i class="material-icons prefix">question_answer</i> 
              <select name="ptype" id="ptype">
             <option value="" disabled selected></option>
          @foreach ($paymenttypes as $paymenttype)
           <option value="{{ $paymenttype->id }}"> {{ $paymenttype->description }} </option>
          @endforeach
          </select>
             <label for="description">Optional Deposit</label>
    </div>

    <div class="row" style="margin-top: -20px;">
         <div class="input-field col s6">
           <i class="material-icons prefix">attach_money</i> 
           {!! Form::number('amount', null, array('class' => 'form-control', 'id' => 'amount')) !!}
             <label for="amount">Amount</label>
    </div>
   </div>
    <button type="button" class="btn pull-right" id="saveinquiry">Save And Next</button>


</div>

</div>
<br><br><br>