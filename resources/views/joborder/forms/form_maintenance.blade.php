  <div class="row" >
     <div class="input-field col s8 offset-s2">
                <div class=" input-field form-group"> 
                 {!! Form::select('description', ['1' => '1st Maintenance', '2' => '2nd Maintenance','3' => '3rd Maintenance', '4' => '4th Maintenance','5' => '5th Maintenance'], array('class' => 'datepicker')) !!}
                <label for="description">Description</label>   
                </div>
        </div>
    </div>
 <div class="row" >
     <div class="input-field col s4 offset-s1">
            <div class=" input-field form-group"> 
            {!! Form::text('sched_from', null, array('class' => 'datepicker')) !!}
             <label for="sched_from">Date From</label>   
            </div>
    </div>
  
    <div class="input-field col s4 offset-s1">
            <div class=" input-field form-group"> 
             {!! Form::text('sched_to', null, array('class' => 'datepicker')) !!}
            <label for="sched_to">Date To</label>   
            </div>
    </div>
 </div>
   


     

 <div class="row" >
    <div class="input-field col s5 offset-s1">
            <div class=" input-field form-group"> 
            {!! Form::text('from_time', null, array('class' => ' timepicker')) !!}
             <label for="from_time">Time From</label>   
            </div>
    </div>
   
    <div class="input-field col s5 offset-s1">
            <div class=" input-field form-group"> 
                 {!! Form::text('to_time', null, array('class' => 'timepicker')) !!}
            <label for="to_time">Time To</label>   
            </div>
    </div>
 </div>

 <div class="row">
    <div class="input-field col s4 offset-s1">
            <div class=" input-field form-group"> 
           {!! Form::number('amount', null, array('class' => 'form-control')) !!}
             <label for="amount">Amount</label>   
            </div>
    </div>

    <div class="input-field col s4 offset-s1">
            <div class=" input-field form-group"> 
           {!! Form::number('discount', null, array('class' => 'form-control')) !!}
             <label for="discount">Discount</label>   
            </div>
    </div>
</div>