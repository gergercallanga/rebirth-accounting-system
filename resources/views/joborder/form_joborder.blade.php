<br>
<br>
<div class="container" style="width: 90%">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Customer ID:</strong>
            {!! Form::text('customer_id', null, array('placeholder' => 'Customer_ID','class' => 'form-control')) !!}
        </div>
        <br>
    </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Vehicle ID:</strong>
            {!! Form::text('vehicle_id', null, array('placeholder' => 'Vehicle ID','class' => 'form-control')) !!}
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Inquiry ID:</strong>
            {!! Form::text('inquiry_id', null, array('placeholder' => 'Inquiry ID','class' => 'form-control')) !!}
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Optional Deposit ID:</strong>
            {!! Form::text('optional_deposit_id', null, array('placeholder' => 'Optional Deposit ID','class' => 'form-control')) !!}
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>First Check Desc:</strong>
            {!! Form::text('first_check_desc', null, array('placeholder' => 'First Check Desc','class' => 'form-control')) !!}
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Second Check Desc:</strong>
            {!! Form::text('second_check_desc', null, array('placeholder' => 'Second Check Desc','class' => 'form-control')) !!}
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Final Check Desc:</strong>
            {!! Form::text('final_check_desc', null, array('placeholder' => 'Final Check Desc','class' => 'form-control')) !!}
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Status:</strong>
            {!! Form::text('status', null, array('placeholder' => 'Status','class' => 'form-control')) !!}
        </div>
      </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>  




