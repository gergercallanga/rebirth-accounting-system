@extends('layouts.default')
@section('content')


<br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Joborders Information</h3>
            </div>
        </div>
    </div>

<ul id="tabs-swipe-demo" class="tabs">
    <li class="tab col s3"><a href="#test-swipe-1">Customer Information</a></li>
   <!--  <li class="tab col s3"><a href="#test-swipe-2">Optional Deposit</a></li> -->
    <li class="tab col s3"><a href="#test-swipe-3">Job Order</a></li>
    <li class="tab col s3"><a href="#test-swipe-1">Initial Checklist</a></li>
    <li class="tab col s3"><a href="#test-swipe-2">Pre-coating Checklist</a></li>
    <li class="tab col s3"><a href="#test-swipe-3">Final Checklist</a></li>
   <!--  <li class="tab col s3"><a href="#test-swipe-1">Customer Schedule Maintenance</a></li> -->
    <li class="tab col s3"><a href="#test-swipe-2">Issuance of OR</a></li>

  </ul>
  <div id="test-swipe-1" class="col s12 blue">Test 1</div>
  <div id="test-swipe-2" class="col s12 red">Test 2</div>
  <div id="test-swipe-3" class="col s12 green">Test 3</div>

  <br> <br>

    <div>
    <div class="row">
    <div class="col-lg-12 margin-tb" style="margin-top: -30px;">
                <div class="input-field col s5" style="margin-top: 25px;">
                <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Insert New Joborder" href="#addjoborder"><i class="material-icons">add</i></a>
            </div>
            <div class="input-field col s5 pull-right">
         <i class="material-icons prefix">search</i>
          {!! Form::text('seacrh_text', null, array('class' => 'search_text','id'=>'search_text')) !!}
          <label for="search_text">Search</label>
        </div>  
    </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered hoverTable">
    <thead>
        <tr class="#bdbdbd grey lighten-1">
            <th>Customer ID</th>
            <th>Vehicle ID</th>
            <th>Inquiry ID</th>
            <th>Optional Deposit ID</th>
            <th>First Check Desc</th>
            <th>Second Check Desc</th>
            <th>Final Check Desc</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody id="jobtable">
    @foreach ($joborders as $joborder)
    <tr>
        <td><a class= "modal-trigger" href="#show{{$joborder->id}}" style="color: black;">{{ $joborder->customer_id}}</a></td>
         <td><a class= "modal-trigger" href="#show{{$joborder->id}}" style="color: black;">{{ $joborder->vehicle_id}}</a></td>
         <td><a class= "modal-trigger" href="#show{{$joborder->id}}" style="color: black;">{{ $joborder->inquiry_id}}</a></td>
         <td><a class= "modal-trigger" href="#show{{$joborder->id}}" style="color: black;">{{ $joborder->optional_deposit_id}}</a></td>
         <td><a class= "modal-trigger" href="#show{{$joborder->id}}" style="color: black;">{{ $joborder->first_check_desc}}</a></td>
         <td><a class= "modal-trigger" href="#show{{$joborder->id}}" style="color: black;">{{ $joborder->second_check_desc}}</a></td>
         <td><a class= "modal-trigger" href="#show{{$joborder->id}}" style="color: black;">{{ $joborder->final_check_desc}}</a></td>
         <td><a class= "modal-trigger" href="#show{{$joborder->id}}" style="color: black;">{{ $joborder->status}}</a></td>

    </tr>
    @endforeach
    </tbody>
    </table>
    </div>


 @foreach ($joborders as $joborder)

<!-- Modal -->
<div id="show{{$joborder->id}}" class="modal" >
    <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Joborder Full Details</h4>
            <div class="pull-right">
            <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Edit" href="#edit{{$joborder->id}}"><i class="material-icons">edit</i></a>

            {!! Form::open(['method' => 'DELETE','route' => ['joborders.destroy', $joborder->id],'style'=>'display:inline'])!!}
                {{ Form::button('<i class="material-icons">delete</i>', array('type' => 'submit','class' => 'waves-effect waves-light red btn modal-trigger tooltipped','data-tooltip' => 'Delete'))}}
            {!! Form::close() !!}

            </div>
        </div>
       <div class="modal-body">
    <div class="row">
       
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ID:</strong>
                {{ $joborder->id}}
            </div>
        </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Customer ID:</strong>
                {{ $joborder->customer_id}}
            </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Vehicle ID:</strong>
                {{ $joborder->vehicle_id}}
            </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Inquiry ID:</strong>
                {{ $joborder->inquiry_id}}
            </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Optional Deposit ID:</strong>
                {{ $joborder->optional_deposit_id}}
            </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>First Check Desc:</strong>
                {{ $joborder->first_check_desc}}
            </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Second Check Desc:</strong>
                {{ $joborder->second_check_desc}}
            </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Final Check Desc:</strong>
                {{ $joborder->final_check_desc}}
            </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status:</strong>
                {{ $joborder->status}}
            </div>
          </div>
        </div> 
    </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-default modal-action modal-close" >Close</button>
      </div>
  </div>
  </div>
   

    <!-- Modal for Edit -->
<div id="edit{{$joborder->id}}" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Details</h4>
            <div class="pull-right">
                <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
        <div class= "modal-body">
        <br>
          @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($joborder, ['method' => 'PATCH','route' => ['joborders.update', $joborder->id]]) !!}
        @include('joborders.form_joborder')
    {!! Form::close() !!}
            
        </div>

    </div>
</div>

<div id="addjoborder" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Insert New Joborder</h4>
            <div class="pull-right">
                    <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
            
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
     {!! Form::open(array('route' => 'joborders.store','method'=>'POST')) !!}
         @include('joborders.form_joborder')
    {!! Form::close() !!}
   
    </div>
 </div>

  @endforeach
  <script>
$(document).ready(function(){
  $("#search_text").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#jobtable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
    {!! $joborders->render() !!}

@endsection