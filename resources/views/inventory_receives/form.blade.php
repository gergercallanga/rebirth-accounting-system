
<div class="row">
    <form class="col s12">
      <div class="row" style="margin-top: -20px;">
        <div class="input-field col s3">
          <i class="material-icons prefix">date_range</i>
           {!! Form::text('date_received', null, array('class' => 'datepicker')) !!}
          <label for="date_received">Receiving Date</label>
        </div>
        <div class="input-field col s1"></div>

        <div class="input-field col s3">
          <i class="material-icons prefix">shopping_cart</i>

           <select name="item_name" id="item_name">
           <option value="" disabled selected>Select Item</option>
          @foreach ($inventories as $inventory)
           <option data-code="{{ $inventory->id }}"> {{ $inventory->product }} </option>
          @endforeach
          </select>
          <label for ="item_name">Item Name</label>
          {!! Form::hidden('item_code', null, array('class' => 'form-control','id' => 'item_code')) !!}
      </div>
      
      <div class="input-field col s1"></div>
      <div class="input-field col s3">
          <i class="material-icons prefix">local_shipping</i>

          <select name="supplier_name" id="supplier_name">
           <option value="" disabled selected>Select Supplier</option>
          @foreach ($suppliers as $supplier)
           <option data-name="{{ $supplier->id }}"> {{ $supplier->supplier_name }} </option>
          @endforeach
          </select>
          <label for="supplier_name">Supplier Name</label>
          {!! Form::hidden('supplier_id', null, array('class' => 'form-control','id' => 'supplier_id')) !!}
        </div>
    </div>
      <div class="row" style="margin-top: -15px;">
       <div class="input-field col s3">
         <i class="material-icons prefix">add_shopping_cart</i>
           {!! Form::text('item_qty', null, array('class' => 'form-control')) !!}
          <label for="item_qty">Quantity</label>
        </div>

        <div class="input-field col s1"></div>  
        <div class="input-field col s3">
         <i class="material-icons prefix">local_offer</i>
           {!! Form::text('item_unit_cost', null, array('class' => 'form-control')) !!}
          <label for="item_unit_cost">Unit Cost</label>
        </div>
         <div class="input-field col s1"></div>  
        <div class="input-field col s3">
          <i class="material-icons prefix">attach_money</i>
           {!! Form::text('item_total_cost', null, array('class' => 'form-control')) !!}
          <label for="item_total_cost">Total Cost</label>
        </div>
      </div>

        <div class="row" style="margin-top: -15px;">
        <div class="input-field col s11" >
          <i class="material-icons prefix">insert_comment</i>
          {!! Form::text('remarks', null, array('class' => 'form-control')) !!}
          <label for="remarks">Remarks</label>
        </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
        </form>
      </div>   