@extends('layouts.default')
@section('content')

<br>
<h4>Inventory Receiving</h4>
<br>

<ul class="collapsible" data-collapsible="accordion">
    <li>
      <div class="collapsible-header waves-effect waves-red"><i class="material-icons">add_box</i>Add Item Record</div>
      <div class="collapsible-body"><span>
   
 @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
 {!! Form::open(array('route' => 'inventory_receiving.store','method'=>'POST')) !!}
         @include('inventory_receives.form')
{!! Form::close() !!}
</span></div>
   </li>
  </ul>    
   
   
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h5>Receiving Transactions</h5>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered hoverTable">
        <tr class="#bdbdbd grey lighten-1"> 

            <th>Date</th>
            <th>Item Name</th>
            <th>Quantity</th>
            <th>Unit Cost</th>
            <th>Total Cost</th>
            <th>Supplier</th>
            <th>Remarks</th>
        </tr>


    @foreach ($inventory_receives as $receive)
    <tr>
        <td><a class="modal-trigger" href="#edit{{$receive->id}}" style="color:black;">{{ $receive->date_received}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $receive->item_name}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $receive->item_qty}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $receive->item_unit_cost}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $receive->item_total_cost}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $receive->supplier_name}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $receive->remarks}}</a></td>


    </tr>
    @endforeach
    </table>

@foreach ($inventory_receives as $receive)
<!-- Modal for Edit -->
 <div id="edit{{$receive->id}}" class="modal">
    <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Update Details</h4>
            <div class="pull-right">
                   <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
    <div class="modal-body">
    <br>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
{!! Form::model($receive, ['method' => 'PATCH','route' => ['inventory_receiving.update', $receive->id]]) !!}
    @include('inventory_receives.form')
    {!! Form::close() !!}
    </div>
    </div>
  </div>
  @endforeach
  

<script>
$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    format: 'yyyy-mm-dd',
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false // Close upon selecting a date,
  });
</script>


<script>
     
    $('#item_name').on('change',function(){
    var code = $(this).children('option:selected').data('code');
    $('#item_code').val(code);
});
          
</script>
<script>


    $('#supplier_name').on('change',function(){
    var name = $(this).children('option:selected').data('name');
    $('#supplier_id').val(name);
});
</script>

  

    {!! $inventory_receives->render() !!}
@endsection