<!-- <div class="container"> -->
  <!--   <button class="waves-effect btn red darken-2" id="role_add"> Add New Role</button> -->
    
<br><br>
<div class="row">
<form class="col s12">
  <div class="row" style="margin-top: -20px;">
     <div class="input-field col s4">
         <i class="material-icons prefix">face</i>
           {!! Form::text('item_qty', null, array('class' => 'form-control')) !!}
          <label for="item_qty">Customer Name</label>
     </div>

      <div class="input-field col s4">
         <i class="material-icons prefix">airport_shuttle</i>
           {!! Form::text('item_qty', null, array('class' => 'form-control')) !!}
          <label for="item_qty">Vehicle</label>
     </div>

        <div class="input-field col s4">
            <i class="material-icons prefix">description</i> 
               {!! Form::text('description', null, array('class' => 'form-control')) !!}
            <label for="description">Description</label>
        </div>
   </div>
</div>

 <div class="row" style="margin-top: -150px;">
    <div class="input-field col s4">
           <i class="material-icons prefix">local_offer</i> 
           {!! Form::number('amount', null, array('class' => 'form-control')) !!}
             <label for="amount">Amount</label>
    </div>
    
    <div class="input-field col s4">
            <i class="material-icons prefix">album</i>
                 {!! Form::text('status', null, array('class' => 'form-control')) !!}
            <label for="status">Status</label>   
            
    </div>
    
    <div class="input-field col s4">
             <i class="material-icons prefix">event_note</i>
                 {!! Form::text('note', null, array('class' => 'form-control')) !!}
            <label for="note">Note</label>
    </div>
 </div>

 <div class="row" style="margin-top: 15px;"> 
    <div class="input-field col s6">
           <i class="material-icons prefix">question_answer</i> 
              <select name="item_name" id="item_name">
             <option value="" disabled selected></option>
          @foreach ($inquiries as $inquiry)
           <option data-code="{{ $inquiry->id }}"> {{ $inquiry->description }} </option>
          @endforeach
          </select>
             <label for="description">Customer Inquiry</label>
    </div>

    <div class="input-field col s6">
           <i class="material-icons prefix">question_answer</i> 
              <select name="item_name" id="item_name">
             <option value="" disabled selected></option>
          @foreach ($paymenttypes as $paymenttype)
           <option data-code="{{ $paymenttype->id }}"> {{ $paymenttype->description }} </option>
          @endforeach
          </select>
             <label for="description">Optional Deposit</label>
    </div>


</div>

    <!-- <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>  -->
</form>
</div>
<br><br><br>

<!-- </div> -->


