
<div class="row">
<form class="col s12">
  <div class="row" style="margin-top: -20px;">
        <div class="input-field col s4">
             <div class=" input-field form-group"> 
            <select name="customer_name" id="customer_name">
            <option value="" disabled selected>Select Customer</option>
              @foreach ($customers as $customer)
               <option data-code="{{ $customer->id }}">{{ $customer->name }} </option>
              @endforeach
            </select>
            <label for ="customer_name">Customer Name</label>
                {!! Form::hidden('customer_id', null, array('class' => 'form-control','id' => 'customer_id')) !!}
                
            </div>

        </div>
        <div class="input-field col s4">
            <div class=" input-field form-group">
            <select name="vehicle" id="vehicle">
            <option value="" disabled selected>Select Vehicle</option>
              @foreach ($vehicles as $vehicle)
               <option data-code="{{ $vehicle->id }}">{{ $vehicle->type }} </option>
              @endforeach
            </select>
             <label for ="vehicle">Type</label>
               {!! Form::hidden('vehicle_id', null, array('class' => 'form-control')) !!}
           <!--  <label for="vehicle_id">Vehicle ID</label> -->
            </div>
        </div>
        <div class="input-field col s4">
            <div class=" input-field form-group"> 
               {!! Form::text('description', null, array('class' => 'form-control')) !!}
            <label for="description">Description</label>
            </div>
        </div>
    </div>
 <div class="row" style="margin-top: -150px;">
     <div class="input-field col s5">
            <div class=" input-field form-group"> 
            {!! Form::text('sched_from', null, array('class' => 'datepicker')) !!}
             <label for="sched_from">Date From</label>   
            </div>
    </div>
    <div class="input-field col s1"></div>
    <div class="input-field col s5">
            <div class=" input-field form-group"> 
             {!! Form::text('sched_to', null, array('class' => 'datepicker')) !!}
            <label for="sched_to">Date To</label>   
            </div>
    </div>
 </div>
     

 <div class="row" style="margin-top: -150px;">
    <div class="input-field col s5">
            <div class=" input-field form-group"> 
            {!! Form::text('from_time', null, array('class' => ' timepicker')) !!}
             <label for="from_time">Time From</label>   
            </div>
    </div>
    <div class="input-field col s1"></div>
    <div class="input-field col s5">

            <div class=" input-field form-group"> 
                 {!! Form::text('to_time', null, array('class' => 'timepicker')) !!}
            <label for="to_time" >Time To</label>   
            </div>
    </div>
 </div>

 <div class="row" style="margin-top: -150px;">
    <div class="input-field col s4">
            <div class=" input-field form-group"> 
           {!! Form::number('amount', null, array('class' => 'form-control')) !!}
             <label for="amount">Amount</label>   
            </div>
    </div>
    
    <div class="input-field col s4">
            <div class=" input-field form-group"> 
                 {!! Form::text('status', null, array('class' => 'form-control')) !!}
            <label for="status">Status</label>   
            </div>
    </div>
    
    <div class="input-field col s4">
            <div class=" input-field form-group"> 
                 {!! Form::text('note', null, array('class' => 'form-control')) !!}
            <label for="note">Note</label>   
            </div>
    </div>
 </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div> 
</form>
</div>

 