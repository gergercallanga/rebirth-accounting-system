<br>
<br>
<div class="container" style="width: 90%">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Supplier Name:</strong>
            {!! Form::text('supplier_name', null, array('placeholder' => 'Supplier Name','class' => 'form-control')) !!}
        </div>
        <br>
    </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Supplier Address:</strong>
            {!! Form::text('supplier_address', null, array('placeholder' => 'Supplier Address','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>  




