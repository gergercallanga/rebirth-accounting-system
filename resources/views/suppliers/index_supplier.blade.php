@extends('layouts.default')
@section('content')
<br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Suppliers Information</h3>
            </div>
        </div>
    </div>
    <div>
    <div class="row">
    <div class="col-lg-12 margin-tb" style="margin-top: -30px;">
                <div class="input-field col s5" style="margin-top: 25px;">
                <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Insert New Schedule" href="#addsupplier"><i class="material-icons">add</i></a>
            </div>
            <div class="input-field col s5 pull-right">
         <i class="material-icons prefix">search</i>
          {!! Form::text('seacrh_text', null, array('class' => 'search_text','id'=>'search_text')) !!}
          <label for="search_text">Search</label>
        </div>  
    </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered hoverTable">
    <thead>
        <tr class="#bdbdbd grey lighten-1">
            <th>Supplier Name</th>
            <th>Supplier Address</th>
        </tr>
    </thead>
    <tbody id="suptable">
    @foreach ($suppliers as $supplier)
    <tr>
        <td><a class= "modal-trigger" href="#show{{$supplier->id}}" style="color: black;">{{ $supplier->supplier_name}}</a></td>
        <td>{{ $supplier->supplier_address}}</td>
    </tr>
    @endforeach
    </tbody>
    </table>
    </div>


 @foreach ($suppliers as $supplier)

<!-- Modal -->
<div id="show{{$supplier->id}}" class="modal" >
    <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Supplier Full Details</h4>
            <div class="pull-right">
            <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Edit" href="#edit{{$supplier->id}}"><i class="material-icons">edit</i></a>

            {!! Form::open(['method' => 'DELETE','route' => ['suppliers.destroy', $supplier->id],'style'=>'display:inline'])!!}
                {{ Form::button('<i class="material-icons">delete</i>', array('type' => 'submit','class' => 'waves-effect waves-light red btn modal-trigger tooltipped','data-tooltip' => 'Delete'))}}
            {!! Form::close() !!}

            </div>
        </div>
        <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class=" input-field form-group">
             <i class="material-icons prefix">local_shipping</i>
            <input disabled value="{{ $supplier->supplier_name}}" id="name" type="text" class="validate">
          <label for="name">Supplier Name</label> 
            </div>
        </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="input-field form-group">
            <i class="material-icons prefix">location_on</i>
            <input disabled value="{{ $supplier->supplier_address}}" id="address" type="text" class="validate">
             <label for="address">Address</label> 
            </div>
        </div>

        </div>  
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default modal-action modal-close">Close</button>
        </div>
    </div>
    </div>

    <!-- Modal for Edit -->
<div id="edit{{$supplier->id}}" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Details</h4>
            <div class="pull-right">
                <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
        <div class= "modal-body">
        <br>
          @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($supplier, ['method' => 'PATCH','route' => ['suppliers.update', $supplier->id]]) !!}
        @include('suppliers.form_supplier')
    {!! Form::close() !!}
            
        </div>

    </div>
</div>

<div id="addsupplier" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Insert New Supplier</h4>
            <div class="pull-right">
                    <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
            
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
     {!! Form::open(array('route' => 'suppliers.store','method'=>'POST')) !!}
         @include('suppliers.form_supplier')
    {!! Form::close() !!}
   
    </div>
 </div>

  @endforeach
  <script>
$(document).ready(function(){
  $("#search_text").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#suptable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
    {!! $suppliers->render() !!}
@endsection