@extends('layouts.default')
@section('content')

<br>
<h4>Inventory Cost Adjustment</h4>
<br>

<ul class="collapsible" data-collapsible="accordion">
    <li>
      <div class="collapsible-header waves-effect waves-red"><i class="material-icons">add_box</i>Make Adjustment</div>
      <div class="collapsible-body"><span>
   
@if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
@endif
 {!! Form::open(array('route' => 'itemcost_adjustment.store','method'=>'POST')) !!}
         @include('itemcost_adjustments.form')
{!! Form::close() !!}
</span></div>
   </li>
  </ul>    


<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h5>Quantity Adjustment Records</h5>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered hoverTable">
        <tr class="#bdbdbd grey lighten-1"> 

            <th>Date</th>
            <th>Item Code</th>
            <th>Amount In</th>
            <th>Amount Out</th>
            <th>Running Qty</th>
            <th>Running Balance</th>
            <th>Average Cost</th>
            <th>Remarks</th>
        </tr>


    @foreach ($itemcost_adjustments as $costadj)
    <tr>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $costadj->adjustdate }}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $costadj->item_code }}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $costadj->amt_in }}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $costadj->amt_out }}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $costadj->runningqty }}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $costadj->runningbal}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $costadj->averagecost }}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $costadj->remarks }}</a></td>


    </tr>
    @endforeach
    </table>

  


<script>
$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    format: 'yyyy-mm-dd',
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false // Close upon selecting a date,
  });
</script>
<script>
      $(document).ready(function() {
    $('select').material_select();
  });
          
</script>

<script>
      $(document).ready(function() {
    $('select').material_select();
  });

    $('#item_name').on('change',function(){
    var code = $(this).children('option:selected').data('code');
    $('#item_code').val(code);
});
          
</script>
<script>
        $(document).ready(function() {
    $('select').material_select();
  });

    $('#supplier_name').on('change',function(){
    var name = $(this).children('option:selected').data('name');
    $('#supplier_id').val(name);
});
</script>
@endsection