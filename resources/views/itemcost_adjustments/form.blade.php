<div class="row">
    <form class="col s12">
      <div class="row" style="margin-top: -20px;">
        <div class="input-field col s3">
          <i class="material-icons prefix">date_range</i>
           {!! Form::text('adjustdate', null, array('class' => 'datepicker')) !!}
          <label for="date_received">Adjustment Date</label>
        </div>
        <div class="input-field col s1"></div>
        <div class="input-field col s3">
          <i class="material-icons prefix">shopping_cart</i>

           <select name="item_name" id="item_name">
           <option value="" disabled selected>Select Item</option>
          @foreach ($inventories as $inventory)
           <option data-code="{{ $inventory->id }}"> {{ $inventory->product }} </option>
          @endforeach
          </select>
          <label for ="item_name">Item Name</label>
          {!! Form::hidden('item_code', null, array('class' => 'form-control','id' => 'item_code')) !!}
      </div>
      <div class="input-field col s1"></div>
    </div>
      <div class="row" style="margin-top: -15px;">
       <div class="input-field col s3">
         <i class="material-icons prefix">add_shopping_cart</i>
           {!! Form::text('amt_in', null, array('class' => 'form-control')) !!}
          <label for="qtyin">Amount In</label>
        </div>

        <div class="input-field col s1"></div>  
        <div class="input-field col s3">
         <i class="material-icons prefix">local_offer</i>
           {!! Form::text('amt_out', null, array('class' => 'form-control')) !!}
          <label for="qtyout">Amount Out</label>
        </div>
         <div class="input-field col s1"></div>  
        <div class="input-field col s3">
          <i class="material-icons prefix">attach_money</i>
           {!! Form::number('runningqty', null, array('class' => 'form-control','id' => 'runningqty','onBlur' => 'avecost();')) !!}
          <label for="runningqty">Running Stock</label>
        </div>
      </div>

        <div class="row" style="margin-top: -15px;">
        <div class="input-field col s11" >
          <i class="material-icons prefix">insert_comment</i>
          {!! Form::number('runningbal', null, array('class' => 'form-control','id' => 'runningbal','onBlur' => 'avecost();')) !!}
          <label for="runningbal">Running Balance</label>
        </div>
        </div>
         <div class="row" style="margin-top: -15px;">
        <div class="input-field col s11" >
          <i class="material-icons prefix">insert_comment</i>
          {!! Form::number('averagecost', null, array('placeholder' => '0','class' => 'form-control','id' => 'averagecost')) !!}
          <label for="averagecost">Average Cost</label>
        </div>
        </div>
        <div class="row" style="margin-top: -15px;">
        <div class="input-field col s11" >
          <i class="material-icons prefix">insert_comment</i>
          {!! Form::text('remarks', null, array('class' => 'form-control')) !!}
          <label for="remarks">Remarks</label>
        </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
        </form>
      </div>   

<script>
      function avecost() {
        var qty = parseInt(document.getElementById('runningqty').value);
        var bal = parseInt(document.getElementById('runningbal').value);
        var cost = bal / qty;

        document.getElementById('averagecost').value= cost;
}
</script>