@extends("layouts.parent")
@section('title', 'Sms Sender')
@section("content")

    <div class="row">
        <form class="col s12" method="POST" action="{{url('tools/smsSend')}}">
            {{csrf_field()}}
            <div class="row">
                <div class="input-field col s6 offset-s3">
                    <input placeholder="Placeholder" id="receiver_num" name="receiver_num" type="text" class="validate">
                    <label for="receiver_num">To:</label>
                </div>
                <div class="input-field col s12">
                    <input id="msg_body" name="msg_body" type="text"  class="validate materialize-textarea">
                    <label for="last_name">Message Body</label>
                </div>
            </div>
            <button type="submit" class="btn">Send Sms</button>
        </form>
    </div>

@endsection