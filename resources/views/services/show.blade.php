@extends('layouts.default')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Services</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('services.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Service ID</strong>
                {{ $service->service_id}}
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Service Name :</strong>
                {{ $service->service_name}}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Price :</strong>
                {{ $service->price}}
            </div>
        </div>
    

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status :</strong>
                {{ $service->status}}
            </div>
        </div>
    

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Remarks :</strong>
                {{ $service->remarks}}
            </div>
        </div>
    </div>

@endsection

<!-- 'service_id','service_name','price','status','remarks' -->