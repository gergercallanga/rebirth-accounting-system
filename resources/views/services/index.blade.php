@extends('layouts.default')
@section('content')
<br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Services Information</h3>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-lg-12 margin-tb" style="margin-top: -30px;">
            <div class="input-field col s5" style="margin-top: 25px;">
                <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Insert New Record" href="#addservice"><i class="material-icons">add</i></a>
            </div>
        <div class="input-field col s5 pull-right">
          <i class="material-icons prefix">search</i>
          {!! Form::text('seacrh_text', null, array('class' => 'search_text','id'=>'search_text')) !!}
          <label for="search_text">Search</label>
        </div>  
    </div>

    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="table-responsive">
    <table class="table table-bordered hoverTable">
    <thead>
         <tr class="#bdbdbd grey lighten-1">
            
            <th>Service Name</th>
            <th>Price</th>
            <th>Status</th>
            <th>Remarks</th>
            
        </tr>
    </thead>
       
    <tbody id="servicetable">
     @foreach ($services as $service)
    <tr>
        
        <td><a class="modal-trigger" style="color: black;" href="#show{{$service->service_id}}">{{ $service->service_name}}</a></td>
        <td><a class="modal-trigger" style="color: black;" href="#show{{$service->_service_id}}">{{ $service->price}}</a></td>
        <td><a class="modal-trigger" style="color: black;" href="#show{{$service->service_id}}">{{ $service->status}}</a></td>
        <td><a class="modal-trigger" style="color: black;" href="#show{{$service->service_id}}">{{ $service->remarks}}</a></td>

    </tr>
     @endforeach
    </tbody>
   
    </table>
    </div>

@foreach ($services as $service)
     <!-- Modal Structure for Show Services -->
  <div id="show{{$service->service_id}}" class="modal">
    <div class="modal-content">
     <div class="modal-header">
        <h4 class="modal-title">Full Details</h4>
            <div class="pull-right">
             <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Edit" href="#edit{{$service->service_id}}"><i class="material-icons">edit</i></a>

            {!! Form::open(['method' => 'DELETE','route' => ['services.destroy', $service->service_id],'style'=>'display:inline']) !!}
            {{ Form::button('<i class="material-icons">delete</i>', array('type' => 'submit','class' => 'waves-effect waves-light red btn modal-trigger tooltipped','data-tooltip' => 'Delete'))}}
            {!! Form::close() !!}
            </div>
    </div>
    <div class="modal-body">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Service ID: </strong>
                {{ $service->service_id}}
            </div>  
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Service Name :</strong>
                {{ $service->service_name}}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Price :</strong>
                {{ $service->price}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status :</strong>
                {{ $service->status}}
            </div>
        </div>
    
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Remarks :</strong>
                {{ $service->remarks}}
            </div>
        </div>
    </div>

    </div>
   
    <div class="modal-footer">
      <a  class="modal-action modal-close waves-effect waves-green btn btn-default">Back</a>
    </div>
    </div>
  </div>

 <div id="edit{{$service->service_id}}" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Details</h4>
            <div class="pull-right">
                    <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
            
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($service, ['method' => 'PATCH','route' => ['services.update', $service->service_id]]) !!}
        @include('services.form')
    {!! Form::close() !!}
   
    </div>
 </div>


<!-- Modal Structure for adding Customer -->
  <div id="addservice" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Insert New Record</h4>
            <div class="pull-right">
                    <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
            
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
     {!! Form::open(array('route' => 'services.store','method'=>'POST')) !!}
         @include('services.form')
    {!! Form::close() !!}
   
    </div>
 </div>
@endforeach
<script>
    $(document).ready(function(){
      $("#search_text").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#servicetable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
</script>



{!! $services->render() !!}



@endsection