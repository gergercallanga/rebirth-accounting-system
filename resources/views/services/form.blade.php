<br>
<div class="container" style="width: 90%">
 
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Service Name</strong>
            {!! Form::text('service_name', null, array('placeholder' => 'Service Name','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Price</strong>
            {!! Form::number('price', null, array('placeholder' => 'Price','class' => 'form-control')) !!}
        </div>
    </div>

     <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
            <strong>Status</strong>
            {!! Form::text('status', null, array('placeholder' => 'Status(Package/Normal)','class' => 'form-control')) !!}
        </div>
    </div>
    
     <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
            <strong>Remarks</strong>
            {!! Form::text('remarks', null, array('placeholder' => 'Remarks','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
