@extends("layouts.parent")
@section("content")
 <div class="table-responsive">
    <table class="table">
        <thead>
        <tr>

            <th>ID</th>
            <th>Causer Name</th>
            <th>Causer Username</th>
            <th>Subject Model/Table</th>
            <th>Description of Changes</th>
            <th>Changes Made</th>
            <th>Date</th>
        </tr>
        </thead>
        <tbody>

        @foreach(json_decode($logs, true) as $activity)
            <tr>
                <td>{{$activity['id']}}</td>

                <td>{{$activity['users']['fname']}} {{$activity['users']['lname']}}</td>
                <td> {{$activity['users']['username']}} </td>

                <td>{{$activity['subject_type']}}</td>
                <td>{{$activity['description']}}</td>
                <td>
                    <div class="row">
                        <ul>
                            @foreach($activity['properties'] as $properties)
                                @isset($properties['attributes'])
                                     <li>{!! json_encode($properties['attributes'][0]['fname']) !!}</li>
                                @endisset
                            @endforeach
                        </ul>

                    </div>

                <td>{{$activity['created_at']}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
