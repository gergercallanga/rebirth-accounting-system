<br>
<div class="container" style="width: 90%">
 

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Product:</strong>
            {!! Form::text('product', null, array('placeholder' => 'Product Name','class' => 'form-control')) !!}
        </div>
    </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Description:</strong>
            {!! Form::text('description', null, array('placeholder' => 'Description','class' => 'form-control')) !!}
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Quantity:</strong>
            {!! Form::number('quantity', null, array('placeholder' => 'Quantity','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Original Price:</strong>
            {!! Form::number('orig_price', null, array('placeholder' => 'Original Price','class' => 'form-control')) !!}
        </div>
    </div>

     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Final Price</strong>
            {!! Form::number('final_price', null, array('placeholder' => 'Final Price','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>  

