@extends('layouts.default')
@section('content')

<br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Inventory Information</h3>
            </div>
        </div>
    </div> 
    <div class="row">
    <div class="col-lg-12 margin-tb" style="margin-top: -30px;">
          <div class="input-field col s5" style="margin-top: 25px;">
                <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Insert New Item" href="#additem"><i class="material-icons">add</i></a>
        </div>
        <div class="input-field col s5 pull-right">
          <i class="material-icons prefix">search</i>
          {!! Form::text('seacrh_text', null, array('class' => 'search_text','id'=>'search_text')) !!}
          <label for="search_text">Search</label>
        </div> 
    </div>
    </div>
       
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered hoverTable" style="margin-top: -30px;" id="example">
        <tr class="#bdbdbd grey lighten-1">
            <th>Inventory ID</th>
            <th>Product</th>
            <th>Description</th>
            <th>Remaining Quantity</th>
        </tr>

<tbody id="inventorytable">

    @foreach ($inventories as $inventory)
    <tr>
        <td><a class="modal-trigger" href="#show{{$inventory->id}}" style="color:black;">{{ $inventory->id}}</a></td>
        <td><a class="modal-trigger" href="#show{{$inventory->id}}" style="color:black;">{{ $inventory->product}}</a></td>
        <td><a class="modal-trigger" href="#show{{$inventory->id}}" style="color:black;">{{ $inventory->description}}</a></td>
         <td><a class="modal-trigger" href="#show{{$inventory->id}}" style="color:black;">{{ $inventory->quantity}}</a></td>
    </tr>
    @endforeach
    </tbody>
    </table>

 @foreach ($inventories as $inventory)

  <!-- Modal Structure for Show Inventory-->
  <div id="show{{$inventory->id}}" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Item Full Details</h4>
            <div class="pull-right">
             <a class="waves-effect waves-light btn modal-trigger tooltipped" data-tooltip="Edit" href="#edit{{$inventory->id}}"><i class="material-icons">edit</i></a>

            {!! Form::open(['method' => 'DELETE','route' => ['inventories.destroy', $inventory->id],'style'=>'display:inline'])!!}
            {{ Form::button('<i class="material-icons">delete</i>', array('type' => 'submit', 'class' => 'waves-effect waves-light red btn modal-trigger tooltipped','data-tooltip' => 'Delete'))}}
            {!! Form::close() !!}
            </div>
        </div> 
<div class="modal-body">
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Inventory ID:</strong>
                {{ $inventory->id}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product:</strong>
                {{ $inventory->product}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description:</strong>
                {{ $inventory->description}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Quantity:</strong>
                {{ $inventory->quantity}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Original Price:</strong>
                {{ $inventory->orig_price}}
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Final Price:</strong>
                {{ $inventory->final_price}}
            </div>
        </div>

    </div>
    </div>
    
    <div class="modal-footer">
            <button type="button" class="btn btn-default modal-action modal-close" >Close</button>
    </div>
    </div>
    </div>

  <!-- Modal Structure for Edit INventory -->
  <div id="edit{{$inventory->id}}" class="modal">
    <div class="modal-content">
        <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Product Information</h2>
            </div>
            <div class="pull-right">
                <button type="button" class="btn btn-default modal-action modal-close" >Back</button>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($inventory, ['method' => 'PATCH','route' => ['inventories.update', $inventory->id]]) !!}
        @include('inventories.form_inventory')
    {!! Form::close() !!}
    </div>
  </div>

  <div id="additem" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Insert New Supplier</h4>
            <div class="pull-right">
                    <a class="btn btn-default modal-action modal-close"> Back</a>
            </div>
        </div>
            
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
     {!! Form::open(array('route' => 'inventories.store','method'=>'POST')) !!}
         @include('inventories.form_inventory')
    {!! Form::close() !!}
   
    </div>
 </div>

  @endforeach
<script>
$(document).ready(function(){
  $("#search_text").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#inventorytable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

    {!! $inventories->render() !!}
@endsection