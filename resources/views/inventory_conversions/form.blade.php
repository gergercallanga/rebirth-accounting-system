
<div class="row">
    <form class="col s12">
      <div class="row" style="margin-top: -20px;">
        <div class="input-field col s3">
        <i class="material-icons prefix">shopping_cart</i>
           <select name="item_name" id="item_name">
           <option>Select Item</option>
          @foreach ($inventories as $inventory)
        <option value="{{ $inventory->product }}" data-code="{{ $inventory->id }}"> {{ $inventory->product }} </option>
          @endforeach

          </select>
          <label for ="item_name">Item Name</label>
        </div>
        <div class="input-field col s1"></div>
           {!! Form::hidden('item_code',null,array('class' => 'form-control', 'id' => 'item_code')) !!}
        <div class="input-field col s1"></div>
        <div class="input-field col s3">
          <i class="material-icons prefix">date_range</i>
           {!! Form::text('date_converted', null, array('class' => 'datepicker')) !!}
          <label for="date_received">Receiving Date</label>
      </div>
    </div>
      <div class="row" style="margin-top: -15px;">
       <div class="input-field col s6">
         <h5>FROM:</h5>
        </div>  
        <div class="input-field col s6">
          <h5>TO:</h5>
        </div>
      </div>

       <div class="row" style="margin-top: -15px;">
       <div class="input-field col s1"></div>
        <div class="input-field col s4">
          <i class="material-icons prefix">local_offer</i>
           {!! Form::number('fr_cost', null, array('class' => 'form-control')) !!}
          <label for="fr_cost">Cost</label>
        </div>
        <div class="input-field col s2"></div>
         <div class="input-field col s4">
          <i class="material-icons prefix">local_offer</i>
           {!! Form::number('to_cost', null, array('class' => 'form-control')) !!}
          <label for="to_cost">Cost</label>
        </div>
      </div>

      <div class="row" style="margin-top: -15px;">
      <div class="input-field col s1"></div>
        <div class="input-field col s4">
          <i class="material-icons prefix">local_offer</i>
           {!! Form::number('fr_qty', null, array('class' => 'form-control')) !!}
          <label for="fr_qty">Quantity</label>
        </div>
        <div class="input-field col s2"></div>
         <div class="input-field col s4">
          <i class="material-icons prefix">local_offer</i>
           {!! Form::number('to_qty', null, array('class' => 'form-control')) !!}
          <label for="to_qty">Quantity</label>
        </div>
      </div>
      <br>
      
      <div class="row" style="margin-top: -15px;">
      <div class="input-field col s1"></div>
      <div class="input-field col s10">
       <i class="material-icons prefix">local_offer</i>
           {!! Form::text('remarks', null, array('class' => 'form-control')) !!}
          <label for="remarks">Remarks</label>
          </div>
      </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
        </form>
      </div>   