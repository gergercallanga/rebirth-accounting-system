@extends('layouts.default')
@section('content')
<style>
    .hoverTable{
        width:100%; 
        border-collapse:collapse; 
    }
    .hoverTable td{ 
        padding:7px; border:#4e95f4 1px solid;
    }
    /* Define the default color for all the table rows */
    .hoverTable tr{
        background: #f5f5f5;
    }
     .hoverTable tr:hover {
          background-color: #ffebee;
      }
</style>
<br>
<h4>Inventory Conversion</h4>
<br>

<ul class="collapsible #f5f5f5 grey lighten-4" data-collapsible="accordion">
    <li>
      <div class="collapsible-header waves-effect waves-red"><i class="material-icons">add_box</i>Make Conversion</div>
      <div class="collapsible-body"><span>
   
 @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
 {!! Form::open(array('route' => 'inventory_conversion.store','method'=>'POST')) !!}
         @include('inventory_conversions.form')
{!! Form::close() !!}

 </span></div>
   </li>
</ul>

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h5>Conversion Records</h5>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered hoverTable">
        <tr class="#bdbdbd grey lighten-1"> 

            <th>Date date_converted</th>
            <th>Item Name</th>
            <th>Cost (from)</th>
            <th>Quantity (from)</th>
            <th>Cost (to)</th>
            <th>Quantity (to)</th>
            <th>Remarks</th>
        </tr>


    @foreach ($inventory_conversions as $converts)
    <tr>
        <td><a class="modal-trigger" href="#edit{{$converts->id}}" style="color:black;">{{ $converts->date_converted}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $converts->item_name}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $converts->fr_cost}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $converts->fr_qty}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $converts->to_cost}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $converts->to_qty}}</a></td>
        <td><a class="modal-trigger" href="#" style="color:black;">{{ $converts->remarks}}</a></td>


    </tr>
    @endforeach
    </table>
   
  
<script>
$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    format: 'yyyy-mm-dd',
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false // Close upon selecting a date,
  });
</script>
<script>
      $(document).ready(function() {
    $('select').material_select();
  });

    $('#item_name').on('change',function(){
    var code = $(this).children('option:selected').data('code');
    $('#item_code').val(code);
});
          
</script>
  

    {!! $inventory_conversions->render() !!}
@endsection