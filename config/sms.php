<?php
/**
 * Created by PhpStorm.
 * Date: 15/01/2018
 * Time: 7:46 PM
 */

return [

    'sms_email' => env('SMS_EMAIL'),

    'sms_password' => env('SMS_PASSWORD'),

    'sms_device_id' => env('SMS_DEVICE_ID')
];