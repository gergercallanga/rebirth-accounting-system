To install this repository:

type commands on your cmd and follow the steps:

Go to htdocs folder : cd C:\xampp\htdocs

Clone repository : git clone <path to repository copy on top>

Go to your repository folder: cd rebirth-accounting-system

update composer(will load all dependency of your project): composer install or composer update

the sql file is located on root folder rebirth.sql import this file to your db instead of running artisan migrate command

copy .env.example and edit this one to your db configuration



type php artisan key:generate
