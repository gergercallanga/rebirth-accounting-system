<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemcostAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itemcost_adjustments', function (Blueprint $table) {
            $table->increments('id');
            $table->date('adjustdate');
            $table->integer('item_code');
            $table->integer('amt_in');
            $table->integer('amt_out');
            $table->integer('runningqty');
            $table->float('runningbal');
            $table->float('averagecost');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itemcost_adjustments');
    }
}
