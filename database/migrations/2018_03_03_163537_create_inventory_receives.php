<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryReceives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_receives', function (Blueprint $table) {

            $table->increments('id');
            $table->date('date_received');
            $table->string('item_code');
            $table->string('item_name');
            $table->integer('item_qty');
            $table->float('item_unit_cost');
            $table->float('item_total_cost');
            $table->integer('supplier_id');
            $table->string('supplier_name');
            $table->string('remarks');
            $table->timestamps();
    });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_receives');
    }
}
