<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('joborders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('vehicle_id');
            $table->integer('inquiry_id');
            $table->integer('optional_deposit_id');
            $table->string('first_check_desc');
            $table->string('second_check_desc');
            $table->string('final_check_desc');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('joborders');
    }
}
