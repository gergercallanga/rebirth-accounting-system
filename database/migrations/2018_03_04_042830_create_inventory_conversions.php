<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryConversions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_conversions', function (Blueprint $table) {

            $table->increments('id');
            $table->date('date_converted');
            $table->string('item_code');
            $table->string('item_name');
            $table->float('fr_cost');
            $table->integer('fr_qty');
            $table->float('to_cost');
            $table->integer('to_qty');
            $table->string('remarks');
            $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('inventory_conversions');
    }
}
