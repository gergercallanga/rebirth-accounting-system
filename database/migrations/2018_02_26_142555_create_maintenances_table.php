<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

 // 'id', 'customer_id', 'vehicle_id', 'description', 'sched_from','sched_to', 'from_time', 'to_time', 'amount', 'status', 'note'


        Schema::create('maintenances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('vehicle_id');
            $table->string('description');
            $table->date('sched_from');
            $table->date('sched_to');
            $table->time('from_time');
            $table->time('to_time');
            $table->integer('amount');
            $table->string('status');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenances');
    }
}
