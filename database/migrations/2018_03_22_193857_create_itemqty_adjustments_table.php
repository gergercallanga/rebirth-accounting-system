<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemqtyAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('itemqty_adjustments', function (Blueprint $table) {
            $table->increments('id');
            $table->date('adjustdate');
            $table->integer('item_code');
            $table->integer('qtyin');
            $table->integer('qtyout');
            $table->integer('runningqty');
            $table->float('runningbal');
            $table->float('averagecost');
            $table->string('remarks');
            $table->timestamps();
        });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itemqty_adjustments');
    }
}
