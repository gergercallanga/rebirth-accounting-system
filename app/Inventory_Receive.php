<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory_Receive extends Model
{

	 

	protected $table = "inventory_receives";
    protected $fillable = [
    'id','date_received','item_code', 'item_name','item_qty', 'item_unit_cost', 'item_total_cost', 'supplier_id', 'supplier_name','remarks'
    ];
}
