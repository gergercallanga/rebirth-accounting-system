<?php

namespace App\Providers;

use App\Library\Services\SmsGateway;
use Illuminate\Support\ServiceProvider;

class SmsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\SmsGateway', function ($app) {
            return new smsGateway(config('sms.sms_email'), config('sms.sms_password'), config('sms.sms_device_id'));
        });
    }
}
