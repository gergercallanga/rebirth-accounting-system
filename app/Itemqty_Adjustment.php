<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itemqty_Adjustment extends Model
{
    protected $table = "itemqty_adjustments";
    protected $fillable = [
    'id','adjustdate','item_code','qtyin', 'qtyout', 'runningqty','runningbal', 'averagecost','remarks'
    ];

     public function down()
    {
        Schema::dropIfExists('itemqty_adjustments');
    }
}