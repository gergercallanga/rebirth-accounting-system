<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Joborder extends Model
{
    protected $table = "joborders";
    protected $guarded = array();

    public function maintenance()
    {
        return $this->belongsToMany( 'App\Maintenance');
    }
    public function customer()
    {
           return $this->belongsTo('App\Customer');
    }
    public function vehicles()
    {
        return $this->belongsTo('App\Vehicle','vehicle_id');
    }
    // public function items()
    // {
    //     return $this->belongsTo('App\Service_Items','item_id');
    // }
    
}
