<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itemcost_Adjustment extends Model
{
    protected $table = "itemcost_adjustments";
    protected $fillable = [
    'id','adjustdate','item_code','amt_in', 'amt_out', 'runningqty','runningbal', 'averagecost','remarks'
    ];
}
