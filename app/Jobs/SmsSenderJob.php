<?php

namespace App\Jobs;

use App\Http\Requests\SmsRequest;
use App\Library\Services\SmsGateway;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;

class SmsSenderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $request_params;
    protected $sms_gateway;
    public function __construct($request, SmsGateway $sms_gateway)
    {

       $this->request_params = $request;
       // Log::info('Showing log params for queue '. json_encode($this->request_params['receiver_num'])." \n".json_encode($this->sms_gateway));
        $this->sms_gateway = $sms_gateway;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::warning('Showing log params for queue '. json_encode($this->request_params['receiver_num'])." \n".json_encode($this->sms_gateway));
        $result = $this->sms_gateway->sendMessageToNumber($this->request_params['receiver_num'], $this->request_params['msg_body']);
        Log::info(json_encode($result));
    }
}
