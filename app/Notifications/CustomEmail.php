<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CustomEmail extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
  public function __construct($subject, $message)
    {
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
            //     return (new MailMessage)
            //                 ->level('info')
            //                 ->line('The introduction to the notification.')
            //                 ->action('Notification Action', url('/'))
            //                 ->line('Thank you for using our application!');

            // $user = new user();
            // $user->email = 'charlotnesniapanal@gmail.com';   // This is the email you want to send to.
            // $user->notify(new TemplateEmail());
            // }

         return (new MailMessage)
            ->subject($this->subject)
            ->view('email_message', ['message' => $this->message]);
        }   

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
