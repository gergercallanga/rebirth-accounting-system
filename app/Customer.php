<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
       'id','name','nickname','address', 'telephone', 'mobile','email'
    ];

    public function vehicles()
    {
        return $this->hasMany('App\Vehicle','customer_id');
    }

       
}
