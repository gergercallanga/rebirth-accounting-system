<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UsersPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Request::isMethod('POST'))
        {
            return [
                'fname' => 'required|unique:users|max:255',
                'lname' => 'required',
                'mname' => 'required',
                'email' => 'required|email',
                'contact_num' => 'required|numeric',
                'username' => 'required|unique:users',
                'role_chbox' => 'required'
            ];

        }elseif (Request::isMethod('PUT')){
            return [
                'fname' => 'required|max:255',
                'lname' => 'required',
                'mname' => 'required',
                'email' => 'required|email',
                'contact_num' => 'required|numeric',
                'username' => 'required',
                'role_chbox' => 'required'
            ];
        }

    }
}
