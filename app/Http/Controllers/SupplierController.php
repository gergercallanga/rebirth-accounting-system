<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;

class SupplierController extends Controller
{


     public function index()
    {
        $suppliers = Supplier::latest()->paginate(10);
        return view('suppliers.index_supplier',compact('suppliers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        return view('suppliers.create_supplier');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        request()->validate([
            // 'customer_id' => 'required',
            'supplier_name' => 'required',
            'supplier_address' => 'required',
           
        ]);
        Supplier::create($request->all());
        return redirect()->route('suppliers.index')
                        ->with('success','Supplier created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show(Supplier $supplier)
    {
        return view('suppliers.show_supplier',compact('supplier'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit(Supplier $supplier)
    {
        return view('suppliers.edit_supplier',compact('supplier'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request,Supplier $supplier)
    {
        request()->validate([
            'supplier_name' => 'required',
            'supplier_address' => 'required',

// 'id', 'customer_id', 'vehicle_id', 'description', 'sched_on', 'from_time', 'to_time', 'amount', 'status', 'note' 

        ]);
        $supplier->update($request->all());
        return redirect()->route('suppliers.index')
                        ->with('success','Supplier updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        Supplier::destroy($id);
        return redirect()->route('suppliers.index')
                        ->with('success','Supplier deleted successfully');
    }
}
