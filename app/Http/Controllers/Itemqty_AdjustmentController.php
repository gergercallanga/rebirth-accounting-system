<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Itemqty_Adjustment;
use App\Inventory;

class Itemqty_AdjustmentController extends Controller
{
     public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
    	$inventories = Inventory::all();
         $itemqty_adjustments = Itemqty_Adjustment::latest()->paginate(10);
        return view('itemqty_adjustments.index',compact('itemqty_adjustments','inventories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

     public function create()
    {
       return view('itemqty_adjustments.index');
    }

    public function store(Request $request)
    {
         request()->validate([

            'adjustdate'=>'required',
            'item_code' => 'required',
            'qtyin' => 'required',
            'qtyout' => 'required',
            'runningqty' => 'required',
            'runningbal' => 'required',
            'averagecost' => 'required',
            'remarks' => 'required',

            //'id','adjustdate','item_code','qty_in', 'qty_out', 'runningqty','runningbal', 'averagecost','remarks'
            
        ]);
        Itemqty_Adjustment::create($request->all());
        $inventory=Inventory::find($request->item_code);
        $qty= $inventory->quantity + $request->qtyin;
        $qty= $qty - $request->qtyout;
        $inventory->quantity = $qty;
        $inventory->save();


        return redirect()->route('itemqty_adjustment.index')
        				->with('success', 'Adjustment Done!');
	}
	 public function show(Itemqty_Adjustment $itemqty_adjustments)
    {
      return view('itemqty_adjustments.index',compact('itemqty_adjustments'));
    }

    public function edit(Itemqty_Adjustment $itemqty_adjustments)
    {
      return view('itemqty_adjustments.index',compact('itemqty_adjustments'));
    }


    public function update(Request $request, $id)
    {
        request()->validate([

            'adjustdate'=>'required',
            'item_code' => 'required',
            'qtyin' => 'required',
            'qtyout' => 'required',
            'runningqty' => 'required',
            'runningbal' => 'required',
            'averagecost' => 'required',
            'remarks' => 'required',
        ]);


        $itemqty_adjustments = Itemqty_Adjustment::find($id)->update($request->all());
         return redirect()->route('itemqty_adjustment.index')
        				->with('success', 'Adjustment Update Done!');
    }
        public function destroy($id)
    {
       
    }
}
