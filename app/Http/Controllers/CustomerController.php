<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
    	// $valuee=customer::all();
    	// return view('customer.index',compact('valuee'));

        $customers = Customer::with('vehicles')->latest()->paginate(20);
        
        return view('customers.index_customer',compact('customers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        return view('customers.create_customer');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        request()->validate([
            // 'customer_id' => 'required',
            'name' => 'required',
            'nickname' => 'required',
            'address' => 'required',
            // 'telephone' => 'required',
            'mobile' => 'required',
            'email' => 'required',
            

 // 'id','name','nickname','address', 'telephone', 'mobile','email'

        ]);
        Customer::create($request->all());
        return redirect()->route('customers.index')
                        ->with('success','Customer created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show(Customer $customer)
    {
        return view('customers.show_customer',compact('customer'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit(Customer $customer)
    {
        return view('customers.edit_customer',compact('customer'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request,Customer $customer)
    {
        request()->validate([
            // 'id' => 'required',
            'name' => 'required',
            'nickname' => 'required',
            'address' => 'required',
            // 'telephone' => 'required',
            'mobile' => 'required',
            'email' => 'required',

// 'id', 'customer_id', 'vehicle_id', 'description', 'sched_on', 'from_time', 'to_time', 'amount', 'status', 'note' 

        ]);
        $customer->update($request->all());
        return redirect()->route('customers.index')
                        ->with('success','Customer updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        Customer::destroy($id);
        return redirect()->route('customers.index')
                        ->with('success','Customer deleted successfully');
    }
}
