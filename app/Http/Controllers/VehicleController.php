<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Vehicle;

class VehicleController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

     public function index()
    {
        $vehicles = Vehicle::latest()->paginate(10);
        return view('vehicles.index',compact('vehicles'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vehicles.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        request()->validate([
            'customer_id' => 'required',
            'type' => 'required',
            'model' => 'required',
            'plate' => 'required',
            'color' => 'required',

        ]);

        Vehicle::create($request->all());
        return redirect()->route('vehicles.index')
                        ->with('success','Vehicle created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function show(Vehicle $vehicle)
    {
        return view('vehicles.show',compact('vehicle'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit(Vehicle $vehicle)
    {
        return view('vehicles.edit',compact('vehicle'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request,Vehicle $vehicle)
    {
        request()->validate([
            'customer_id' => 'required',
            'type' => 'required',
            'model' => 'required',
            'plate' => 'required',
            'color' => 'required',
        ]);

        $vehicle->update($request->all());
        return redirect()->route('vehicles.index')
                        ->with('success','Vehicle updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Vehicle::destroy($id);
        return redirect()->route('vehicles.index')
                        ->with('success','Vehicle deleted successfully');
    }


}

