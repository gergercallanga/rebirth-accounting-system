<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Itemcost_Adjustment;
use App\Inventory;
class Itemcost_AdjustmentController extends Controller
{
     public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
    	$inventories = Inventory::all();
         $itemcost_adjustments = Itemcost_Adjustment::latest()->paginate(10);
        return view('itemcost_adjustments.index',compact('itemcost_adjustments','inventories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
       return view('itemqty_adjustments.index');
    }

    public function store(Request $request)
    {
         request()->validate([

            'adjustdate'=>'required',
            'item_code' => 'required',
            'amt_in' => 'required',
            'amt_out' => 'required',
            'runningqty' => 'required',
            'runningbal' => 'required',
            'averagecost' => 'required',
            'remarks' => 'required',

            
        ]);
        Itemcost_Adjustment::create($request->all());

        $inventory=Inventory::find($request->item_code);
        $qty= $inventory->quantity + $request->qtyin;
        $qty= $qty - $request->qtyout;
        $inventory->quantity = $qty;
        $inventory->save();

        return redirect()->route('itemcost_adjustment.index')
        				->with('success', 'Adjustment Done!');
	}

	public function show(Itemcost_Adjustment $itemcost_adjustments)
    {
      return view('itemcost_adjustments.index',compact('itemcost_adjustments'));
    }

    public function edit(Itemcost_Adjustment $itemcost_adjustments)
    {
      return view('itemcost_adjustments.index',compact('itemcost_adjustments'));
    }

    public function update(Request $request, $id)
    {
        request()->validate([

            'adjustdate'=>'required',
            'item_code' => 'required',
            'amt_in' => 'required',
            'amt_out' => 'required',
            'runningqty' => 'required',
            'runningbal' => 'required',
            'averagecost' => 'required',
            'remarks' => 'required',
        ]);


        $itemcost_adjustments = Itemcost_Adjustment::find($id)->update($request->all());
         return redirect()->route('itemcost_adjustment.index')
        				->with('success', 'Adjustment Update Done!');
    }
        public function destroy($id)
    {
       
    }

}
