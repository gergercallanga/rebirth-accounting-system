<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::latest()->paginate(10);
        return view('services.index',compact('services'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            // 'service_id' => 'required',
            'service_name' => 'required',
            'price' => 'required',
            'status' => 'required',
            'remarks' => 'required',
        ]);

 // 'service_id','service_name','price','status','remarks'

        Service::create($request->all());
        return redirect()->route('services.index')
                        ->with('success','Service created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show(Service $service)
    {
        return view('services.show',compact('service'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit(Service $service)
    {
        return view('services.edit',compact('service'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Service $service)
    {
        request()->validate([
            // 'service_id' => 'required',
            'service_name' => 'required',
            'price' => 'required',
            'status' => 'required',
            'remarks' => 'required',
        ]);
        $service->update($request->all());
        return redirect()->route('services.index')
                        ->with('success','Service updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($service_id)
    {
        Service::destroy($service_id);
        return redirect()->route('services.index')
                        ->with('success','Service deleted successfully');
    }
}
