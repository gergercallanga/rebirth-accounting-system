<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserPost;
use App\Http\Requests\UsersPost;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;
use Spatie\Activitylog\Models\Activity;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:create users', ['only' =>['store']]);
        $this->middleware('permission:edit users', ['only' =>['edit','update']]);
    }

    public function view($id){

    }

    public function show($id){
        try{
            $id = Crypt::decrypt($id);

            $user = User::findorFail($id);
            return $user;
        }catch (Exception $e){

        }
    }
    public function create(){

    }

    public function store(UsersPost $request){
        try{
            DB::transaction(function () use ($request) {
                $request['password'] = bcrypt(str_random(6));
                $user = new User($request->all());
                
                $user->save();
                
                foreach ($request->role_chbox as $roles) {
                    $role_name = Role::find($roles);
                    $user->assignRole($role_name);
                }


            });
            $this->sendResetLinkEmail($request);
            return redirect()->back();
        }catch (Exception $e){
            return view('errors.500');
        }
    }

    public function edit($id){
        try{
            $id = Crypt::decrypt($id);
            $user = json_decode(User::with('roles')->find($id));

            return response(json_encode($user))
                ->header('Content-Type', 'application/json');
        }catch (Exception $e){
            return $e;
        }

    }

    public function update($id, UsersPost $request){

        try{
            $id = Crypt::decrypt($id);
            //check if username exist
            if((User::where('username', $request->username)->where('id','!=',$id)->exists())){
                return redirect()->back()->with('success', 'Username Already Exist');
            }
            DB::transaction(function () use ($request, $id) {

                $request_keys=['fname','lname','mname','contact_num','username','email'];
                $user =  User::find($id);
                $role_name = Role::find($request->role_chbox)->keyBy('name')->toArray();
                $role_name = array_keys($role_name);

                $user->syncRoles($role_name);
                $user->update($request->only($request_keys));

            });

            return redirect()->back()->with('success', 'User Updated');

        }catch (Exception $e){

        }

    }

    public function userToggleStatus(Request $request){
        $request->user_id = Crypt::decrypt($request->user_id);
        //check if user is active or not
        $user_status = User::find($request->user_id);
        $user_status->status = !$user_status->status;

        $user_status->save();
        if($user_status->status){
            $msg = "User Activated Successfully";
        }else{
            $msg = "User Deactivated Successfully";
        }
        return redirect()->back()->with('success', $msg);

    }


    public function UserManagement(){
        $role = Role::all()->toJson();
        $newroles = Role::with("permissions")->get()->toJson();
        $user_roles = User::with("roles")->get()->toJson();
        $permission = Permission::with("roles")->get()->toJson();

        return view("userManagement.userManagement",compact('role','newroles','user_roles','permission','logs'));
    }

    public function index()
        {

            $user = User::all();
            
            return view('userprofiles.index',compact('user'))
                ->with('i', (request()->input('page', 1) - 1) * 5);
        }

    public function imageUpload()

        {

            return view('userprofiles.index');

        }

    public function imageUploadPost(Request $request)

        {

            request()->validate([

                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

            ]);

            $imageName = time().'.'.request()->image->getClientOriginalExtension();

            request()->image->move(public_path('images/users'), $imageName);

            $user=User::find(Auth::user()->id);
            $user->avatar = "users/".$imageName;
            $user->save();

            return redirect()->route('userprofiles.index')
                ->with('success','You have successfully upload image.')
                ->with('image',$imageName);

        }

}