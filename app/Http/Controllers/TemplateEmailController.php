<?php

namespace App\Http\Controllers;
use App\Notifications\CustomEmail;
use App\User;
use Illuminate\Http\Request;

class TemplateEmailController extends Controller
{
	

	 public function customEmail(Request $request)
    {
        if ($request->isMethod('get'))
            return view('custom_email');
        else {
            $rules = [
                'to_email' => 'required|email',
                'subject' => 'required',
                'message' => 'required',
            ];
            $this->validate($request, $rules);
            $user = new User();
            $user->email = $request->to_email;
            $user->notify(new CustomEmail($request->subject, $request->message));
            $request->session()->put('status', true);
            return back();
        }
    }

    //  public function sendEmail(Request $request){

	// 		return (new MailMessage)
 //                    ->level('info')
 //                    ->line('The introduction to the notification.')
 //                    ->action('Notification Action', url('/'))
 //                    ->line('Thank you for using our application!');

 //    $user = new user();
	// $user->email = 'charlotnesniapanal@gmail.com';   // This is the email you want to send to.
	// $user->notify(new TemplateEmail());
	// }
}
