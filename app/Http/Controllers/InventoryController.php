<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;

class InventoryController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

     public function index()
    {
        $inventories = Inventory::latest()->paginate(5);
        return view('inventories.index_inventory',compact('inventories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        return view('inventories.create_inventory');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        request()->validate([
            // 'customer_id' => 'required',
            // 'id' => 'required',
            'product' => 'required',
            'description' => 'required',
            'quantity' => 'required',
            'orig_price' => 'required',
            'final_price' => 'required',
       

            // 'id','product', 'description','quantity', 'orig_price', 'final_price'
        ]);
        Inventory::create($request->all());
        return redirect()->route('inventories.index')
                        ->with('success','Product created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show(Inventory $inventory)
    {
        return view('inventories.show_inventory',compact('inventory'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit(Inventory $inventory)
    {
        return view('inventories.edit_inventory',compact('inventory'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request,Inventory $inventory)
    {
        request()->validate([
            'product' => 'required',
            'description' => 'required',
            'quantity' => 'required',
            'orig_price' => 'required',
            'final_price' => 'required',

        ]);
        $inventory->update($request->all());
        return redirect()->route('inventories.index')
                        ->with('success','Product updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        Inventory::destroy($id);
        return redirect()->route('inventories.index')
                        ->with('success','Product deleted successfully');
    }
}
