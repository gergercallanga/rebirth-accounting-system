<?php

namespace App\Http\Controllers;

use App\CheckList_Descriptions;
use App\Customer;
use App\Joborder;
use App\Inquiry;
use App\PaymentType;
use App\Images_Checklist;
use App\Image;
use App\Service;
use App\Joborder_Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;


class JobOrderController extends Controller
{

    public function index(){
        $joborder = Joborder::with("customer")->with("vehicles")->get();
        $check_list_desc  = CheckList_Descriptions::all();
        $inquiries = Inquiry::all();
        $paymenttypes = PaymentType::all();
        $images_checklists=Images_Checklist::all();
        $images=Image::all();

        return view("joborder.index", compact("joborder","check_list_desc","inquiries","paymenttypes","images_checklists","images"));
    }
    public function showCustomerVehicles(){
        $customer_vehicle = Customer::with("vehicles")->get();
        $customer_vehicle = ["data" => $customer_vehicle->toArray(),
            "recordsTotal" => $customer_vehicle->count(),
            "recordsFiltered" => $customer_vehicle->count(),
            "draw" => 1];
        return json_encode($customer_vehicle);
    }

     public function addJoborder( Request $request){
                $joborder = new Joborder();
                $joborder->customer_id = $request->input("customer_id");
                $joborder->vehicle_id = $request->input("vehicle_id");
                $joborder->save();
        return $joborder->id;


   }

    public function showServices(){
        $services = Service::all();
        $allservices = ["data" => $services->toArray(),
            "recordsTotal" => $services->count(),
            "recordsFiltered" => $services->count(),
            "draw" => 1];
        return json_encode($allservices);
    }

    public function checkListUpload(Request $request){
        $path = array();
        $file_upload_path = 'images/uploads';
      try{
     
        foreach($request->files as $files){
            foreach ($files as $key => $file) {
               
                if(is_file($file)) {    // not sure this is needed
                    $file_name = $file->getClientOriginalName();
                    $string = time().str_random(3).$key;
                    $ext = $file->guessExtension();
                    $file_name = $string . '.' .  $ext;
                    $filepath = 'images' . $file_name;

                   if( $file->move(public_path($file_upload_path), $file_name) ){
                        $this->saveChecklistToDb($file_upload_path.$filepath,$request->input('joborder_id'),$request->input('checklist_id'),$request->input('check_description'));
                    }
                    // $file->move(public_path('images/uploads'), $file_name);
                    // $image=new Images_Checklist();
                    // $image->checklist_number = 1;
                    // $image->image_path=$filepath;
                    // $image->save();
                }   
            }
        }
      }catch (Exception $e){
            return $e;
      }

        return $path;
    }

      public function saveChecklistToDb(  $filepath,$joborder_id,$checklist_id,$description){
            $image = new Images_Checklist();
            $image->image_path = $filepath;
            $image->joborder_id=$joborder_id;
            $image->checklist_number = $checklist_id;
            $joborder =Joborder::find($joborder_id);
                switch ($image->checklist_number) {
                    case "1":
                       $joborder->first_check_desc = $description;
                        break;
                    case "2":
                       $joborder->second_check_desc = $description;
                        break;
                    case "3":
                       $joborder->final_check_desc = $description;
                        break;
                    
                    default:
                        # code...
                        break;
                }



            $image->save();
            $joborder->save();

            //  $description =Joborder::find($request->joborder_id);
            // $description->first_check_desc = $check_list;
            // $description->save();

    }




    public function saveServiceToDb(Request $request){
            $service =Joborder::find($request->joborder_id);
            $service->total_amt_rendered = $request->total_amount_rendered;
            $service->save();
            $joborder_service=Joborder_Service::where("joborder_id",$request->joborder_id)->delete();
          
            $serv = explode("|",$request->service_id);
                       foreach($serv as $key => $value){
                $joborder_service=new Joborder_Service();
                $joborder_service->joborder_id=$request->joborder_id;
                $joborder_service->services_id=$value;
                $joborder_service->save();
            }


            return json_encode(["msg" => "success"]);

    }

      public function saveDescriptionToDb(Request $request){
            $description =Joborder::find($request->joborder_id);
            $description->description = $request->description;
            $description->save();
          
            }

     public function saveInquiry( Request $request){
        $joborder=Joborder::find($request->input("joborder_id"));
        $joborder->inquiry_id = $request->input("inquiry_name");
        $joborder->save();

        $payment = new Payment();
        $payment->joborder_id = $request->input("joborder_id");
        $payment->payment_type_id = $request->input("ptype");
        $payment->amount = $request->input("amount");
        $payment->type = "Deposit";
        $payment->save();
   }


           
    

    public function view($id){

    }
    public function create(){

    }
    public function store(){

    }
    public function edit(){

    }
    public function update(){

    }
    public function delete(){

    }
    public function destroy(){

    }
}