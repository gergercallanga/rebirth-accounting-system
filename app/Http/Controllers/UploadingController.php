<?php
namespace App\Http\Controllers;

use App\Customer;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class UploadingController extends Controller
{
    public function index(Request $request)
    {
        $images = Image::orderBy('created_at', 'desc')->paginate(8);
        return view('uploading.index', compact('images'));
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get'))
            return view('uploading.form');
        else {
            $rules = [
                'description' => 'required',
            ];
            $this->validate($request, $rules);
            $image = new Image();
            if ($request->hasFile('image')) {
                   $file_name = $file->getClientOriginalName();
                    $string = time().str_random(3).$key;
                    $ext = $file->guessExtension();
                    $file_name = $string . '.' .  $ext;
                    $filepath = 'images' . $file_name;

                   if( $file->move(public_path('images/uploads'), $file_name) ){
                        $this->saveChecklistToDb($filepath);
                    }

            }
            $image->description = $request->description;

            $image->save();
            return redirect('/imageuploads');
        }
    }
    public function saveChecklistToDb($filepath){
            $image = new Images_Checklist();
            $image->checklist_number = 1;
            $image->image_path = $filepath;
            $image->save();
    }

    public function delete($id)
    {
       
         $image = Image::find($id);
         // dd($image);
        // chmod("uploads/".$image, 0777);
        // unlink("uploads/".$image);
        unlink("uploads/".$image->image);
        Image::destroy($id);
        return redirect('/imageuploads');

    }

    public function update(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('uploading.form', ['image' => Image::find($id)]);
        else {
            $rules = [
                'description' => 'required',
            ];
            $this->validate($request, $rules);
            $image = Image::find($id);
            if ($request->hasFile('image')) {
                $dir = 'uploads/';
                if ($image->image != '' && File::exists($dir . $image->image))
                    File::delete($dir . $image->image);
                $extension = strtolower($request->file('image')->getClientOriginalExtension());
                $fileName = str_random() . '.' . $extension;
                $request->file('image')->move($dir, $fileName);
                $image->image = $fileName;
            } elseif ($request->remove == 1 && File::exists('uploads/' . $image->image)) {
                File::delete('uploads/' . $image->post_image);
                $image->image = null;
            }
            $image->description = $request->description;
            $image->save();
            return redirect('/imageuploads');
        }
    }
}