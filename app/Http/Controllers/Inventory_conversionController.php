<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Inventory_Conversion;
use App\Inventory;

class Inventory_conversionController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

	public function index()
	{
        $inventories = Inventory::all();
        
 		$inventory_conversions = Inventory_Conversion::latest()->paginate(10);
        return view('inventory_conversions.index',compact('inventory_conversions','inventories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
            

	}

	 public function create()
    {
       return view('Inventory_conversion.index');
    }

    
     public function store(Request $request)
    {
         request()->validate([

            'date_converted'=>'required',
            'item_code' => 'required',
            'item_name' => 'required',
            'fr_qty' => 'required',
            'fr_cost' => 'required',
            'to_cost' => 'required',
            'to_qty' => 'required',
            'remarks' => 'required',

            // 'receive_id','date_received','item_code', 'item_name','item_qty', 'item_unit_cost', 'item_total_cost', 'supplier_id', 'supplier_name','remarks'
        ]);
        Inventory_Conversion::create($request->all());

         $inventory=Inventory::find($request->item_code);
        $qty= $inventory->quantity - $request->fr_qty;
        $inventory->quantity = $qty;
        $inventory->save();
        
        return redirect()->route('inventory_conversion.index')
                        ->with('success','Conversion Done!');
    }

    public function show(Inventory_Conversion $inventory_conversions)
    {
      return view('inventory_conversions.index',compact('inventory_conversions'));
    }

     public function edit(Inventory_Conversion $inventory_conversions)
    {
       return view('inventory_conversions.index',compact('inventory_conversions'));
    }
     

    public function update(Request $request, $id)
    {
        request()->validate([

            'date_converted'=>'required',
            'item_code' => 'required',
            'item_name' => 'required',
            'fr_qty' => 'required',
            'fr_cost' => 'required',
            'to_cost' => 'required',
            'to_qty' => 'required',
            'remarks' => 'required',

        ]);
        
        $inventory_conversions=Inventory_Conversion::find($id)->update($request->all());
        return redirect()->route('inventory_conversion.index')
                        ->with('success','Record successfully updated!');
    } 

    public function destroy($id)
    {
       
    }
}
