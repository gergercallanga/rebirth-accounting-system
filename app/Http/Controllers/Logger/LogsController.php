<?php

namespace App\Http\Controllers\Logger;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Activitylog\Models\Activity;

class LogsController extends Controller
{
    public function index(){
        $logs = Activity::with('users')->get()->toJson();
        return view("loggerTemplates.logger", compact("logs"));
    }

}
