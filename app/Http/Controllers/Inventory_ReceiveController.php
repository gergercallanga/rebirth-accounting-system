<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Inventory_Receive;
use App\Inventory;
use App\Supplier;

class Inventory_ReceiveController extends Controller
{
     public function __construct(){
        $this->middleware('auth');
    }

     public function index()
    {
         $suppliers = Supplier::all();
        $inventories = Inventory::all();
        $inventory_receives = Inventory_Receive::latest()->paginate(10);
        return view('inventory_receives.index',compact('inventory_receives','inventories','suppliers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
       return view('inventory_receives.index');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
         request()->validate([

            'date_received'=>'required',
            'item_code' => 'required',
            'item_name' => 'required',
            'item_qty' => 'required',
            'item_unit_cost' => 'required',
            'item_total_cost' => 'required',
            'supplier_id' => 'required',
            'supplier_name' => 'required',
            'remarks' => 'required',

            // 'receive_id','date_received','item_code', 'item_name','item_qty', 'item_unit_cost', 'item_total_cost', 'supplier_id', 'supplier_name','remarks'
        ]);
       Inventory_Receive::create($request->all());

        $inventory=Inventory::find($request->item_code);
        $qty= $inventory->quantity + $request->item_qty;
        $inventory->quantity = $qty;
        $inventory->save();
        return redirect()->route('inventory_receiving.index')
                        ->with('success','Item successfully Received!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show(Inventory_Receive $inventory_receives)
    {
      return view('inventory_receives.index',compact('inventory_receives'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit(Inventory_Receive $inventory_receives)
    {
       return view('inventory_receives.index',compact('inventory_receives'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request, $id)
    {
        request()->validate([

            'date_received'=>'required',
            'item_code' => 'required',
            'item_name' => 'required',
            'item_qty' => 'required',
            'item_unit_cost' => 'required',
            'item_total_cost' => 'required',
            'supplier_id' => 'required',
            'supplier_name' => 'required',
            'remarks' => 'required',
        ]);
        
        $inventory_receives=Inventory_Receive::find($id)->update($request->all());
        return redirect()->route('inventory_receiving.index')
                        ->with('success','Record successfully updated!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
       
    }
}
