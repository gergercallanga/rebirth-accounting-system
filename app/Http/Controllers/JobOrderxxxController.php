<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Maintenance;
use App\Customer;
use App\Vehicle;
use App\Inquiry;
use App\PaymentType;
use App\Service;
use App\Image;
use DB;


class JobOrderxxxController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

     public function index()
    {
        $customers=Customer::all();
        $vehicles=Vehicle::all();
        $inquiries=Inquiry::all();
        $paymenttypes=PaymentType::all();
        $services=Service::all();
        $images=Image::all();
     

        $cusvehs=Customer::where('id',$vehicles)->with('vehicles')->get();
        // $articles = Articles::where('category_id', $categoryID)->with('author')->get();

        $maintenances=DB::table('maintenances')
            ->leftjoin('customers','maintenances.customer_id','=','customers.id')
            ->leftjoin('vehicles','maintenances.vehicle_id','=','vehicles.id')
            ->select('maintenances.*','customers.name','vehicles.type', 'vehicles.plate')
            ->distinct()
            ->get();

        return view('joborderxxx.index_joborder',compact('maintenances','roles','joborders','cusvehs','services','inquiries','paymenttypes','vehicles','customers','images','customers_list'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        return view('joborderxxx.create_joborder');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        request()->validate([
            // 'customer_id' => 'required',
            'vehicle_id' => 'required',
            'description' => 'required',
            'sched_from' => 'required',
            'sched_to' => 'required',
            'from_time' => 'required',
            'to_time' => 'required',
            'amount' => 'required',
            'status' => 'required',
            'note' => 'required',
        ]);
        Maintenance::create($request->all());
        return redirect()->route('joborderxxx.index')
                        ->with('success','Maintenance created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show(Maintenance $maintenance)
    {
        return view('joborderxxx..show_joborder',compact('maintenance'));
    }   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit(Maintenance $maintenance)
    {
       
        return view('joborderxxx.edit_joborder',compact('maintenance'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request,Maintenance $maintenance)
    {
        request()->validate([
            'customer_id' => 'required',
            'vehicle_id' => 'required',
            'description' => 'required',
            'sched_from' => 'required',
            'sched_to' => 'required',
            'from_time' => 'required',
            'to_time' => 'required',
            'amount' => 'required',
            'status' => 'required',
            'note' => 'required',

// 'id', 'customer_id', 'vehicle_id', 'description', 'sched_on', 'from_time', 'to_time', 'amount', 'status', 'note' 

        ]);
        $maintenance->update($request->all());
        return redirect()->route('joborderxxx..index')
                        ->with('success','Maintenance updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        Maintenance::destroy($id);
        return redirect()->route('joborderxxx.index')
                        ->with('success','Maintenance deleted successfully');
    }
}
