<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Maintenance;
use App\Calendar;
use App\Customer;
use App\Vehicle;

class CalendarController extends Controller
{

     public function __construct(){
        $this->middleware('auth');
    }

    

    public function show(){
       $maintenance = Maintenance::all();
        $customers = Customer::all();
        $vehicles = Vehicle::all();
       return view('calendar.calendar', compact('maintenance','customers','vehicles'));
    }
    public function viewMaintenance()
    {

        $maintenance = Maintenance::with('vehicles')->get();
        $plotcalendar = array();
        foreach ($maintenance as $maintenances) {
          $plotcalendar[] = [
             'id' => $maintenances->id,
             'title' => $maintenances->vehicles->type." ".$maintenances->vehicles->plate,
             'start' => $maintenances->sched_from."T".$maintenances->from_time,
             'end' => $maintenances->sched_to."T".$maintenances->to_time,
             'color'=> '#B71C1C'
   

             ];

        }

        return json_encode($plotcalendar);
    }
}
