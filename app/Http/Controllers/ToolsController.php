<?php

namespace App\Http\Controllers;

use App\Http\Requests\SmsRequest;




use App\Jobs\SmsSenderJob;
use App\Library\Services\SmsGateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;


class ToolsController extends Controller
{
    function __construct(SmsGateway $sms_gateway){
        $this->sms_gateway = $sms_gateway;

    }
    public function smsSender(){

        return view('utilities.tools.sms_sender');
    /*    var_dump($this->sms_gateway->getMessage(
            53920053));*/
    }
    public function sendSms(SmsRequest $request){

        return redirect()->back()->with('success', json_encode($this->sms_gateway->sendMessageToNumber($request->receiver_num, $request->msg_body)));


       $this->dispatch(new SmsSenderJob($request->all(),$this->sms_gateway));
      /*  $result = $this->sms_gateway->sendMessageToNumber($request->receiver_num, $request->msg_body);
        Log::info(json_encode($result));*/

        return redirect()->back()->with('success', "SMS send please view logs to monitor this message");

    }
}
