<?php

namespace App\Http\Controllers;

use App\Http\Requests\PermissionPost;
use App\User;
use Illuminate\Http\Request;
use Mockery\Exception;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function show($id){
        try {
            $permission = Permission::findorFail($id)->toJson();

            return response($permission)
                ->header('Content-Type', 'application/json');
        }catch(Exception $e){

        }

    }
    public function view(){

    }
    public function create(){

    }
    public function store(PermissionPost $request){
        $request->guard_name = 'auth';
        $permission = new Permission($request->all());
        $permission->save();
        return back();

    }
    public function edit(){
        return response(json_encode("success"))
            ->header('Content-Type', 'application/json');
    }
    public function update(){

    }
    public function delete($id){
      $permission = Permission::all();
      return response(json_encode("success"))
          ->header('Content-Type', 'application/json');
    }
    public function destroy($id){
        Permission::destroy($id);

        return response(json_encode("success"))
            ->header('Content-Type', 'application/json');
    }
}
