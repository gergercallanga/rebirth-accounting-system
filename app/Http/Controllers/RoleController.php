<?php

namespace App\Http\Controllers;

use App\Http\Requests\RolePost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{


    public function view($id){

    }

    public function create(){

    }
    public function edit($id){
        $role = Role::with('permissions')->where('id',$id)->get();
        return response()->json($role);
    }
    public function update($id, RolePost $request){

        DB::transaction(function () use ($request, $id) {
            $role = Role::where('id', $id)->first();
            $role->name = $request->name;
            $role->save();

                $permission_name = Permission::find($request->permission_chbox);
                $role->syncPermissions($permission_name);
        });
        return redirect()->back()->with('success', 'Role Updated');   ;
    }
    public function store(RolePost $request){
        DB::transaction(function () use ($request) {
            $role = Role::create($request->all());
            foreach ($request->permission_chbox as $role_permissions){
                $permission_name = Permission::find($role_permissions);
                $role->givePermissionTo($permission_name->name);
            }

        });

        return redirect()->back();
    }

    public function delete(){

    }
}
