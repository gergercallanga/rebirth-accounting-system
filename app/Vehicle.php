<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'id','customer_id', 'type', 'model', 'plate','color'
    ];

    public function customer(){
	return $this->belongsTo(Customer::class,'customer_id');
}
}
