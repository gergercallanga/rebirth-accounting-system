<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Maintenance;
use Illuminate\Support\Facades\Mail;
class SendScheduleEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
   
    public function handle()
    {
        $maintenances_customer = Maintenance::with('customer')->where('sched_from', date('Y-m-d'))->get();

      $this->info('Fetch Data:'.  date('Y-m-d'));
     
        // foreach($maintenances as $maintenance) {
     
        //     // Create a unique 8 character promo code for user
        //     $new_promo_code = new PromoCode([
        //         'promo_code' => str_random(8),
        //     ]);
     
        //     $user->promo_code()->save($new_promo_code);
            
            // Send the email to user
        foreach ($maintenances_customer as $maintenances) {
           
        
            Mail::send('emails.schedule', ['maintenances' => $maintenances], function ($mail) use ($maintenances) {
                $mail->to($maintenances['customer']['email'])
                    ->from('chacharlooot@gmail.com', 'Company')
                    ->subject('Rebirth Car Coating Schedule');
                });
             $this->info('Sending Email to :'.$maintenances['customer']['email']);
             $this->info('Schedule sent successfully!');
        }

                 
       
     
    }
}
