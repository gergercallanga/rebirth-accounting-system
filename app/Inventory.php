<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
     protected $fillable = [
        'id','product', 'description','quantity', 'orig_price', 'final_price'
    ];
}
