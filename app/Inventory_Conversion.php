<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory_Conversion extends Model
{
    protected $table = "inventory_conversions";
    protected $fillable = [
    'id','date_converted','item_code', 'item_name','fr_cost', 'fr_qty','to_cost','to_qty','remarks'
    ];
}
