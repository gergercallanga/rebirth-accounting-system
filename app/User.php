<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use LogsActivity;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname','lname','mname', 'email','password','contact_num','username'
    ];
    protected $guard_name = 'web';

    protected static $logAttributes = [ 'fname','lname','mname', 'email','password','contact_num','username','password'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAllUsers(){

    }

    public function passwordSecurity()
    {
        return $this->hasOne('App\PasswordSecurity');
    }

}
