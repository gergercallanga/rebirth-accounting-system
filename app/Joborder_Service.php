<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Joborder_Service extends Model
{
    protected $table="joborder_services";
    protected $timestamp=false;
}
