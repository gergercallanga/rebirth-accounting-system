<?php

namespace App;
use Illuminate\Notifications\Notifiable;
	use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{


    use Notifiable;
	// protected $maintenances = "id";
      protected $fillable = [
        'id', 'customer_id', 'vehicle_id', 'description', 'sched_from','sched_to', 'from_time', 'to_time', 'amount', 'status', 'note'

        ];

           public function vehicles()
      		  {
           		 return $this->belongsTo('App\Vehicle','vehicle_id');
       		  }

           public function customer()
            { 
              return $this->belongsTo('App\Customer','customer_id');
            }

   		


}
