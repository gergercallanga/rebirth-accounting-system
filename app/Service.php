<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $primaryKey = "service_id";
    protected $fillable = [
        'service_id','service_name','price','status','remarks'
    ];

}
