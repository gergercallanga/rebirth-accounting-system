-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2018 at 02:46 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rebirth`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` int(11) DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'default', 'updated', 1, 'App\\User', 1, 'App\\User', '[]', '2017-12-28 23:52:08', '2017-12-28 23:52:08'),
(2, 'default', 'updated', 1, 'App\\User', 1, 'App\\User', '[]', '2017-12-28 23:54:14', '2017-12-28 23:54:14'),
(3, 'default', 'updated', 1, 'App\\User', 1, 'App\\User', '{\"attributes\":{\"fname\":\"gerwin\",\"lname\":\"callanga\",\"mname\":\"gsssss.\",\"email\":\"gerwincallanga@yahoo.coms\",\"contact_num\":\"093924048652\",\"username\":\"gerwin.callanga\"},\"old\":{\"fname\":\"gerwin\",\"lname\":\"callanga\",\"mname\":\"gsssss.\",\"email\":\"gerwincallanga@yahoo.coms\",\"contact_num\":\"093924048652\",\"username\":\"gerwin.callanga\"}}', '2017-12-29 00:32:01', '2017-12-29 00:32:01'),
(4, 'default', 'updated', 2, 'App\\User', 8, 'App\\User', '{\"attributes\":{\"fname\":\"pogis\",\"lname\":\"qweqwe\",\"mname\":\"qweqwe\",\"email\":\"gerwinclal@yahoo.com\",\"contact_num\":\"77722722\",\"username\":\"pogis.qweqwe\"},\"old\":{\"fname\":\"pogi\",\"lname\":\"qweqwe\",\"mname\":\"qweqwe\",\"email\":\"gerwinclal@yahoo.com\",\"contact_num\":\"77722722\",\"username\":\"gerwin.cal\"}}', '2017-12-29 00:35:30', '2017-12-29 00:35:30'),
(5, 'default', 'updated', 7, 'App\\User', 1, 'App\\User', '{\"attributes\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"123123\",\"username\":\"charlot.panal\"},\"old\":{\"fname\":\"qweqweqwe\",\"lname\":\"qwkjehqwkjlehkj\",\"mname\":\"qwekjqwejqw\",\"email\":\"ggg@mail.com\",\"contact_num\":\"123123\",\"username\":\"ggg.cal\"}}', '2018-01-11 21:51:09', '2018-01-11 21:51:09'),
(6, 'default', 'updated', 1, 'App\\User', 1, 'App\\User', '{\"attributes\":{\"fname\":\"gerwin\",\"lname\":\"callanga\",\"mname\":\"gsssss.\",\"email\":\"gerwincallanga@yahoo.coms\",\"contact_num\":\"093924048652\",\"username\":\"gerwin.callanga\"},\"old\":{\"fname\":\"gerwin\",\"lname\":\"callanga\",\"mname\":\"gsssss.\",\"email\":\"gerwincallanga@yahoo.coms\",\"contact_num\":\"093924048652\",\"username\":\"gerwin.callanga\"}}', '2018-01-13 00:46:39', '2018-01-13 00:46:39'),
(7, 'default', 'created', 11, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Kaye\",\"lname\":\"Pinas\",\"mname\":\"Ladoc\",\"email\":\"kaye@kaye.com\",\"contact_num\":\"0981735465\",\"username\":\"kaye.pinas\"}}', '2018-01-25 22:51:30', '2018-01-25 22:51:30'),
(8, 'default', 'updated', 7, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"09477895676\",\"username\":\"charlot.panal\"},\"old\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"123123\",\"username\":\"charlot.panal\"}}', '2018-02-06 08:33:35', '2018-02-06 08:33:35'),
(9, 'default', 'created', 12, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Kaye\",\"lname\":\"Pinas\",\"mname\":\"Ladoc\",\"email\":\"kayeann@gmail.com\",\"contact_num\":\"09480671556\",\"username\":\"kaye.pinas\"}}', '2018-02-06 21:12:49', '2018-02-06 21:12:49'),
(10, 'default', 'updated', 7, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"09477895676\",\"username\":\"charlot.panal\"},\"old\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"09477895676\",\"username\":\"charlot.panal\"}}', '2018-02-06 21:15:11', '2018-02-06 21:15:11'),
(11, 'default', 'created', 14, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Cha\",\"lname\":\"Pan\",\"mname\":\"s\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"0904204\",\"username\":\"cha.pan\"}}', '2018-02-28 11:14:43', '2018-02-28 11:14:43'),
(12, 'default', 'created', 15, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"Purazo\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"0897654\",\"username\":\"charlie.panal\"}}', '2018-02-28 22:50:35', '2018-02-28 22:50:35'),
(13, 'default', 'created', 16, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"Purazo\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"987654\",\"username\":\"charlie.panal\"}}', '2018-02-28 22:53:57', '2018-02-28 22:53:57'),
(14, 'default', 'created', 17, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"Purazo\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"87654\",\"username\":\"charlie.panal\"}}', '2018-02-28 22:57:52', '2018-02-28 22:57:52'),
(15, 'default', 'created', 18, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"Purazo\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"98765432\",\"username\":\"charlie.panal\"}}', '2018-02-28 23:01:44', '2018-02-28 23:01:44'),
(16, 'default', 'updated', 7, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"09477895676\",\"username\":\"charlot.panal\"},\"old\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"09477895676\",\"username\":\"charlot.panal\"}}', '2018-03-03 07:47:22', '2018-03-03 07:47:22'),
(17, 'default', 'created', 19, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"Purazo\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"34567\",\"username\":\"charlie.panal\"}}', '2018-03-03 08:53:46', '2018-03-03 08:53:46'),
(18, 'default', 'created', 20, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"Purazo\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"456789\",\"username\":\"charlie.panal\"}}', '2018-03-03 08:55:42', '2018-03-03 08:55:42'),
(19, 'default', 'updated', 7, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"09477895676\",\"username\":\"charlot.panal\"},\"old\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"09477895676\",\"username\":\"charlot.panal\"}}', '2018-03-03 08:56:14', '2018-03-03 08:56:14'),
(20, 'default', 'created', 21, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"gfc\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"45678\",\"username\":\"charlie.panal\"}}', '2018-03-03 08:57:59', '2018-03-03 08:57:59'),
(21, 'default', 'updated', 7, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"09477895676\",\"username\":\"charlot.panal\"},\"old\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"09477895676\",\"username\":\"charlot.panal\"}}', '2018-03-03 08:59:45', '2018-03-03 08:59:45'),
(22, 'default', 'updated', 21, 'App\\User', NULL, NULL, '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"gfc\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"45678\",\"username\":\"charlie.panal\"},\"old\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"gfc\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"45678\",\"username\":\"charlie.panal\"}}', '2018-03-03 09:10:49', '2018-03-03 09:10:49'),
(23, 'default', 'updated', 21, 'App\\User', 21, 'App\\User', '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"gfc\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"45678\",\"username\":\"charlie.panal\"},\"old\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"gfc\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"45678\",\"username\":\"charlie.panal\"}}', '2018-03-03 09:11:44', '2018-03-03 09:11:44'),
(24, 'default', 'updated', 21, 'App\\User', 21, 'App\\User', '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"gfc\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"45678\",\"username\":\"charlie.panal\"},\"old\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"gfc\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"45678\",\"username\":\"charlie.panal\"}}', '2018-03-03 09:12:31', '2018-03-03 09:12:31'),
(25, 'default', 'created', 22, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"fghjk\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"56789\",\"username\":\"charlie.panal\"}}', '2018-03-03 09:43:05', '2018-03-03 09:43:05'),
(26, 'default', 'updated', 7, 'App\\User', 7, 'App\\User', '{\"attributes\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"09477895676\",\"username\":\"charlot.panal\"},\"old\":{\"fname\":\"Charlot\",\"lname\":\"Panal\",\"mname\":\"Nesnia\",\"email\":\"charlotnesniapanal@gmail.com\",\"contact_num\":\"09477895676\",\"username\":\"charlot.panal\"}}', '2018-03-03 09:44:14', '2018-03-03 09:44:14'),
(27, 'default', 'updated', 22, 'App\\User', NULL, NULL, '{\"attributes\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"fghjk\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"56789\",\"username\":\"charlie.panal\"},\"old\":{\"fname\":\"Charlie\",\"lname\":\"Panal\",\"mname\":\"fghjk\",\"email\":\"charliepanal@gmail.com\",\"contact_num\":\"56789\",\"username\":\"charlie.panal\"}}', '2018-03-03 09:46:13', '2018-03-03 09:46:13');

-- --------------------------------------------------------

--
-- Table structure for table `calendars`
--

CREATE TABLE `calendars` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calendar_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sync_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT 'None',
  `mobile` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `nickname`, `address`, `telephone`, `mobile`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Divine Loreto', 'Vine', 'Purok 6-C South Daang Hari Taguig City', '7543010', '09477895676', 'viney@gmail.com', '2018-01-24 11:33:39', '2018-02-06 09:28:12'),
(3, 'Jeffrey Eusebio', 'Jeff', 'Cupang Muntinlupa City', '2349054', '09197865456', 'jeff_3@yahoo.com', '2018-01-24 11:37:11', '2018-02-06 09:26:48'),
(7, 'Charles Reyes', 'Charles', 'Purok 4 Sucat Muntinlupa City', '876653434', '09097483678', 'chareyes@gmail.com', '2018-01-29 09:57:45', '2018-02-06 09:29:35'),
(8, 'Henry Sandoval', 'Henry', 'Makati City', '35235968', '09473498657', 'henrysands@gmail.com', '2018-02-01 10:31:15', '2018-02-06 09:28:49');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calendar_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datetime_start` datetime NOT NULL,
  `datetime_end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE `inventories` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `orig_price` decimal(7,2) NOT NULL,
  `final_price` decimal(7,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `product`, `description`, `quantity`, `orig_price`, `final_price`, `created_at`, `updated_at`) VALUES
(1, 'Glass Coating', 'For Glass Coating and other stuffs', 50, '900.00', '1000.00', '2018-02-01 10:07:29', '2018-02-06 09:47:19'),
(2, 'Ceramic Coating', 'For Ceramic Car Coating', 20, '1000.00', '1300.00', '2018-02-06 09:46:32', '2018-02-06 09:46:32');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_conversions`
--

CREATE TABLE `inventory_conversions` (
  `id` int(10) UNSIGNED NOT NULL,
  `date_converted` date NOT NULL,
  `item_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fr_cost` double(8,2) NOT NULL,
  `fr_qty` int(11) NOT NULL,
  `to_cost` double(8,2) NOT NULL,
  `to_qty` int(11) NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_receives`
--

CREATE TABLE `inventory_receives` (
  `id` int(10) UNSIGNED NOT NULL,
  `date_received` date NOT NULL,
  `item_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_qty` int(11) NOT NULL,
  `item_unit_cost` double(8,2) NOT NULL,
  `item_total_cost` double(8,2) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `maintenances`
--

CREATE TABLE `maintenances` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sched_from` date NOT NULL,
  `sched_to` date NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `amount` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `maintenances`
--

INSERT INTO `maintenances` (`id`, `customer_id`, `vehicle_id`, `description`, `sched_from`, `sched_to`, `from_time`, `to_time`, `amount`, `status`, `note`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '1st Maintenance', '2018-02-25', '2018-02-28', '20:45:00', '19:40:00', 39000, 'Pending', 'Dhaihfi', '2018-02-26 06:47:33', '2018-02-26 06:47:33'),
(2, 7, 3, '1st Maintenance', '2018-02-25', '2018-02-28', '07:45:00', '06:50:00', 50000, 'Pending', 'asx', '2018-02-26 06:56:50', '2018-02-26 06:56:50'),
(3, 1, 4, '1st Maintenance', '2018-03-12', '2018-03-21', '08:30:00', '17:25:00', 50000, 'Pending', 'Test', '2018-02-28 23:14:38', '2018-02-28 23:14:38'),
(4, 1, 5, '1st Maintenance', '2018-03-26', '2018-03-27', '05:40:00', '04:40:00', 50000, 'Pending', 'Test', '2018-02-28 23:39:29', '2018-02-28 23:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2017_12_02_083537_create_permission_tables', 1),
(7, '2016_01_01_000000_add_voyager_user_fields', 2),
(8, '2016_01_01_000000_create_data_types_table', 2),
(9, '2016_01_01_000000_create_pages_table', 2),
(10, '2016_01_01_000000_create_posts_table', 2),
(11, '2016_02_15_204651_create_categories_table', 2),
(12, '2016_05_19_173453_create_menu_table', 2),
(13, '2017_12_29_062946_create_activity_log_table', 3),
(16, '2018_01_13_090404_create_vehicle_table', 4),
(17, '2018_01_13_174352_create_services_table', 5),
(20, '2018_01_16_032802_create_jobs_table', 7),
(21, '2018_01_16_050818_create_failed_jobs_table', 7),
(22, '2018_01_13_093707_create_customers_table', 8),
(23, '2018_02_01_163214_create_inventories_table', 9),
(24, '2015_11_09_072713_create_calendars_table', 10),
(25, '2015_11_09_072741_create_events_table', 10),
(26, '2015_11_10_152439_add_sync_token_to_calendars_table', 10),
(27, '2015_11_10_180354_add_event_id_to_events_table', 10),
(28, '2015_11_09_072626_create_usergoos_table', 11),
(31, '2018_02_26_142555_create_maintenances_table', 12),
(32, '2018_02_28_181528_create_suppliers_table', 13),
(33, '2018_03_03_163537_create_inventory_receives', 14),
(34, '2018_03_04_042830_create_inventory_conversions', 14);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_id`, `model_type`) VALUES
(1, 1, 'App\\User'),
(1, 2, 'App\\User'),
(1, 4, 'App\\User'),
(1, 6, 'App\\User'),
(1, 7, 'App\\User'),
(1, 8, 'App\\User'),
(1, 9, 'App\\User'),
(1, 10, 'App\\User'),
(1, 11, 'App\\User'),
(1, 12, 'App\\User'),
(1, 14, 'App\\User'),
(1, 15, 'App\\User'),
(1, 16, 'App\\User'),
(1, 17, 'App\\User'),
(1, 18, 'App\\User'),
(1, 19, 'App\\User'),
(1, 20, 'App\\User'),
(1, 22, 'App\\User'),
(14, 1, 'App\\User'),
(14, 7, 'App\\User'),
(14, 8, 'App\\User'),
(14, 9, 'App\\User'),
(14, 10, 'App\\User'),
(14, 11, 'App\\User'),
(14, 12, 'App\\User'),
(14, 14, 'App\\User'),
(14, 15, 'App\\User'),
(14, 16, 'App\\User'),
(14, 17, 'App\\User'),
(14, 18, 'App\\User'),
(14, 19, 'App\\User'),
(14, 21, 'App\\User'),
(14, 22, 'App\\User'),
(15, 7, 'App\\User'),
(15, 8, 'App\\User'),
(15, 10, 'App\\User'),
(15, 12, 'App\\User'),
(15, 14, 'App\\User'),
(15, 15, 'App\\User'),
(15, 16, 'App\\User'),
(15, 17, 'App\\User'),
(15, 19, 'App\\User'),
(16, 7, 'App\\User'),
(16, 10, 'App\\User'),
(16, 11, 'App\\User'),
(16, 12, 'App\\User'),
(16, 15, 'App\\User'),
(16, 16, 'App\\User'),
(16, 19, 'App\\User'),
(17, 7, 'App\\User'),
(17, 11, 'App\\User'),
(17, 12, 'App\\User'),
(17, 15, 'App\\User'),
(17, 16, 'App\\User'),
(17, 19, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('gerwincallanga@yahoo.com', '$2y$10$hN26bYFO9kwrxKfXhYaPAeEQlMEH0MSnS5cnM3hDIfx651vEc/Ar.', '2017-12-20 21:53:48'),
('gergercallanga@gmail.com', '$2y$10$TydgsIpkPRXVf4KaE0xIT.0dMb03oWeqiFncg7qoo.Tsx91W6q39K', '2017-12-27 01:51:22'),
('kaye@kaye.com', '$2y$10$QMFR45N/rE/KI8PCD7Y0Ze8Zm1ul/Xre.DKfw7cxGs1DypJejskee', '2018-01-25 22:51:30'),
('kayeann@gmail.com', '$2y$10$j129o6tXrW363KwHj5fkCuxHCZMJmkI3LdJPdcrJDy930Ih1Jm8nS', '2018-02-06 21:12:49'),
('gerwincallanga@yahoo.com', '$2y$10$hN26bYFO9kwrxKfXhYaPAeEQlMEH0MSnS5cnM3hDIfx651vEc/Ar.', '2017-12-20 21:53:48'),
('gergercallanga@gmail.com', '$2y$10$TydgsIpkPRXVf4KaE0xIT.0dMb03oWeqiFncg7qoo.Tsx91W6q39K', '2017-12-27 01:51:22'),
('kaye@kaye.com', '$2y$10$QMFR45N/rE/KI8PCD7Y0Ze8Zm1ul/Xre.DKfw7cxGs1DypJejskee', '2018-01-25 22:51:30'),
('kayeann@gmail.com', '$2y$10$j129o6tXrW363KwHj5fkCuxHCZMJmkI3LdJPdcrJDy930Ih1Jm8nS', '2018-02-06 21:12:49');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'create users', 'web', '2017-12-02 02:51:33', '2017-12-02 02:51:33', '0000-00-00 00:00:00'),
(3, 'edit users', 'web', '2017-12-12 03:17:06', '2017-12-12 03:17:06', '0000-00-00 00:00:00'),
(4, 'delete users', 'web', '2017-12-12 03:17:37', '2017-12-12 03:17:37', '0000-00-00 00:00:00'),
(5, 'show users', 'web', '2017-12-25 17:39:01', '2017-12-25 17:39:01', NULL),
(6, 'wwww', 'web', '2017-12-27 19:45:44', '2017-12-27 19:45:44', NULL),
(7, 'edit permissions', 'web', '2017-12-28 22:00:27', '2017-12-28 22:00:27', NULL),
(8, 'delete permissions', 'web', '2017-12-28 22:18:06', '2017-12-28 22:18:06', NULL),
(9, 'WOW', 'web', '2018-02-28 22:43:16', '2018-02-28 22:43:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'web', '2017-12-02 02:51:32', '2017-12-02 02:51:32', '0000-00-00 00:00:00'),
(14, 'admin_accounting_update', 'web', '2017-12-25 19:22:56', '2017-12-26 23:42:25', NULL),
(15, 'admin_finance', 'web', '2017-12-25 19:24:22', '2017-12-25 19:24:22', NULL),
(16, 'admin_IT', 'web', '2017-12-26 02:21:53', '2017-12-26 02:21:53', NULL),
(17, 'admin_dev_team', 'web', '2017-12-28 22:16:12', '2017-12-28 22:16:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(3, 1),
(3, 14),
(3, 15),
(3, 16),
(3, 17),
(4, 14),
(4, 15),
(4, 17),
(5, 1),
(5, 15),
(5, 17),
(7, 1),
(7, 17),
(8, 17);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_name`, `price`, `status`, `remarks`, `created_at`, `updated_at`) VALUES
(1, 'Glass Coating', 7000, 'Normal', 'Standard', '2018-01-13 19:44:37', '2018-02-06 21:29:50'),
(2, 'Titanium Coating', 13000, 'Normal', 'Standard w/ Carbon Heater', '2018-01-13 11:51:13', '2018-02-06 21:30:38'),
(5, 'Ceramic Coating', 10000, 'Package', 'Super Premium w/ Carbon Heater', '2018-01-14 02:07:33', '2018-02-06 09:51:00'),
(6, 'Diamond Coating', 13000, 'Package', 'Using Carbon Heater', '2018-01-24 10:23:29', '2018-02-06 09:49:28');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `supplier_name`, `supplier_address`, `created_at`, `updated_at`) VALUES
(1, 'Charlot Panal', 'Taguig City', '2018-02-28 10:54:36', '2018-02-28 10:54:36'),
(2, 'Shiela Marie Supan', 'Muntinlupa City', '2018-02-28 10:56:25', '2018-02-28 10:57:15');

-- --------------------------------------------------------

--
-- Table structure for table `usergoos`
--

CREATE TABLE `usergoos` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `contact_num` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `fname`, `lname`, `mname`, `username`, `email`, `avatar`, `contact_num`, `password`, `status`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, NULL, 'Charlot', 'Panal', 'Nesnia', 'charlot.panal', 'charlotnesniapanal@gmail.com', 'users/yongji.jpg', '09477895676', '$2y$10$zAwlUUqkR/pCRwWw.VfNgenaInOuGMM3rL0f5KVoDHKt8j3uBBdom', 1, 'jbTJX4tNLQGTD6phQe83k3QYhNb5jwxV9caxnNoQiS0OFIHy0RzrpCMFd5ni', '2017-12-05 00:29:32', '2018-02-06 08:33:35', '0000-00-00 00:00:00'),
(9, NULL, 'sample', 'sample', 'sample', 'sample.sample', 'sample@gmail.com', 'users/default.png', '123123', '$2y$10$Vqj.wtGnkFclvSEo75/e.uyQvJEev5Ft.B12qe3pcuoEZpfMolwf6', 1, NULL, '2017-12-26 02:20:32', '2017-12-26 02:20:32', NULL),
(12, NULL, 'Kaye', 'Pinas', 'Ladoc', 'kaye.pinas', 'kayeann@gmail.com', 'users/default.png', '09480671556', '$2y$10$Ivj/8YkkfesTFP2k9aIQHOWV/JNSuJZBQvNIVMNUxXlcI.VbmLkCO', 1, NULL, '2018-02-06 21:12:48', '2018-02-06 21:12:48', NULL),
(22, NULL, 'Charlie', 'Panal', 'fghjk', 'charlie.panal', 'charliepanal@gmail.com', 'users/default.png', '56789', '$2y$10$APLIqbXz8FEx7q69y1XVEui0F3oujQuxz0tWmEIrDcX4XSD2ByicS', 1, 'mkUqU5jLbylPdDIKBACBvuLgTvwB07WNj3fgE7GMoGI3JgjglQ4or5Pg3EoA', '2018-03-03 09:43:05', '2018-03-03 09:46:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `customer_id`, `type`, `model`, `plate`, `color`, `created_at`, `updated_at`) VALUES
(1, 1, 'Chevrolet', 'Sonic', 'QWE 897', 'Red', NULL, '2018-02-06 08:54:17'),
(3, 7, 'Audi', 'A3', 'GHT 879', 'White', '2018-01-13 08:26:37', '2018-02-06 08:52:58'),
(4, 3, 'Benz', 'A-Class', 'SDF 097', 'Black', '2018-01-13 08:47:03', '2018-02-06 08:47:04'),
(5, 7, 'BMW', '5Series', 'WDF 563', 'Red', '2018-02-06 09:32:31', '2018-02-06 22:02:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`);

--
-- Indexes for table `calendars`
--
ALTER TABLE `calendars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_conversions`
--
ALTER TABLE `inventory_conversions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_receives`
--
ALTER TABLE `inventory_receives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `maintenances`
--
ALTER TABLE `maintenances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usergoos`
--
ALTER TABLE `usergoos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `calendars`
--
ALTER TABLE `calendars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `inventory_conversions`
--
ALTER TABLE `inventory_conversions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory_receives`
--
ALTER TABLE `inventory_receives`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `maintenances`
--
ALTER TABLE `maintenances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `usergoos`
--
ALTER TABLE `usergoos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
