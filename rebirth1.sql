-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2017 at 11:20 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rebirth`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` int(11) DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'default', 'updated', 1, 'App\\User', 1, 'App\\User', '[]', '2017-12-28 23:52:08', '2017-12-28 23:52:08'),
(2, 'default', 'updated', 1, 'App\\User', 1, 'App\\User', '[]', '2017-12-28 23:54:14', '2017-12-28 23:54:14'),
(3, 'default', 'updated', 1, 'App\\User', 1, 'App\\User', '{\"attributes\":{\"fname\":\"gerwin\",\"lname\":\"callanga\",\"mname\":\"gsssss.\",\"email\":\"gerwincallanga@yahoo.coms\",\"contact_num\":\"093924048652\",\"username\":\"gerwin.callanga\"},\"old\":{\"fname\":\"gerwin\",\"lname\":\"callanga\",\"mname\":\"gsssss.\",\"email\":\"gerwincallanga@yahoo.coms\",\"contact_num\":\"093924048652\",\"username\":\"gerwin.callanga\"}}', '2017-12-29 00:32:01', '2017-12-29 00:32:01'),
(4, 'default', 'updated', 2, 'App\\User', 8, 'App\\User', '{\"attributes\":{\"fname\":\"pogis\",\"lname\":\"qweqwe\",\"mname\":\"qweqwe\",\"email\":\"gerwinclal@yahoo.com\",\"contact_num\":\"77722722\",\"username\":\"pogis.qweqwe\"},\"old\":{\"fname\":\"pogi\",\"lname\":\"qweqwe\",\"mname\":\"qweqwe\",\"email\":\"gerwinclal@yahoo.com\",\"contact_num\":\"77722722\",\"username\":\"gerwin.cal\"}}', '2017-12-29 00:35:30', '2017-12-29 00:35:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2017_12_02_083537_create_permission_tables', 1),
(7, '2016_01_01_000000_add_voyager_user_fields', 2),
(8, '2016_01_01_000000_create_data_types_table', 2),
(9, '2016_01_01_000000_create_pages_table', 2),
(10, '2016_01_01_000000_create_posts_table', 2),
(11, '2016_02_15_204651_create_categories_table', 2),
(12, '2016_05_19_173453_create_menu_table', 2),
(13, '2017_12_29_062946_create_activity_log_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_id`, `model_type`) VALUES
(1, 1, 'App\\User'),
(1, 2, 'App\\User'),
(1, 4, 'App\\User'),
(1, 6, 'App\\User'),
(1, 7, 'App\\User'),
(1, 8, 'App\\User'),
(1, 9, 'App\\User'),
(1, 10, 'App\\User'),
(14, 1, 'App\\User'),
(14, 8, 'App\\User'),
(14, 9, 'App\\User'),
(14, 10, 'App\\User'),
(15, 8, 'App\\User'),
(15, 10, 'App\\User'),
(16, 10, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('gerwincallanga@yahoo.com', '$2y$10$hN26bYFO9kwrxKfXhYaPAeEQlMEH0MSnS5cnM3hDIfx651vEc/Ar.', '2017-12-20 21:53:48'),
('gergercallanga@gmail.com', '$2y$10$TydgsIpkPRXVf4KaE0xIT.0dMb03oWeqiFncg7qoo.Tsx91W6q39K', '2017-12-27 01:51:22');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'create users', 'web', '2017-12-02 02:51:33', '2017-12-02 02:51:33', '0000-00-00 00:00:00'),
(3, 'edit users', 'web', '2017-12-12 03:17:06', '2017-12-12 03:17:06', '0000-00-00 00:00:00'),
(4, 'delete users', 'web', '2017-12-12 03:17:37', '2017-12-12 03:17:37', '0000-00-00 00:00:00'),
(5, 'show users', 'web', '2017-12-25 17:39:01', '2017-12-25 17:39:01', NULL),
(6, 'wwww', 'web', '2017-12-27 19:45:44', '2017-12-27 19:45:44', NULL),
(7, 'edit permissions', 'web', '2017-12-28 22:00:27', '2017-12-28 22:00:27', NULL),
(8, 'delete permissions', 'web', '2017-12-28 22:18:06', '2017-12-28 22:18:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'web', '2017-12-02 02:51:32', '2017-12-02 02:51:32', '0000-00-00 00:00:00'),
(14, 'admin_accounting_update', 'web', '2017-12-25 19:22:56', '2017-12-26 23:42:25', NULL),
(15, 'admin_finance', 'web', '2017-12-25 19:24:22', '2017-12-25 19:24:22', NULL),
(16, 'admin_IT', 'web', '2017-12-26 02:21:53', '2017-12-26 02:21:53', NULL),
(17, 'admin_dev_team', 'web', '2017-12-28 22:16:12', '2017-12-28 22:16:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(3, 1),
(3, 14),
(3, 15),
(3, 16),
(3, 17),
(4, 14),
(4, 15),
(4, 17),
(5, 1),
(5, 15),
(5, 17),
(6, 17),
(7, 1),
(7, 17),
(8, 17);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `contact_num` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `fname`, `lname`, `mname`, `username`, `email`, `avatar`, `contact_num`, `password`, `status`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 'gerwin', 'callanga', 'gsssss.', 'gerwin.callanga', 'gerwincallanga@yahoo.coms', 'users/default.png', '093924048652', '$2y$10$Vi1ej01hplZ9uB023HrDbuXj9m5CIG0fIUYh3yQ5CcL.fPVeT0EB.', 1, 'HMEQeN3xFAe3oHSxhdGTCiRfIyIsHNOgfPGq6K8rzrZU6ku4T2LC7xnf50N3', '2017-12-02 02:51:33', '2017-12-28 23:54:14', '0000-00-00 00:00:00'),
(2, NULL, 'pogis', 'qweqwe', 'qweqwe', 'pogis.qweqwe', 'gerwinclal@yahoo.com', 'users/default.png', '77722722', '$2y$10$.yVbeu5JXhg.5yI3/RjXce2.Ivln/0cpcP49YfYG0HHjS.ngpNQT2', 0, NULL, '2017-12-05 00:10:25', '2017-12-29 00:35:30', '0000-00-00 00:00:00'),
(7, NULL, 'qweqweqwe', 'qwkjehqwkjlehkj', 'qwekjqwejqw', 'ggg.cal', 'ggg@mail.com', 'users/default.png', '123123', '123123', 1, NULL, '2017-12-05 00:29:32', '2017-12-05 00:29:32', '0000-00-00 00:00:00'),
(8, NULL, 'vince', 'albuera', 'ss', 'vince.albuera', 'vv@mail.com', 'users/default.png', '123123', '$2y$10$Vi1ej01hplZ9uB023HrDbuXj9m5CIG0fIUYh3yQ5CcL.fPVeT0EB.', 1, NULL, '2017-12-25 19:29:03', '2017-12-25 19:29:03', NULL),
(9, NULL, 'sample', 'sample', 'sample', 'sample.sample', 'sample@gmail.com', 'users/default.png', '123123', '$2y$10$Vqj.wtGnkFclvSEo75/e.uyQvJEev5Ft.B12qe3pcuoEZpfMolwf6', 1, NULL, '2017-12-26 02:20:32', '2017-12-26 02:20:32', NULL),
(10, NULL, 'gerwin', 'callanga', 'gunio', 'gerwin.callangas', 'gergercallanga@gmail.com', 'users/default.png', '123123', '$2y$10$JZP9CrjkQzBLmxtlZKSjAut6GgChV6QUDogEvRYiV2ImlIUjaLP1K', 1, NULL, '2017-12-27 01:51:22', '2017-12-27 18:48:32', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
