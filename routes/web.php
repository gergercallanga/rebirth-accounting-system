	<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', 'UserController@UserManagement')->name('UserManagement');

Route::group(['prefix' => '/admin'], function(){
    Route::get('/', 'UserController@UserManagement')->name('UserManagement');

    Route::resource('users', 'UserController', [
        'except' => ['delete']
    ]);
    Route::post('users/toggle_activation', 'UserController@userToggleStatus');

    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');

    Route::get('/logs', 'Logger\LogsController@index')->name('LogsViewer');
});


Route::group(['prefix' => '/tools'], function() {

    Route::get('/smsSender', 'ToolsController@smsSender')->name('SmsSender');
    Route::post('/smsSend', 'ToolsController@sendSms')->name('SmsSend');
});



Route::resource('customers','CustomerController');

Route::resource('services','ServiceController');

Route::resource('vehicles','VehicleController');

Route::resource('maintenances','MaintenanceController');

Route::resource('inventories','InventoryController');

Route::resource('inventory_receiving','Inventory_ReceiveController');

Route::resource('inventory_conversion','Inventory_ConversionController');

Route::resource('itemqty_adjustment','Itemqty_AdjustmentController');

Route::resource('itemcost_adjustment','Itemcost_AdjustmentController');

Route::group(['prefix' => '/calendar'], function () {
    Route::get('show', 'CalendarController@show');
    Route::get('viewMaintenance', 'CalendarController@viewMaintenance');
});

Route::resource('suppliers','SupplierController');

Route::post('/upload' , 'UploadController@upload');
Route::get('/test', function () {
    return view('testing/test');
});


Route::get('dropzoneFile','HomeController@dropzoneFile') ;
Route::post('dropzoneUploadFile',array('as'=>'dropzone.uploadfile','uses'=>'HomeController@dropzoneUploadFile')) ;

Route::get('/passwordExpiration','Auth\PwdExpirationController@showPasswordExpirationForm');
Route::post('/passwordExpiration','Auth\PwdExpirationController@postPasswordExpiration')->name('passwordExpiration');


Route::get('form','FormController@create');
Route::post('form','FormController@store');

 
Route::get('form/show','FormController@show');

Route::group(['prefix' => 'imageuploads '], function () {
    Route::get('/', 'UploadingController@index');
    Route::match(['get', 'post'], 'create', 'UploadingController@create');
    Route::match(['get', 'put'], 'update/{id}', 'UploadingController@update');
    Route::delete('delete/{id}', 'UploadingController@delete');
});

Route::resource('joborderxxx','JobOrderxxxController');
Route::resource('userprofiles','UserController');
Route::get('image-upload',['as'=>'image.upload','uses'=>'UserController@imageUpload']);
Route::post('image-upload',['as'=>'image.upload.post','uses'=>'UserController@imageUploadPost']);

Route::group(['prefix' => '/joborder','middleware' => 'auth'] , function(){
    Route::resource('/','JobOrderController');
    Route::get('/showCustomerVehicles', 'JobOrderController@showCustomerVehicles');
    Route::post('/upload', 'JobOrderController@checkListUpload');
    Route::get('/showServices', 'JobOrderController@showServices');
    Route::post('/getJoborder', 'JobOrderController@addJoborder');
    Route::post('/saveInquiry', 'JobOrderController@saveInquiry');
    Route::post('/saveServices', 'JobOrderController@saveServiceTodb');

});

Route::match(['get', 'post'], 'custom-email', 'TemplateEmailController@customEmail');